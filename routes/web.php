<?php

use App\Http\Controllers\ChatController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\MemberController;

use App\Http\Controllers\Api\NotificationController as Notify;
use App\Http\Controllers\ChurchBranchController;
use App\Http\Controllers\ChurchController;
use App\Http\Controllers\DownloadController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\ReadingPlanController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WebNotificationController;
use App\Http\Controllers\API\ReadingTypeController;
use App\Http\Controllers\BroadcastController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/', function () {
//     return view('admin/test');
// });
// Route::get('test', dd("aa"));
// Route::get('testload', [DashboardController::class, 'test']);
// Route::get('abc', [DashboardController::class, 'abc'])->middleware("role2");




Route::middleware(['auth', "role2"])->group(function () {

    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');


    Route::get('gereja', [ChurchController::class, 'index'])->name('gereja');
    Route::post('gereja', [ChurchController::class, 'store']);
    Route::put('gereja', [ChurchController::class, 'update']);
    Route::delete('gereja', [ChurchController::class, 'destroy']);
    Route::get('gereja/{id}', [ChurchBranchController::class, 'index'])->name('gereja.show');
    Route::post('gereja/cabang', [ChurchBranchController::class, 'store'])->name('gereja.cabang');
    Route::put('gereja/cabang', [ChurchBranchController::class, 'update']);
    Route::delete('gereja/cabang', [ChurchBranchController::class, 'destroy']);
    Route::get('rencana-baca', [ReadingPlanController::class, 'index'])->name('rencana.baca');
    Route::get('rencana-baca/{id}', [ReadingPlanController::class, 'show'])->name('rencana.baca.detail');
    Route::post('rencana-baca', [ReadingPlanController::class, 'store']);
    Route::put('rencana-baca', [ReadingPlanController::class, 'update']);
    Route::delete('rencana-baca', [ReadingPlanController::class, 'destroy']);
    // Route::delete('rencana-baca', [GroupPlanController::class, 'destroy']);

    // Route::prefix('anggota')->group(function () {
    Route::get('group', [GroupController::class, 'index'])->name('group');
    Route::get('group/create', [GroupController::class, 'create'])->name('group.add');
    Route::post('group', [GroupController::class, 'store']);
    Route::delete('group', [GroupController::class, 'destroy']);
    Route::get('group/{id}', [GroupController::class, 'show'])->name('group.show');
    // Route::get('grup/{id}/edit', [GroupController::class, 'edit'])->name('group.edit');
    // Route::patch('grup/{id}/edit', [GroupController::class, 'update']);
    // Route::patch('group/time', [GroupController::class, 'updatetime']);
    Route::patch('group/admin', [GroupController::class, 'updateadmin']);
    Route::patch('group/todo', [GroupController::class, 'updatetodo']);
    Route::post('group/transfer', [GroupController::class, 'transfer'])->name('transfer.group');

    Route::get('individu', [GroupController::class, 'index2'])->name('group2');
    Route::get('individu/{id}', [GroupController::class, 'show2'])->name('group.show2');
    // });
    Route::prefix('persetujuan')->group(function () {
        Route::get('join', [MemberController::class, 'indexjoin'])->name('join');
        Route::get('leave', [MemberController::class, 'indexleave'])->name('leave');
        Route::get('transfer', [MemberController::class, 'indextf'])->name('transfer');
        Route::post('join', [MemberController::class, 'storejoin']);
        Route::post('leave', [MemberController::class, 'storeleave']);
        Route::post('transfer', [MemberController::class, 'storetf']);
        Route::delete('join', [MemberController::class, 'rejectjoin']);
        Route::delete('leave', [MemberController::class, 'rejectleave']);
        Route::delete('transfer', [MemberController::class, 'rejecttf']);
    });
    Route::prefix('download')->group(function () {
        Route::get('r03/{id}', [DownloadController::class, 'r03'])->name('r03');
        Route::get('member', [DownloadController::class, 'user'])->name('r.member');
        Route::get('strike', [DownloadController::class, 'strike'])->name('r.strike');
        Route::get('overdue', [DownloadController::class, 'overdue'])->name('r.overdue');
    });



    Route::get('pengaturan', [SettingController::class, 'index'])->name('setting');
    Route::prefix('pengaturan')->group(function () {
        Route::get('group', [SettingController::class, 'group'])->name('setting.group');
        Route::get('hak-akses', [PermissionController::class, 'index'])->name('setting.hakakses');
        Route::get('hak-akses/{id}/edit', [PermissionController::class, 'edit'])->name('setting.hakakses.show');
        Route::patch('hak-akses/{id}/edit', [PermissionController::class, 'update']);
    });
    Route::get('profile', [MemberController::class, 'index'])->name('profile');
    Route::patch('profile', [MemberController::class, 'update']);
    Route::patch('profile/change-password', [MemberController::class, 'changepassword'])->name('change.password');


    Route::get('user/add', [UserController::class, 'create']);
    Route::get('user', [UserController::class, 'index'])->name('user');
    Route::post('user', [UserController::class, 'store']);
    Route::patch('user', [UserController::class, 'update']);

    // Route::get('chat', [ChatController::class, 'update'])->name('chat');
    // Route::view('chat/{path?}', 'admin.chat')
    //     ->where('path', '.*');

    Route::get('chat/group', [ChatController::class, 'update'])->name('chat');
    Route::view('chat/group/{path?}', 'admin.chat')
        ->where('path', '.*');
    Route::get('chat/broadcast', [BroadcastController::class, 'index'])->name('broadcast');;
    Route::post('chat/broadcast', [BroadcastController::class, 'store']);

    //json

    Route::post('messages/p', [ChatController::class, 'storep']);
    Route::post('messages/g', [ChatController::class, 'storeg']);
    Route::get('contact/user', [ChatController::class, 'contact']);
    Route::get('contact/group', [ChatController::class, 'group']);
    Route::get('messages/p/{id}', [ChatController::class, 'message']);
    Route::get('messages/g/{id}', [ChatController::class, 'messageGroup']);
    Route::get('user/{id}', [UserController::class, 'show']);
    Route::get('g/{id}', [UserController::class, 'showg']);
    Route::get('auth', [UserController::class, 'auth']);
    // route::post('token', [ProfileController::class, 'storetoken']);
    route::get('notifications', [Notify::class, 'index']);
    route::get('notification/unread', [Notify::class, 'unread']);
    route::post('notification/read/{id}', [Notify::class, 'update']);
    route::post('notification/readall', [Notify::class, 'all']);
    Route::get('t/{id}', [UserController::class, 'test']);
});

// Route::get('contact', [ChatController::class, 'contact']);


// Route::post('/approve/join', [DashboardController::class, 'approve'])->middleware(['auth']);
// Route::post('/approve/leave', [DashboardController::class, 'approveleave'])->middleware(['auth']);

Route::get('/member/data', function () {
    return view('admin.test');
})->middleware(['auth'])->name('member.data');

require __DIR__ . '/auth.php';


// ??????

Route::get('/push-notificaiton', [WebNotificationController::class, 'index'])->name('push-notificaiton');
Route::post('/store-token', [WebNotificationController::class, 'storeToken'])->name('store.token');
Route::post('/send-web-notification', [WebNotificationController::class, 'sendWebNotification'])->name('send.web-notification');

<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class Permissionseeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'pengaturan-hak-akses',
            // 'pengaturan-group',

            'menu-gereja',
            'menu-admin/pic',
            'menu-rencana-baca',

            'persetujuan-join',
            'persetujuan-leave',
            'persetujuan-tansfer',

            'tambah-group',
            // 'edit-pic-group',
            'edit-group',
            'transfer-anggota-group',


        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
    }
}

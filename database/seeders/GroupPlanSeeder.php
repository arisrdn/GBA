<?php

namespace Database\Seeders;

use App\Imports\GroupTodolistImport;
use App\Models\Group;
use App\Models\ReadingPlan;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class GroupPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        ReadingPlan::create([
            'id' => '1',
            'description' => '1 ayat perhari',
            'reading_plan_type_id' => 1
        ]);
        ReadingPlan::create([
            'id' => '2',
            'description' => '1 ayat perhari',
            'reading_plan_type_id' => 2
        ]);
        ReadingPlan::create([
            'id' => '3',
            'description' => '2 ayat perhari',
            'reading_plan_type_id' => 1

        ]);
        ReadingPlan::create([
            'id' => 4,
            'description' => '2 ayat perhari ',
            'reading_plan_type_id' => 2

        ]);

        Excel::import(new GroupTodolistImport(1), public_path('/files/upload.xlsx'));
        Excel::import(new GroupTodolistImport(2), public_path('/files/upload.xlsx'));
        Excel::import(new GroupTodolistImport(3), public_path('/files/upload.xlsx'));
        Excel::import(new GroupTodolistImport(4), public_path('/files/upload.xlsx'));
    }
}

<?php

namespace Database\Seeders;

use App\Models\GroupAdmin;
use App\Models\GroupMember;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $role = Role::create(['name' => 'Admin']);
        $permissions = Permission::pluck('id', 'id')->all();
        $role->givePermissionTo($permissions);
        $usr = Role::create(['name' => 'User']);
        $pic = Role::create(['name' => 'PIC']);
        $pic->givePermissionTo(5, 6, 7, 9, 10);

        //admin
        $faker = Faker::create('id_ID');
        $id = 2;
        $id2 = 1;
        $user = User::create([
            'id' => 1,
            'name' => "admin",
            'gender' => "male",
            'email' => 'adminsystem' . '@mail.com',
            'password' => Hash::make("psiodjsdhjkahdklsahdlajkshdlkajs6&''&(90~=0~0="),
            'whatsapp_no' => $faker->PhoneNumber,
            'province' => "DKI JAKARTA",
            'city' => $faker->randomElement(['jakarta barat', 'jakarta utara', 'jakarta timur', 'jakarta selatan', 'jakarta pusat']),
            'birth_date' => $faker->numberBetween(1990, 2000),
            'country_id' => 16,
            'role_id' => 1,
            // 'regency_id' => $faker->numberBetween(3171, 3175),
        ]);
        $user->assignRole(1);
        for ($i = 1; $i <= 3; $i++) {
            $gender = $faker->randomElement(['male', 'female']);
            $user = User::create([
                'id' => $id,
                'name' => $faker->name($gender),
                'gender' => $gender,
                'email' => 'admin' . $i . '@mail.com',
                'password' => Hash::make("password"),
                'whatsapp_no' => $faker->PhoneNumber,
                'province' => "DKI JAKARTA",
                'city' => $faker->randomElement(['jakarta barat', 'jakarta utara', 'jakarta timur', 'jakarta selatan', 'jakarta pusat']),
                'birth_date' => $faker->numberBetween(1990, 2000),
                'country_id' => 16,
                'role_id' => 1,
                // 'regency_id' => $faker->numberBetween(3171, 3175),
            ]);
            $user->assignRole(1);
            $id++;
        }


        for ($i = 1; $i <= 6; $i++) {
            $gender = $faker->randomElement(['male', 'female']);
            $pic = User::create([
                'id' => $id,
                'name' => "pic" . $faker->name($gender),
                'gender' => $gender,
                'email' => 'pic' . $i . '@mail.com',
                'password' => Hash::make("password"),
                'whatsapp_no' => $faker->PhoneNumber,
                'province' => "DKI JAKARTA",
                'city' => $faker->randomElement(['jakarta barat', 'jakarta utara', 'jakarta timur', 'jakarta selatan', 'jakarta pusat']),
                'birth_date' => $faker->numberBetween(1990, 2000),
                'country_id' => 16,
                'role_id' => 3,
                // 'regency_id' => $faker->numberBetween(3171, 3175),
            ]);
            $pic->assignRole(3);
            $id++;
        }

        GroupAdmin::create([
            'user_id' => '5',
            'group_id' => '1',
            'type' => '0',
        ]);
        groupAdmin::create([
            'user_id' => '6',
            'group_id' => '1',
            'type' => '1',
        ]);
        groupAdmin::create([
            'user_id' => '7',
            'group_id' => '2',
            'type' => '0',

        ]);
        groupAdmin::create([
            'user_id' => '8',
            'group_id' => '2',
            'type' => '1',

        ]);
        groupAdmin::create([
            'user_id' => '9',
            'group_id' => '3',
            'type' => '0',

        ]);
        groupAdmin::create([
            'user_id' => '10',
            'group_id' => '3',
            'type' => '1',

        ]);
        groupAdmin::create([
            'user_id' => '10',
            'group_id' => '4',
            'type' => '0',

        ]);
        groupAdmin::create([
            'user_id' => '5',
            'group_id' => '4',
            'type' => '1',

        ]);
        groupAdmin::create([
            'user_id' => '6',
            'group_id' => '9',
            'type' => '1',

        ]);
        groupAdmin::create([
            'user_id' => '7',
            'group_id' => '9',
            'type' => '1',

        ]);
    }
}

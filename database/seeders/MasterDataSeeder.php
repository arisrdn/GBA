<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\Setting;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MasterDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Role::create([
        //     "id" => 1,
        //     'name' => 'Admin',
        //     'guard_name' => 'web',

        // ]);
        // Role::create([
        //     "id" => 2,
        //     'name' => 'User',
        //     'guard_name' => 'web',

        // ]);
        Setting::create([
            'max_group_user' => '50',
            'max_group_add_user' => '7',
            'max_to_inactive_user' => '30',
            'max_chat_group_show' => '60',

        ]);
        $this->call([
            CountrySeeder::class,
            ProvinceSeeder::class,
            Permissionseeders::class,
            // RoleSeeder::class,
            ReasonSeeder::class,


        ]);
    }
}

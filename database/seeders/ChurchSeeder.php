<?php

namespace Database\Seeders;

use App\Models\Church;
use App\Models\ReadingPlanType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ChurchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Church::create([
            'id' => '1',
            'name' => 'GBI (CK7)',
        ]);
        Church::create([
            'id' => '2',
            'name' => 'Other',
        ]);
        // Church::create([
        //     'id' => '4',
        //     'name' => 'Gereja Isa Almasih - GIA',
        // ]);
        // Church::create([
        //     'id' => '3',
        //     'name' => 'Gereja Bethel Injil Sepenuh - GBIS',
        // ]);
        ReadingPlanType::create([
            'id' => '1',
            'name' => 'Kronologi',
            'short' => 'KR',

        ]);
        ReadingPlanType::create([
            'id' => '2',
            'name' => 'Reguler',
            'short' => 'REG',

        ]);
        ReadingPlanType::create([
            'id' => '3',
            'name' => 'MK',
            'short' => 'MK',

        ]);
    }
}

<?php

namespace Database\Seeders;

use App\Imports\GroupTodolistImport;
use App\Models\Group;
use App\Models\GroupEod;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class GroupDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Group::create([
            'id' => '1',
            'name' => 'GBA-KR1-I',
            'reading_plan_id' => '1',
            'start_date' => Carbon::yesterday()->subMonth(3),
            'end_date' => Carbon::now()->subDay(7),
        ]);
        Group::create([
            'id' => '2',
            'name' => 'GBA-KR1-II',
            'reading_plan_id' => '1',
            'start_date' => Carbon::yesterday()->subMonth(2),
            'end_date' => Carbon::now()->addMonth(1)
        ]);
        Group::create([
            'id' => '3',
            'reading_plan_id' => '2',
            'name' => 'GBA-REG1-I',
            'start_date' => Carbon::yesterday()->subMonth(2),
            'end_date' => Carbon::now()->addMonth(2)
        ]);
        Group::create([
            'id' => '4',
            'reading_plan_id' => '2',
            'name' => 'GBA-REG1-II',
            'start_date' => Carbon::now(),
            'end_date' => Carbon::yesterday()->addMonth(2)
        ]);
        Group::create([
            'id' => '5',
            'reading_plan_id' => '1',
            'name' => 'individu',
            'type' => 'individu',
        ]);
        Group::create([
            'id' => '6',
            'reading_plan_id' => '2',
            'name' => 'individu',
            'type' => 'individu',
        ]);
        Group::create([
            'id' => '7',
            'reading_plan_id' => '3',
            'name' => 'individu',
            'type' => 'individu',
        ]);
        Group::create([
            'id' => '8',
            'reading_plan_id' => '4',
            'name' => 'individu',
            'type' => 'individu',
        ]);
        Group::create([
            'reading_plan_id' => '1',
            'name' => 'GBA-KR1-III',
            'start_date' => Carbon::yesterday(),
            'end_date' => Carbon::now()->addMonth(2)
        ]);
    }
}

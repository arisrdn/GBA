<?php

namespace Database\Seeders;

use App\Models\GroupMember;
use App\Models\GroupTodolist;
use App\Models\History;
use App\Models\MemberTodolist;
use App\Models\ReadingPlanList;
use App\Models\User;
use App\Models\WaitingList;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;

class MemeberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //g1
        $faker = Faker::create('id_ID');
        $id = 11;
        $id2 = 1;
        // User join group1 id 11-15
        for ($i = 1; $i <= 5; $i++) {
            $gender = $faker->randomElement(['male', 'female']);
            User::create([
                'id' => $id,
                'name' =>  $faker->name($gender),
                'gender' => $gender,
                'email' => 'userg1-' . $i . '@mail.com',
                'password' => Hash::make("password"),
                'whatsapp_no' => "85" . $faker->numberBetween(00000001, 99999999),
                'province' => "DKI JAKARTA",
                'city' => $faker->randomElement(['jakarta barat', 'jakarta utara', 'jakarta timur', 'jakarta selatan', 'jakarta pusat']),
                'birth_date' => $faker->numberBetween(1990, 2000),
                // 'regency_id' => $faker->numberBetween(3171, 3175),
                'country_id' => 102,
                'role_id' => 2,
                'church_branch_id' =>  $faker->numberBetween(1, 4),
                'email_verified_at' => now()->subMonth(),
            ]);
            $gm  = GroupMember::create([
                'id' => $id2,
                'user_id' => $id,
                'group_id' => '1',
                'approved_at' => Carbon::yesterday()->subMonth(4),
                'created_at' => Carbon::yesterday()->subMonth(4),
            ]);
            $data = ReadingPlanList::where("reading_plan_id", 1)->get();
            $day = 1;
            foreach ($data as  $val) {
                // echo $value->day;
                $read = Carbon::yesterday()->subMonth(4)->addDays($day);
                if ($read > now()) {
                    # code...
                    $read = null;
                }

                MemberTodolist::create([
                    'group_member_id' => $id - 10,
                    'reading_plan_list_id' => $val->id,
                    'read_at' => $read,
                    'chapter_verse' => $val->chapter_verse,
                    'schedule' => Carbon::yesterday()->subMonth()->addDays($val->day)
                ]);
                $day++;
            }

            $strike = 0;
            $check = GroupMember::find($gm->id);
            $todo = $check->todo->where("read_at", "!=", null);
            for ($a = 0; $a < $todo->count(); $a++) {
                // for ($i = 0; $i < 5; $i++) {
                if ($a == 0) {
                    $strike = 1;
                } else {

                    // if (date('d-m-Y', strtotime($da->todo[$i]->read_at)) == date('d-m-Y', strtotime($da->todo[$i - 1]->read_at->addday(1))) || date('d-m-Y', strtotime($da->todo[$i]->read_at)) == date('d-m-Y', strtotime($da->todo[$i - 1]->read_at))) {
                    if ($todo[$a]->read_at == $todo[$a - 1]->read_at->addday(1)) {
                        // echo $da->todo[$i]->id . 'tambah^';
                        $strike = $strike + 1;
                    } else {
                        $strike = 1;
                    }
                }
            }
            $check->update(['strike' => $strike]);
            $id++;
            $id2++;
        }
        // User join group3 id 15-20
        for ($i = 1; $i <= 5; $i++) {
            $gender = $faker->randomElement(['male', 'female']);
            User::create([
                'id' => $id,
                'name' =>  $faker->name($gender),
                'email' => 'userg3-' . $i . '@mail.com',
                'password' => Hash::make("password"),
                'whatsapp_no' => "85" . $faker->numberBetween(00000001, 99999999),
                'gender' => $gender,
                'province' => "DKI JAKARTA",
                'city' => $faker->randomElement(['jakarta barat', 'jakarta utara', 'jakarta timur', 'jakarta selatan', 'jakarta pusat']),
                'birth_date' => $faker->numberBetween(1990, 2000),
                'country_id' => 102,
                'role_id' => 2,
                'church_branch_id' =>  $faker->numberBetween(1, 4),
                'email_verified_at' => now()->subMonth(),
                // 'regency_id' => $faker->numberBetween(3171, 3175),

            ]);
            $gm = GroupMember::create([
                'id' => $id2,
                'user_id' => $id,
                'group_id' => '3',
                'approved_at' => now(),
                'created_at' => Carbon::yesterday()->subMonth(),
            ]);
            $data = ReadingPlanList::where("reading_plan_id", 1)->get();
            $day = 1;
            foreach ($data as  $val) {
                // echo $value->day;
                $read =   now()->subMonth()->addDays($day);
                if ($read > now()->subDays(rand(1, 5))) {
                    # code...
                    $read = null;
                }
                MemberTodolist::create([
                    'group_member_id' => $id - 10,
                    'reading_plan_list_id' => $val->id,
                    'read_at' => $read,
                    'chapter_verse' => $val->chapter_verse,
                    'schedule' => Carbon::yesterday()->subMonth()->addDays($val->day)
                ]);


                $day++;
            }
            $check = GroupMember::find($gm->id);
            $todo = $check->todo->where("read_at", "!=", null);
            $check->update(['strike' => $todo->count()]);

            $id++;
            $id2++;
        }

        // User join group4 id 20-25
        for ($i = 1; $i <= 5; $i++) {
            $gender = $faker->randomElement(['male', 'female']);
            User::create([
                'id' => $id,
                'name' =>  $faker->name($gender),
                'email' => 'userg4-' . $i . '@mail.com',
                'password' => Hash::make("password"),
                'whatsapp_no' => "85" . $faker->numberBetween(00000001, 999999999),
                'gender' => $gender,
                'province' => "DKI JAKARTA",
                'city' => $faker->randomElement(['jakarta barat', 'jakarta utara', 'jakarta timur', 'jakarta selatan', 'jakarta pusat']),

                'birth_date' => $faker->numberBetween(1990, 2000),
                'country_id' => 102,
                'role_id' => 2,
                'church_branch_id' =>  $faker->numberBetween(1, 4),
                'email_verified_at' => now()->subMonth(),
                // 'regency_id' => $faker->numberBetween(3171, 3175),

            ]);
            $gm = GroupMember::create([
                'id' => $id2,
                'user_id' => $id,
                'group_id' => '4',
                'complete_at' => now(),
                'approved_at' => now(),
                'reason_leave_id' => '1',
                'note' => 'pindah',
                'created_at' => Carbon::yesterday()->subMonth(),
            ]);
            $data = ReadingPlanList::where("reading_plan_id", 1)->get();
            $day = 1;
            foreach ($data as  $val) {
                $read =   now()->subMonth()->addDays($day);
                if ($read > now()->subDays(rand(1, 5))) {
                    $read = null;
                }
                MemberTodolist::create([
                    'group_member_id' => $id - 10,
                    'reading_plan_list_id' => $val->id,
                    'read_at' => $read,
                    'chapter_verse' => $val->chapter_verse,
                    'schedule' => Carbon::yesterday()->subMonth()->addDays($val->day)
                ]);


                $day++;
            }
            $check = GroupMember::find($gm->id);
            $todo = $check->todo->where("read_at", "!=", null);
            $check->update(['strike' => $todo->count()]);

            $id++;
            $id2++;
        }
        // User join group2 id 25-75
        for ($i = 1; $i <= 50; $i++) {
            $gender = $faker->randomElement(['male', 'female']);
            User::create([
                'id' => $id,
                'name' =>  $faker->name($gender),
                'email' => 'userg2-' . $i . '@mail.com',
                'password' => Hash::make("password"),
                'whatsapp_no' => "85" . $faker->numberBetween(00000001, 99999999),
                'gender' => $gender,
                'province' => "DKI JAKARTA",
                'city' => $faker->randomElement(['jakarta barat', 'jakarta utara', 'jakarta timur', 'jakarta selatan', 'jakarta pusat']),
                'birth_date' => $faker->numberBetween(1990, 2000),
                'country_id' => 102,
                'role_id' => 2,
                'church_branch_id' =>  $faker->numberBetween(1, 4),
                'email_verified_at' => now()->subMonth(),
                // 'regency_id' => $faker->numberBetween(3171, 3175),

            ]);
            $gm = GroupMember::create([
                'id' => $id2,
                'user_id' => $id,
                'group_id' => '2',
                'complete_at' => now(),
                'approved_at' => now(),
                'reason_leave_id' => '1',
                'note' => 'pindah',
                'created_at' => Carbon::yesterday()->subMonth(),
            ]);
            $data = ReadingPlanList::where("reading_plan_id", 1)->get();
            $day = 1;
            foreach ($data as  $val) {
                $read =   now()->subMonth()->addDays($day);
                if ($read > now()->subDays(rand(1, 5))) {
                    $read = null;
                }
                MemberTodolist::create([
                    'group_member_id' => $id - 10,
                    'reading_plan_list_id' => $val->id,
                    'read_at' => $read,
                    'chapter_verse' => $val->chapter_verse,
                    'schedule' => Carbon::yesterday()->subMonth()->addDays($val->day)
                ]);


                $day++;
            }
            $check = GroupMember::find($gm->id);
            $todo = $check->todo->where("read_at", "!=", null);
            $check->update(['strike' => $todo->count()]);
            $id++;
            $id2++;
        }

        // user approved 75-80
        for ($i = 1; $i <= 5; $i++) {
            $gender = $faker->randomElement(['male', 'female']);
            User::create([
                'id' => $id,
                'name' =>  $faker->name($gender),
                'email' => 'user' . $i . '@mail.com',
                'password' => Hash::make("password"),
                'whatsapp_no' => "85" . $faker->numberBetween(00000001, 99999999),
                'gender' => $gender,
                'province' => "DKI JAKARTA",
                'city' => $faker->randomElement(['jakarta barat', 'jakarta utara', 'jakarta timur', 'jakarta selatan', 'jakarta pusat']),
                'birth_date' => $faker->numberBetween(1990, 2000),
                'country_id' => 102,
                'role_id' => 2,
                'church_branch_id' =>  $faker->numberBetween(1, 4),
                'email_verified_at' => now()->subMonth(),
                // 'regency_id' => $faker->numberBetween(3171, 3175),
            ]);
            $id++;
        }

        // waiting list join
        $idw = 1;
        // User join group2 id 80-85
        for ($i = 1; $i <= 5; $i++) {
            $gender = $faker->randomElement(['male', 'female']);
            User::create([
                'id' => $id,
                'name' =>  $faker->name($gender),
                'email' => 'userjoin' . $i . '@mail.com',
                'password' => Hash::make("password"),
                'whatsapp_no' => "85" . $faker->numberBetween(00000001, 99999999),
                'gender' => $gender,
                'province' => "DKI JAKARTA",
                'city' => $faker->randomElement(['jakarta barat', 'jakarta utara', 'jakarta timur', 'jakarta selatan', 'jakarta pusat']),
                'birth_date' => $faker->numberBetween(1990, 2000),
                'country_id' => 102,
                'role_id' => 2,
                'church_branch_id' =>  $faker->numberBetween(1, 4),
                'email_verified_at' => now()->subMonth(),
                // 'regency_id' => $faker->numberBetween(3171, 3175),

            ]);
            $dt = [
                'read_option' => "group",

            ];
            WaitingList::create([
                'user_id' => $id,
                'type' => "join",
                'data' => json_encode($dt),
                'reading_plan_id' => $faker->numberBetween(1, 3),
            ]);

            $id++;
        }
        // join2
        // User join group2 id 85-90
        for ($i = 1; $i <= 5; $i++) {
            $gender = $faker->randomElement(['male', 'female']);
            User::create([
                'id' => $id,
                'name' =>  "2" . $faker->name($gender),
                'email' => 'userjoin2' . $i . '@mail.com',
                'password' => Hash::make("password"),
                'whatsapp_no' => "85" . $faker->numberBetween(00000001, 99999999),
                'gender' => $gender,
                'province' => "DKI JAKARTA",
                'city' => $faker->randomElement(['jakarta barat', 'jakarta utara', 'jakarta timur', 'jakarta selatan', 'jakarta pusat']),
                'birth_date' => $faker->numberBetween(1990, 2000),
                'country_id' => 102,
                'role_id' => 2,
                'church_branch_id' => 4,
                'email_verified_at' => now()->subMonth(),
                // 'regency_id' => $faker->numberBetween(3171, 3175),

            ]);
            $dt = [
                'read_option' => "group",

            ];
            WaitingList::create([
                'user_id' => $id,
                'type' => "join",
                'data' => json_encode($dt),
                'reading_plan_id' => $faker->numberBetween(1, 3),
            ]);

            $id++;
        }
        // leave id 90-95
        for ($i = 1; $i <= 5; $i++) {
            $gender = $faker->randomElement(['male', 'female']);
            User::create([
                'id' => $id,
                'name' =>  $faker->name($gender),
                'email' => 'userleave' . $i . '@mail.com',
                'password' => Hash::make("password"),
                'whatsapp_no' => "85" . $faker->numberBetween(00000001, 99999999),
                'gender' => $gender,
                'province' => "DKI JAKARTA",
                'city' => $faker->randomElement(['jakarta barat', 'jakarta utara', 'jakarta timur', 'jakarta selatan', 'jakarta pusat']),
                'birth_date' => $faker->numberBetween(1990, 2000),
                'country_id' => 102,
                'role_id' => 2,
                'church_branch_id' => 2,
                'email_verified_at' => now()->subMonth(),
                // 'regency_id' => $faker->numberBetween(3171, 3175),

            ]);
            $gm  = GroupMember::create([
                'id' => $id2,
                'user_id' => $id,
                'group_id' => '4',
                'complete_at' => now(),
                'approved_at' => now(),
                'reason_leave_id' => '1',
                'note' => 'pindah',
                'created_at' => Carbon::yesterday()->subMonth(),
            ]);
            $data = ReadingPlanList::where("reading_plan_id", 1)->get();
            $day = 1;
            foreach ($data as  $val) {
                $read =   now()->subMonth()->addDays($day);
                if ($read > now()->subDays(rand(1, 5))) {
                    $read = null;
                }
                MemberTodolist::create([
                    'group_member_id' => $gm->id,
                    'reading_plan_list_id' => $val->id,
                    'read_at' => $read,
                    'chapter_verse' => $val->chapter_verse,
                    'schedule' => Carbon::yesterday()->subMonth()->addDays($val->day)
                ]);


                $day++;
            }
            $dt = array('note' => $faker->text(20));
            $data = WaitingList::create([
                'user_id' => $id,
                'type' => "leave",
                'data' => json_encode($dt),
                'reason_leave_id' => $faker->numberBetween(1, 2),
            ]);

            $id++;
            $id2++;
        }
        // tf
        for ($i = 1; $i <= 5; $i++) {
            $gender = $faker->randomElement(['male', 'female']);
            User::create([
                'id' => $id,
                'name' =>  $faker->name($gender),
                'gender' => $gender,
                'email' => 'usertf' . $i . '@mail.com',
                'password' => Hash::make("password"),
                'whatsapp_no' => "85" . $faker->numberBetween(00000001, 99999999),
                'province' => "DKI JAKARTA",
                'city' => $faker->randomElement(['jakarta barat', 'jakarta utara', 'jakarta timur', 'jakarta selatan', 'jakarta pusat']),
                'birth_date' => $faker->numberBetween(1990, 2000),
                // 'regency_id' => $faker->numberBetween(3171, 3175),
                'country_id' => 102,
                'role_id' => 2,
                'church_branch_id' => 3,
                'email_verified_at' => now()->subMonth(),
            ]);
            $gm =  GroupMember::create([
                'id' => $id2,
                'user_id' => $id,
                'group_id' => '1',
                'approved_at' => Carbon::yesterday()->subMonth(3),
                'created_at' => Carbon::yesterday()->subMonth(3),
            ]);
            $data = ReadingPlanList::where("reading_plan_id", 1)->get();
            $day = 1;
            foreach ($data as  $val) {
                // echo $value->day;
                $read =   now()->subMonth()->addDays($day);
                if ($read > now()->subDays(rand(1, 5))) {
                    # code...
                    $read = null;
                }
                MemberTodolist::create([
                    'group_member_id' => $gm->id,
                    'reading_plan_list_id' => $val->id,
                    'read_at' => $read,
                    'chapter_verse' => $val->chapter_verse,
                    'schedule' => Carbon::yesterday()->subMonth()->addDays($val->day)
                ]);


                $day++;
            }


            $data = WaitingList::create([
                'user_id' => $id,
                'type' => "transfer",
                'data' => $faker->numberBetween(0, 1),
            ]);

            $check = GroupMember::find($gm->id);
            $todo = $check->todo->where("read_at", "!=", null);
            $check->update(['strike' => $todo->count()]);
            $id++;
            $id2++;
        }


        // individu
        for ($i = 1; $i <= 50; $i++) {
            $gender = $faker->randomElement(['male', 'female']);
            $user =   User::create([
                'name' =>  $faker->name($gender),
                'email' => 'useri' . $i . '@mail.com',
                'password' => Hash::make("password"),
                'whatsapp_no' => "85" . $faker->numberBetween(00000001, 99999999),
                'gender' => $gender,
                'province' => "DKI JAKARTA",
                'city' => $faker->randomElement(['jakarta barat', 'jakarta utara', 'jakarta timur', 'jakarta selatan', 'jakarta pusat']),
                'birth_date' => $faker->numberBetween(1990, 2000),
                'country_id' => 102,
                'role_id' => 2,
                'church_branch_id' =>  $faker->numberBetween(1, 4),
                'email_verified_at' => now()->subMonth(),
                // 'regency_id' => $faker->numberBetween(3171, 3175),

            ]);
            $gm = GroupMember::create([

                'user_id' => $user->id,
                'group_id' => $faker->numberBetween(5, 8),
                'complete_at' => now(),
                'approved_at' => now(),
                'created_at' => Carbon::yesterday()->subMonth(3),
            ]);
            $data = ReadingPlanList::where("reading_plan_id", 1)->get();
            $day = 1;
            foreach ($data as  $val) {
                $read = Carbon::yesterday()->subMonth(2)->addDays($day);
                if ($read > now()->subDays(rand(1, 5))) {
                    $read = null;
                }
                MemberTodolist::create([
                    'group_member_id' => $gm->id,
                    'reading_plan_list_id' => $val->id,
                    'read_at' => $read,
                    'chapter_verse' => $val->chapter_verse,
                    'schedule' => Carbon::yesterday()->subMonth()->addDays($val->day)
                ]);


                $day++;
            }
            $check = GroupMember::find($gm->id);
            $todo = $check->todo->where("read_at", "!=", null);
            $check->update(['strike' => $todo->count()]);
            $id++;
        }

        for ($i = 1; $i <= 3; $i++) {
            $gender = $faker->randomElement(['male', 'female']);
            $user =   User::create([
                'name' =>  $faker->name($gender),
                'email' => 'userh' . $i . '@mail.com',
                'password' => Hash::make("password"),
                'whatsapp_no' => "85" . $faker->numberBetween(00000001, 99999999),
                'gender' => $gender,
                'province' => "DKI JAKARTA",
                'city' => $faker->randomElement(['jakarta barat', 'jakarta utara', 'jakarta timur', 'jakarta selatan', 'jakarta pusat']),
                'birth_date' => $faker->numberBetween(1990, 2000),
                'country_id' => 102,
                'role_id' => 2,
                'church_branch_id' =>  $faker->numberBetween(1, 4),
                'email_verified_at' => now()->subMonth(),
                // 'regency_id' => $faker->numberBetween(3171, 3175),

            ]);
            $hs = History::create([
                'user_id' => $user->id,
                'group_id' => 5,
                'group_name' => "Individu",
                'priode_start' => null,
                'priode_end' => null,
                'reading_plan_id' => 1,
                'reading_plan_description' => "1 ayat perhari",
                'reading_plan_count' => 130,
                'readed' => 99,
                'strike' => 20,
                'status' => "leave"
            ]);
            $gm = GroupMember::create([
                'user_id' => $user->id,
                'group_id' => '4',
                'approved_at' => now(),
                'created_at' => Carbon::yesterday()->subMonth(),
            ]);
            $data = ReadingPlanList::where("reading_plan_id", 1)->get();
            $day = 1;
            foreach ($data as  $val) {
                $read =   now()->subMonth()->addDays($day);
                if ($read > now()->subDays(rand(1, 5))) {
                    $read = null;
                }
                MemberTodolist::create([
                    'group_member_id' =>  $gm->id,
                    'reading_plan_list_id' => $val->id,
                    'read_at' => $read,
                    'chapter_verse' => $val->chapter_verse,
                    'schedule' => Carbon::yesterday()->subMonth()->addDays($val->day)
                ]);


                $day++;
            }
            $check = GroupMember::find($gm->id);
            $todo = $check->todo->where("read_at", "!=", null);
            $check->update(['strike' => $todo->count()]);

            $id++;
            $id2++;
        }
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->integer('max_group_user')->nullable();
            $table->integer('max_group_add_user')->nullable();
            $table->integer('max_to_inactive_user')->nullable();
            $table->integer('max_chat_group_show')->nullable();
            // $table->string('can_approve_join')->nullable();
            // $table->string('can_approve_leave')->nullable();
            // $table->string('can_approve_transfer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
};

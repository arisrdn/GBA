<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_todolists', function (Blueprint $table) {
            $table->id();
            $table->string('read_at')->nullable();
            $table->string('schedule')->nullable();
            $table->string('chapter_verse')->nullable();
            $table->foreignId('reading_plan_list_id');
            $table->foreignId('group_member_id')
                ->references('id')->on('group_members')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_todolists');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('histories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('group_id');
            $table->string('group_name')->nullable();
            $table->date('priode_start')->nullable();
            $table->date('priode_end')->nullable();
            $table->foreignId('reading_plan_id')->nullable();
            $table->string('reading_plan_description')->nullable();
            $table->integer('reading_plan_count')->nullable();
            $table->integer('readed')->nullable();
            $table->integer('strike')->default(0);
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('histories');
    }
};

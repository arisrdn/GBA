@extends('layouts.admin-main')

@section('title')
    Persetujuan
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Persetujuan</h1>
            @include('layouts.partials.breadcums')
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col">
                    @include('layouts.partials.flash-message')
                    <div class="card">
                        <div class="card-header">
                            <h4>Setujui Keluar Group</h4>
                            <div class="card-header-action">

                                {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addChurch">
                                    Tambah Gereja
                                </button> --}}

                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table-striped table" id="table-church">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Group </th>
                                            <th>Reason </th>
                                            <th>Catatan </th>
                                            <th>Request leave</th>
                                            <th>Action</th>
                                        </tr>

                                    </thead>
                                    <tbody>
                                        @foreach ($member as $k => $dat)
                                            <tr>
                                                <td>
                                                    {{ $k + 1 }}</td>
                                                <td>{{ $dat->user->name }}</td>
                                                <td>{{ $dat->user->memberactive->group->name }}</td>
                                                <td>{{ $dat->reason->reason }}</td>
                                                @php
                                                    $a = json_decode($dat->data);
                                                @endphp
                                                <td>{{ $a->note }}</td>
                                                <td>{{ $dat->updated_at }}</td>
                                                <td>
                                                    {{-- <form method="post" action="{{ route('leave') }}"
                                                        enctype="multipart/form-data">
                                                        {{ csrf_field() }}

                                                        <input type="hidden" id="custId" name="wl_id"
                                                            value="{{ $dat->id }} ">
                                                        <input type="hidden" id="custId" name="gm_id"
                                                            value="{{ $dat->user->memberactive->id }} ">
                                                        <button type="submit" class="btn btn-danger">approve</button>
                                                    </form>
                                                    <a href="#" id="btn-reject" data-toggle="modal"
                                                        data-target="#Modaldelete" data-wl="{{ $dat }}"
                                                        data-name="{{ $dat->user->name }}" class="btn btn-warning">
                                                        Reject</a> --}}
                                                    <a href="#" id="btn-approve" data-toggle="modal"
                                                        data-wl="{{ $dat->id }}"
                                                        data-gm="{{ $dat->user->memberactive->id }}"
                                                        data-reason="{{ $dat->reason->reason }}"
                                                        data-note="{{ $a->note }}" data-name="{{ $dat->user->name }}"
                                                        data-target="#modalapprove" class="btn btn-primary">
                                                        Approve</a>
                                                    <a href="#" id="btn-reject" data-toggle="modal"
                                                        data-target="#Modaldelete" data-wl="{{ $dat }}"
                                                        data-name="{{ $dat->user->name }}" class="btn btn-warning">
                                                        Reject</a>
                                                </td>
                                            </tr>
                                        @endforeach

                                        <input type="hidden" id="custId" name="custId" value="3487">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </section>
    <div class="modal fade" id="modalapprove" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <form action="{{ route('leave') }}" method="POST">
            @csrf
            @method('post')
            <div class="modal-dialog" role="document">

                <input type="hidden" id="wl_id" name="wl_id" value="">
                <input type="hidden" id="gm_id" name="gm_id" value="">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="edit">Approve User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row mb-3">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Nama</label>
                            <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext" id="name" value="">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Alasan Keluar</label>
                            <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext" id="data_reason"
                                    value="">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Catatan</label>
                            <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext" id="data_note" value="">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Approve </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal fade" id="Modaldelete" tabindex="-1" role="dialog" aria-labelledby="delete" aria-hidden="true">
        <form action="{{ route('join') }}" method="POST">
            @csrf
            @method('DELETE')
            <div class="modal-dialog" role="document">

                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="delete">Reject data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            Yakin akan di Reject ?
                            <input type="hidden" name="reject_id" class="form-control" required id="reject-id">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger btn-shadow">Ya</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@section('plugin_css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">
@endsection
@section('plugin_js')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function() {
            setTimeout(function() {
                $(".alert").alert('close');
            }, 2000);
        });

        $("#table-church").dataTable({
            "columnDefs": [{

            }]
        });
    </script>

    <script type="text/javascript">
        $(document).on("click", "#btn-reject", function() {
            var itemid = $(this).attr('data-id');
            var item = JSON.parse($(this).attr('data-wl'))
            $('#reject-id').val(item.id);
            $('#delete-name').val(itemname);
            console.log(itemid, itemname);
        });
        $(document).on("click", "#btn-approve", function() {
            var itemname = $(this).attr('data-name');
            var itemwl = $(this).attr('data-wl');
            var itemgm = $(this).attr('data-dm');
            var itemreason = $(this).attr('data-reason');
            var itemnote = $(this).attr('data-note');
            $('#name').val(itemname);


            $('#gm_id').val(itemgm);
            $('#wl_id').val(itemwl);
            $('#data_note').val(itemnote);
            $('#data_reason').val(itemreason);



        });
    </script>
@endsection
@endsection

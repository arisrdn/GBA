@extends('layouts.admin-main')

@section('title')
    Dashboard
@endsection

@section('content')
    <section class="section">

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="card card-statistic-2">
                    <div class="card-stats">
                        <div class="card-stats-title mt-3">
                        </div>
                        <div class="card-stats-items">
                            @php
                                $totaluser = 0;
                                $totalmember = 0;
                                $r = '';
                            @endphp
                            @foreach ($user as $role)
                                @if ($role->name == 'User')
                                    @php
                                        $totalmember = $totalmember + $role->user_count;
                                        $r = 'Member';
                                    @endphp
                                @else
                                    @php
                                        $totaluser = $totaluser + $role->user_count;
                                        $r = $role->name;
                                    @endphp
                                @endif

                                <div class="card-stats-item col">
                                    <div class="card-stats-item-count">{{ $role->user_count }}</div>
                                    <div class="card-stats-item-label">{{ $r }}</div>
                                </div>
                            @endforeach


                        </div>
                    </div>
                    <div class="card-icon shadow-primary bg-primary">
                        <i class="fas fa-user-cog"></i>

                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>USER</h4>
                        </div>
                        <div class="card-body">
                            {{ $totaluser + $totalmember }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="card card-statistic-2">
                    <div class="card-stats">
                        <div class="card-stats-title mt-3">
                        </div>
                        <div class="card-stats-items">
                            <div class="card-stats-item">
                                <div class="card-stats-item-count">{{ $wl->where('type', 'join')->count() }}</div>
                                <div class="card-stats-item-label">Request Join</div>
                            </div>
                            <div class="card-stats-item">
                                <div class="card-stats-item-count">{{ $wl->where('type', 'leave')->count() }}</div>
                                <div class="card-stats-item-label">Request Leave</div>
                            </div>
                            <div class="card-stats-item">
                                <div class="card-stats-item-count">{{ $wl->where('type', 'transfer')->count() }}</div>
                                <div class="card-stats-item-label">Transfer</div>
                            </div>
                        </div>
                    </div>
                    <div class="card-icon shadow-warning bg-warning">
                        <i class="fas fa-list"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Permintaan</h4>
                        </div>
                        <div class="card-body">
                            {{ $wl->count() }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="card card-statistic-2">
                    <div class="card-stats">
                        <div class="card-stats-title mt-3">
                        </div>
                        <div class="card-stats-items">
                            <div class="card-stats-item col">
                                <div class="card-stats-item-count">{{ $group['active'] }}</div>
                                <div class="card-stats-item-label">Group Active</div>
                            </div>

                            <div class="card-stats-item col">
                                <div class="card-stats-item-count">{{ $group['inactive'] }}</div>
                                <div class="card-stats-item-label">Group Inactive</div>
                            </div>

                        </div>
                    </div>
                    <div class="card-icon shadow-success bg-success">
                        <i class="fas fa-users"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Group</h4>
                        </div>
                        <div class="card-body">
                            {{ $group['active'] + $group['inactive'] }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.partials.flash-message')

        <div class="row">

            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4> member Gender Composition
                        </h4>
                        <div class="card-header-action">
                            <a class="btn btn-primary" href="{{ route('r.member') }}">
                                Download member list
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <canvas id="gender" width="200" height="200"></canvas>
                    </div>
                </div>
            </div>
            {{-- location --}}
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4>Gereja </h4>
                    </div>
                    <div class="card-body">
                        <canvas id="location" width="200" height="200"></canvas>
                    </div>
                </div>
            </div>
            {{-- overdue --}}
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4>MEMBER LIST WITH OVERDUE > 3 DAYS </h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table-striped table-data table">
                                <thead>
                                    <tr>
                                        <th class="text-center">
                                            NO
                                        </th>
                                        <th>Nama</th>
                                        <th>Nama Group</th>
                                        <th>Overdue (Days)</th>
                                        <th>WA No</th>

                                    </tr>
                                </thead>
                                <tbody>


                                    @foreach ($member as $key => $dat)
                                        @if ($dat->overdue->d >= 3)
                                            <tr>
                                                <td>
                                                    {{ $key + 1 }}</td>
                                                <td>{{ $dat->user->name }}</td>
                                                <td>{{ $dat->group->name }}</td>
                                                <td>{{ $dat->overdue->d }}</td>
                                                <td>{{ '(+' . $dat->user->country->phone_code . ')' . $dat->user->whatsapp_no }}
                                                </td>

                                            </tr>
                                        @endif
                                    @endforeach



                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            {{-- strike --}}
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4>Top Strike </h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table-striped table-data table">
                                <thead>
                                    <tr>
                                        <th class="text-center">
                                            NO
                                        </th>
                                        <th>Nama</th>
                                        <th>Nama Group</th>
                                        <th>Strike</th>
                                        <th>#PERFECT WEEK</th>

                                    </tr>
                                </thead>
                                <tbody>

                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($member->sortByDesc('strike')->take(20) as $dat)
                                        @if ($dat->overdue->d <= 1)
                                            <tr>
                                                <td>
                                                    {{ $no++ }}</td>
                                                <td>{{ $dat->user->name }}</td>
                                                <td>{{ $dat->group->name }}</td>
                                                <td>{{ $dat->strike }}</td>
                                                <td>{{ ceil($dat->strike / 7) }}
                                                </td>

                                            </tr>
                                        @endif
                                    @endforeach



                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            {{-- GBA - Gender CompositionR-01-C --}}
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4>GBA - Gender Composition R-01-C
                        </h4>
                    </div>
                    <div class="card-body">
                        <canvas id="myChart" width="200" height="200"></canvas>
                    </div>
                </div>
            </div>
            {{-- GBA Composition (# of Group) R-01-B --}}
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4>GBA Composition (# of Group) R-01-B
                        </h4>
                    </div>
                    <div class="card-body">
                        <canvas id="R-01-B" width="200" height="200"></canvas>
                    </div>
                </div>
            </div>
            {{-- BA Composition (# of Member) R-01-D --}}
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4>GBA Composition (# of Member) R-01-D
                        </h4>
                    </div>
                    <div class="card-body">
                        <canvas id="R-01-D" width="200" height="200"></canvas>
                    </div>
                </div>
            </div>
            {{-- GBA - BRANCH COMPOSITION R-01-E --}}
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4>GBA - BRANCH COMPOSITION R-01-E
                        </h4>
                    </div>
                    <div class="card-body">
                        <canvas id="R01E" width="200" height="200"></canvas>
                    </div>
                </div>
            </div>

        </div>

    </section>

    @php
        
        $lcount = [];
        $lname = [];
        foreach ($gereja as $loc) {
            // print_r($loc);
            array_push($lname, $loc['name']);
            array_push($lcount, $loc['user_count']);
        }
        
        $lcount4 = [];
        $lname4 = [];
        foreach ($plan as $loc) {
            // print_r($loc);
            array_push($lname4, $loc->description);
            array_push($lcount4, $loc->group->count());
        }
        $lcount5 = [];
        $lname5 = [];
        foreach ($plan as $loc) {
            // print_r($loc);
            array_push($lname5, $loc->description);
            array_push($lcount5, $loc->member_count);
        }
        $lcount61 = [];
        $lcount62 = [];
        $lcount6 = [];
        $lname6 = [];
        foreach ($branch as $val) {
            array_push($lname6, $val->name);
            array_push($lcount6, $val->user_count);
            array_push($lcount61, $val->useractive);
            array_push($lcount62, $val->userinactive);
            // foreach ($val as $loc) {
            //     // array_push($lcount6, $loc->user->count);
            // }
        }
    @endphp


@section('plugin_css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">
@endsection
@section('plugin_js')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function() {
            setTimeout(function() {
                $(".alert").alert('close');
            }, 2000);
        });

        $(".table-data").dataTable({
            "columnDefs": [{
                // "sortable": false,
                // "targets": [2, 3]
            }]
        });
    </script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.js"
        integrity="sha512-oaUGh3C8smdaT0kMeyQ7xS1UY60lko23ZRSnRljkh2cbB7GJHZjqe3novnhSNc+Qj21dwBE5dFBqhcUrFc9xIw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script> --}}

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.9.1/chart.min.js"
        integrity="sha512-ElRFoEQdI5Ht6kZvyzXhYG9NqjtkmlkfYk0wr6wHxU9JEHakS7UJZNeml5ALk+8IKlU6jDgMabC3vkumRokgJA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script> --}}
    {{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
    <script type="text/javascript"
        src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.4.0/dist/chartjs-plugin-datalabels.min.js"></script> --}}
    {{-- <script>
        var ctx1 = document.getElementById("balance-chart");

        

        var app = <?php echo json_encode($datagroup); ?>;
        var lable = [];
        var datagroup = [];
        var color = [];
        app.forEach(element => {
            lable.push(element.name)
            datagroup.push(element.total_member_count)
            color.push('rgba(54, 162, 235, 1)')
        });
        console.log(app);

        var ctx = document.getElementById("myChart");
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: lable,
                datasets: [{
                    label: '# of Votes',
                    data: datagroup,
                    backgroundColor: color
                }]
            },
            options: {
                legend: {
                    display: false,
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script> --}}
    {{-- <script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.0/dist/chart.min.js"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.9.1/dist/chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>

    <script>
        function dynamicColors() {
            var r = Math.floor(Math.random() * 255);
            var g = Math.floor(Math.random() * 255);
            var b = Math.floor(Math.random() * 255);
            return "rgba(" + r + "," + g + "," + b + ", 0.5)";
        }

        function poolColors(a) {
            var pool = [];
            for (i = 0; i < a; i++) {
                pool.push(dynamicColors());
            }
            return pool;
        }
        Chart.register(ChartDataLabels)
        Chart.defaults.set('plugins.datalabels', {
            color: '#fff'
        });

        var barChartData = {
            labels: [
                "ck7",
                "Other",

            ],
            datasets: [{
                    label: "male",
                    // backgroundColor: poolColors(1),
                    backgroundColor: '#0278a3',

                    // borderColor: "red",
                    borderWidth: 1,
                    data: ["{{ $composition[0]->sum('user_male_count') }}",
                        "{{ $composition[1]->sum('user_male_count') }}"
                    ]
                },
                {
                    label: "female",
                    // backgroundColor: poolColors(1),
                    backgroundColor: '#ffa426',
                    // borderColor: "blue",
                    borderWidth: 1,
                    data: ["{{ $composition[0]->sum('user_female_count') }}",
                        "{{ $composition[1]->sum('user_female_count') }}"
                    ]
                },

            ]
        };

        var chartOptions = {
            responsive: true,
            legend: {
                position: "top"
            },
            title: {
                display: false,
                text: "Chart.js Bar Chart"
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }

        const ctxx = document.getElementById('myChart');
        const myChart = new Chart(ctxx, {
            type: "bar",
            data: barChartData,
            options: chartOptions
        });

        const ctx = document.getElementById('gender');
        const data = {
            labels: [
                'Pria',
                'Wanita',

            ],
            datasets: [{
                label: 'My First Dataset',
                data: ["{{ $gender['male'] }}", "{{ $gender['female'] }}"],
                backgroundColor: [
                    '#0278a3',
                    '#ffa426',
                ],
                hoverOffset: 4
            }]
        };
        const myChart2 = new Chart(ctx, {
            type: 'pie',
            data: data,
            options: {
                plugins: {
                    datalabels: {
                        // formatter: (value, ctx) => {
                        //     let datasets = ctx.chart.data.datasets;
                        //     if (datasets.indexOf(ctx.dataset) === datasets.length - 1) {
                        //         let sum = datasets[0].data.reduce((a, b) => a + b, 0);
                        //         let percentage = Math.round((value / sum) * 100) + '%';
                        //         return percentage;
                        //     } else {
                        //         return percentage;
                        //     }
                        // },
                        color: '#fff',
                    }
                }
            }

        });

        const lcount = @json($lcount);
        const lname = @json($lname);
        var ctx3 = document.getElementById('location').getContext('2d');
        var chart3 = new Chart(ctx3, {
            type: 'bar',
            data: {
                labels: lname,
                datasets: [{
                    label: 'Member',
                    backgroundColor: "#0278a3",
                    borderColor: 'rgb(255, 255, 255)',
                    data: lcount
                }]
            },
            options: {
                plugins: {
                    legend: {
                        labels: {
                            // This more specific font property overrides the global property
                            font: {
                                size: 7
                            }
                        }
                    },
                    datalabels: {
                        // formatter: (value, ctx) => {
                        //     let datasets = ctx.chart.data.datasets;
                        //     if (datasets.indexOf(ctx.dataset) === datasets.length - 1) {
                        //         let sum = datasets[0].data.reduce((a, b) => a + b, 0);
                        //         let percentage = Math.round((value / sum) * 100) + '%';
                        //         return percentage;
                        //     } else {
                        //         return percentage;
                        //     }
                        // },
                        color: '#fff',
                    }
                }
            }


        });
        const lcount4 = @json($lcount4);
        const lname4 = @json($lname4);
        const ctx4 = document.getElementById('R-01-B');
        const chart4 = new Chart(ctx4, {
            type: "bar",
            data: {
                labels: lname4,
                datasets: [{
                    backgroundColor: "#0278a3",
                    borderColor: 'rgb(255, 255, 255)',
                    data: lcount4
                }]
            },
            options: {
                indexAxis: 'y',
                // Elements options apply to all of the options unless overridden in a dataset
                // In this case, we are setting the border of each horizontal bar to be 2px wide
                elements: {
                    bar: {
                        borderWidth: 2,
                    }
                },
                responsive: true,
                plugins: {
                    legend: {
                        display: false,

                    },
                    title: {
                        display: true,
                        text: 'Chart.js Horizontal Bar Chart'
                    }
                }
            },
        });
        const lcount5 = @json($lcount5);
        const lname5 = @json($lname5);
        const ctx5 = document.getElementById('R-01-D');
        const chart5 = new Chart(ctx5, {
            type: "bar",
            data: {
                labels: lname5,
                datasets: [{
                    backgroundColor: "#0278a3",
                    borderColor: 'rgb(255, 255, 255)',
                    data: lcount5
                }]
            },
            options: {
                indexAxis: 'y',
                // Elements options apply to all of the options unless overridden in a dataset
                // In this case, we are setting the border of each horizontal bar to be 2px wide
                elements: {
                    bar: {
                        borderWidth: 2,
                    }
                },
                responsive: true,
                plugins: {
                    legend: {
                        display: false,

                    },

                }
            },
        });


        const lname6 = @json($lname6);
        const lcount61 = @json($lcount61);
        const lcount62 = @json($lcount62);
        const lcount6 = @json($lcount6);
        // var barChartData6 = {
        //     labels: lname6,
        //     datasets: [{
        //             label: "male",
        //             backgroundColor: poolColors(1),
        //             // borderColor: "red",
        //             borderWidth: 1,
        //             data: ["{{ $composition[0]->sum('user_male_count') }}",
        //                 "{{ $composition[1]->sum('user_male_count') }}"
        //             ]
        //         },
        //         {
        //             label: "female",
        //             backgroundColor: poolColors(1),
        //             // borderColor: "blue",
        //             borderWidth: 1,
        //             data: ["{{ $composition[0]->sum('user_female_count') }}",
        //                 "{{ $composition[1]->sum('user_female_count') }}"
        //             ]
        //         },

        //     ]
        // };

        // const data6 = {
        //     labels: ["ALam Sutera", "Jaksel", "palembang"],
        //     datasets: [{
        //             label: 'Dataset 1',
        //             data: [3, 1, 2],
        //             backgroundColor: "red"
        //         },
        //         {
        //             label: 'Dataset 2',
        //             data: [3, 3, 2],
        //             backgroundColor: "blue"
        //         },
        //         {
        //             label: 'Dataset 3',
        //             data: [3, 1, 2],
        //             backgroundColor: "yellow"
        //         },
        //     ]
        // };
        // const ctx6 = document.getElementById('R01E');
        // const chart6 = new Chart(ctx6, {
        //     type: "bar",
        //     data: barChartData,
        //     options: chartOptions
        // });

        const ctx77 = document.getElementById("R01E").getContext("2d");

        const dataku = {
            labels: lname6,
            datasets: [{
                    label: "active",
                    backgroundColor: "#0278a3",

                    data: lcount61,
                },
                {
                    label: "inactive",
                    backgroundColor: "#ed204a",
                    data: lcount62,
                },

            ]
        };

        const chartsss = new Chart(ctx77, {
            type: 'bar',
            data: dataku,
            options: {
                plugins: {
                    title: {
                        display: false,

                    },
                },
                responsive: true,
                interaction: {
                    intersect: false,
                },
                scales: {
                    x: {
                        stacked: true,
                    },
                    y: {
                        stacked: true
                    }
                }
            }
        });
    </script>

    <script>
        var firebaseConfig = {
            apiKey: "AIzaSyANUovdt1apORq-aiSOARqsnkt4s2Q61Uo",
            authDomain: "gabapp-2d12b.firebaseapp.com",
            projectId: "gabapp-2d12b",
            storageBucket: "gabapp-2d12b.appspot.com",
            messagingSenderId: "55539172106",
            appId: "1:55539172106:web:2dc032dbb01d43f850b0f3",
            measurementId: "G-3X0NLCGS86"
        };
        // Add the public key generated from the console here.
        firebase.initializeApp(firebaseConfig);
        const messaging = firebase.messaging();
        messaging.getToken().then((currentToken) => {
            if (currentToken) {
                // Send the token to your server and update the UI if necessary
                sendToken(currentToken);
                console.log('ok');

                // ...
            } else {
                // Show permission request UI
                console.log('No registration token available. Request permission to generate one.');
                // ...
            }
        }).catch((err) => {
            console.log('An error occurred while retrieving token. ', err);
            // ...
        });

        function sendToken(token) {
            axios.post('{{ route('store.token') }}', {
                    token
                })
                .then(function(response) {
                    console.log(response);
                })
                .catch(function(error) {
                    console.log(error);
                });
        }
    </script>
@endsection
@endsection

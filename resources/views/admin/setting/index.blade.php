@extends('layouts.admin-main')

@section('title')
    Pengaturan
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Pengaturan</h1>
            @include('layouts.partials.breadcums')
        </div>

        @include('layouts.partials.flash-message')
        <div class="section-body">
            {{-- <h2 class="section-title">P</h2>
            <p class="section-lead">
                Pengaturan
            </p> --}}

            <div class="row">
                @can('pengaturan-group')
                    <div class="col-lg-6">
                        <div class="card card-large-icons">
                            <div class="card-icon bg-primary text-white">
                                <i class="fas fa-users-cog"></i>
                            </div>
                            <div class="card-body">
                                <h4>Group</h4>
                                <p>Pengaturan Group
                                </p>
                                <a href="{{ route('setting.group') }}" class="card-cta">Ubah Pengaturan <i
                                        class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                @endcan
                @can('pengaturan-hak-akses')
                    <div class="col-lg-6">
                        <div class="card card-large-icons">
                            <div class="card-icon bg-primary text-white">
                                <i class="fas fa-user-lock"></i>
                            </div>
                            <div class="card-body">
                                <h4>Hak Akses</h4>
                                <p>Pengaturan hak akses.</p>
                                <a href="{{ route('setting.hakakses') }}" class="card-cta">Ubah Pengaturan <i
                                        class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                @endcan

            </div>
        </div>


    </section>
@endsection

@extends('layouts.admin-main')

@section('title')
    Pengaturan
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Pengaturan</h1>
            @include('layouts.partials.breadcums')
        </div>

        @include('layouts.partials.flash-message')
        <div class="section-body">
            {{-- <h2 class="section-title">P</h2>
            <p class="section-lead">
                Pengaturan
            </p> --}}

            <div class="card">
                <div class="card-header">
                    <h4>Pengaturan Group</h4>
                </div>
                <div class="card-body">
                    {{-- {{ $setting }} --}}
                    <div class="form-group row">
                        {{-- <label for="inputmax1" class="col-sm-3 col-form-label">Maksimal Jumlah User per-group</label> --}}
                        <label for="inputmax1" class="col-sm-3 col-form-label">max_group_user</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="inputmax1" placeholder="max user" required
                                value="{{ $setting->max_group_user }}" name="max_group_user">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputmax1" class="col-sm-3 col-form-label">max_group_add_user</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="inputmax1" placeholder="max user" required
                                value="{{ $setting->max_group_add_user }}" name="max_group_add_user">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputmax1" class="col-sm-3 col-form-label">maximal_to_inactive_user</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="inputmax1" placeholder="max user" required
                                value="{{ $setting->max_to_inactive_user }}" name="max_to_inactive_user">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputmax1" class="col-sm-3 col-form-label">maximal_chat_group_show</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="inputmax1" placeholder="max user" required
                                value="{{ $setting->max_chat_group_show }}" name="max_chat_group_show">
                        </div>
                    </div>

                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </div>


    </section>
@endsection

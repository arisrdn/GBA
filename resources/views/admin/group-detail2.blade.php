@extends('layouts.admin-main')

@section('title')
    Group
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Grup Individu</h1>
            @include('layouts.partials.breadcums')
        </div>
        @php
            $plan = '';
            if (!isset($data->plan)) {
                $plan = '(di hapus)' . $data->plan_with_trashed->description;
            } else {
                $plan = $data->plan->description;
            }
        @endphp
        @include('layouts.partials.flash-message')
        <div class="section-body">
            <h2 class="section-title">{{ $plan }}</h2>


            <div class="row">
                <div class="col">
                    <div class="card card-danger">
                        <div class="card-header">
                            <h4>MEMBER LIST</h4>
                            <div class="card-header-action">
                                <a class="btn btn-primary" href="{{ route('r03', $data->id) }}">
                                    Download
                                </a>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table-striped table" id="table-user">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th>Nama </th>
                                            <th>Progress </th>
                                            <th>Terakhir Baca </th>
                                            <th>Status</th>
                                            <th>Streak</th>
                                        </tr>
                                        <tr class="filters">
                                            <td></td>
                                            <td><input type="text" placeholder="Nama" style="width: 100%;"></td>
                                            <td><input type="text" placeholder="Progress" style="width: 100%;"></td>
                                            <td><input type="text" placeholder="Terakhir Baca" style="width: 100%;"></td>
                                            <td>
                                                <select name="" id="">
                                                    <option value="">Pilih Semua</option>
                                                    <option value="AKTIF">Aktif</option>
                                                    <option value="TIDAK AKTIF">Tidak Aktif</option>
                                                </select>
                                            </td>
                                            <td><input type="text" placeholder="Steak" style="width: 100%;"></td>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($data->member as $dat => $da)
                                            @php
                                                $count_todo = $da->todo->count();
                                                $count_read = $da->todo->where('read_at', '!=', null)->count();
                                            @endphp
                                            <tr>
                                                <td>
                                                    {{ $dat + 1 }}
                                                </td>
                                                <td>{{ $da->user->name }}</td>
                                                <td>
                                                    @isset($da->todo[0])
                                                        <div class="progress text-dark mb-3">
                                                            <div class="progress-bar progress-bar-striped" role="progressbar"
                                                                aria-valuenow=" {{ $count_read }}" aria-valuemin="0"
                                                                aria-valuemax=" {{ $count_todo }}"
                                                                data-width="{{ ($count_read / $count_todo) * 100 }}%">
                                                                {{ $count_read }}</div>
                                                        </div>
                                                    @endisset



                                                </td>
                                                <td>
                                                    @if ($last = $da->todo->where('read_at', '!=', null)->sortByDesc('id')->first())
                                                        {{ $last->todolist->chapter_verse }} <br />
                                                        {{ date('j M Y', strtotime($last->read_at)) }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>

                                                <td>
                                                    @if ($da->todo->sortByDesc('id')->first())
                                                        @php
                                                            $readdate = \Carbon\Carbon::parse($da->todo->sortByDesc('id')->first()->created_at)->addday(30);
                                                            $status = 'ACTIVE';
                                                            // $retVal = ? ($status = 'INACTIVE') : ($status = 'ACTIVE');
                                                        @endphp
                                                        @if ($readdate <= today())
                                                            <span class="badge badge-warning">{{ 'INACTIVE' }}</span>
                                                        @else
                                                            <span class="badge badge-success">{{ 'ACTIVE' }}</span>
                                                        @endif
                                                    @else
                                                        @if ($da->updated_at->addday(30) <= today())
                                                            <span class="badge badge-warning">{{ 'INACTIVE' }}</span>
                                                        @else
                                                            <span class="badge badge-success">{{ 'ACTIVE' }}</span>
                                                        @endif
                                                    @endif

                                                </td>
                                                <td>
                                                    @if (isset($last))
                                                        {{-- @if ($last->read_at == today() || $last->read_at == today()->subday()) --}}
                                                        @if ($last->read_at == today())
                                                            {{ $da->strike }}
                                                        @else
                                                            0
                                                        @endif
                                                    @endif



                                                </td>

                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

    <div class="modal fade" id="addTodo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <form action="{{ route('group') . '/todo' }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('patch')
            <input type="hidden" name="group_id" value="{{ request('id') }}">

            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel">Ubah Data</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{-- <h5 class="section-title">Nama Grup</h5> --}}
                        {{-- <div class="form-row">
                            <div class="form-group col">
                                <div class="input-group"> --}}

                        <input type="hidden" class="form-control" name="name" required placeholder="Nama Grup"
                            value="{{ $data->name }}">
                        <input type="hidden" class="form-control" name="type" required placeholder="Nama Grup"
                            value="{{ $data->name }}">
                        {{-- </div>
                            </div>

                        </div> --}}

                        <h5 class="section-title">Bacaan </h5>
                        {{-- <div class="section-title mt-0">Upload file Bacaan</div> --}}
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Rencana Baca</label>
                            <div class="col-sm-9">

                                <input type="text" class="form-control" name="plan_id" required readonly
                                    {{-- value="{{ $data->plan->description }}" --}}>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Upload File</label>
                            <div class="col-sm-9">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="todo"
                                        accept=" application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                                    <label class="custom-file-label" for="customFile">{{ $data->todo_file }}</label>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save </button>
                    </div>
                </div>
            </div>

        </form>
    </div>

    <div class="modal fade" id="addtime" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <form action="{{ route('group') . '/time' }}" method="POST">
            @csrf
            @method('patch')

            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ubah waktu</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{-- <div class="section-title mt-0">Upload file Bacaan</div> --}}

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <div class="input-group">

                                    {{-- <input type="time" class="form-control" name="start" required
                                        placeholder="04:00" value="{{ $data->eod->start }}" id="istart"> --}}
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                {{-- <label for="inputPassword4">Password</label> --}}
                                <div class="input-group">

                                    {{-- <input type="time" class="form-control" name="end" required
                                            placeholder="20:00" value="{{ $data->eod->end }}" id="iend"> --}}
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save </button>
                    </div>
                </div>
            </div>

        </form>
    </div>
    <div class="modal fade" id="addtime" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <form action="{{ route('group') . '/time' }}" method="POST">
            @csrf
            @method('patch')

            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ubah waktu</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{-- <div class="section-title mt-0">Upload file Bacaan</div> --}}

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <div class="input-group">

                                    {{-- <input type="time" class="form-control" name="start" required
                                        placeholder="04:00" value="{{ $data->eod->start }}" id="istart"> --}}
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                {{-- <label for="inputPassword4">Password</label> --}}
                                <div class="input-group">

                                    {{-- <input type="time" class="form-control" name="end" required
                                        placeholder="20:00" value="{{ $data->eod->end }}" id="iend"> --}}
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save </button>
                    </div>
                </div>
            </div>

        </form>
    </div>


    <div class="modal fade" id="modaldetail" tabindex="-1" role="dialog" aria-labelledby="delete"
        aria-hidden="true">

        <div class="modal-dialog modal-md modal-dialog-centered" role="document">

            <div class="card profile-widget col px-0 pt-0">
                <div class="profile-widget-header">
                    <img alt="image" src="../assets/img/avatar/avatar-1.png"
                        class="rounded-circle profile-widget-picture">
                    <div class="profile-widget-items">
                        <div class="profile-widget-item">
                            <div class="profile-widget-item-label">Posts</div>
                            <div class="profile-widget-item-value">187</div>
                        </div>
                        <div class="profile-widget-item">
                            <div class="profile-widget-item-label">Followers</div>
                            <div class="profile-widget-item-value">6,8K</div>
                        </div>
                        <div class="profile-widget-item">
                            <div class="profile-widget-item-label">Following</div>
                            <div class="profile-widget-item-value">2,1K</div>
                        </div>
                    </div>
                </div>
                <div class="profile-widget-description px-3">

                    <div class="form-group row align-items-center">
                        <label class="col-md-4 text-md-right text-left">Nama</label>
                        <div class="col">
                            <p id="itemname"></p>
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <label class="col-md-4 text-md-right text-left">Alamat</label>
                        <div class="col">
                            <p id="itemaddress"></p>
                        </div>
                    </div>
                    {{-- <div class="form-group row align-items-center">
                        <label class="col-md-4 text-md-right text-left">Tota</label>
                        <div class="col">
                            {{ count($data->todo) }}
                        </div>
                    </div> --}}
                </div>
                <div class="modal-content">
                    <div class="modal-footer">
                        {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
                        <a type="submit" id="link" class="btn btn-primary">Kirim Pesan </a>
                    </div>
                </div>

            </div>


        </div>

    </div>
@section('plugin_css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">
@endsection
@section('plugin_js')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function() {
            setTimeout(function() {
                $(".alert").alert('close');
            }, 2000);
        });

        $("#table-admin").dataTable({
            scrollY: '150px',
            paging: false,
            "lengthChange": false
        });
        // $("#table-user").dataTable({
        //     "columnDefs": [{

        //     }]
        // });
        $("#table-todo").dataTable({
            // "info": false,
            searching: false,
            scrollY: '200px',
            paging: false,
            "lengthChange": false
        });

        $(document).ready(function() {
            $('input[type="file"]').on("change", function() {
                let filenames = [];
                let files = this.files;
                if (files.length > 1) {
                    filenames.push("Total Files (" + files.length + ")");
                } else {
                    for (let i in files) {
                        if (files.hasOwnProperty(i)) {
                            filenames.push(files[i].name);
                        }
                    }
                }
                $(this)
                    .next(".custom-file-label")
                    .html(filenames.join(","));
            });
        });
        $(document).ready(function() {
            // Setup - add a text input to each footer cell
            // $('#table-user thead tr')
            //     .clone(true)
            //     .addClass('filt')
            //     .appendTo('#table-user thead');

            var table = $('#table-user').DataTable({
                orderCellsTop: true,
                fixedHeader: true,
                initComplete: function() {
                    var api = this.api();

                    // For each column
                    api.columns()
                        .eq(0)
                        .each(function(colIdx) {
                            // Set the header cell to contain the input element
                            // var cell = $('.filters td').eq(
                            //     $(api.column(colIdx).header()).index()
                            // );
                            // var title = $(cell).text();
                            // $(cell).html('<input type="text" placeholder="' + title + '" />');

                            // On every keypress in this input
                            $(
                                    'input',
                                    $('.filters td').eq($(api.column(colIdx).header()).index())
                                )
                                .off('keyup change')
                                .on('change', function(e) {
                                    // Get the search value
                                    $(this).attr('title', $(this).val());
                                    var regexr =
                                        '({search})'; //$(this).parents('th').find('select').val();

                                    var cursorPosition = this.selectionStart;
                                    // Search the column for that value
                                    api
                                        .column(colIdx)
                                        .search(
                                            this.value != '' ?
                                            regexr.replace('{search}', '(((' + this.value +
                                                ')))') :
                                            '',
                                            this.value != '',
                                            this.value == ''
                                        )
                                        .draw();
                                })
                                .on('keyup', function(e) {
                                    e.stopPropagation();

                                    $(this).trigger('change');
                                    $(this)
                                        .focus()[0]
                                        .setSelectionRange(cursorPosition, cursorPosition);
                                });
                        });
                    api.columns()
                        .eq(0)
                        .each(function(colIdx) {
                            // Set the header cell to contain the input element
                            // var cell = $('.filters td').eq(
                            //     $(api.column(colIdx).header()).index()
                            // );
                            // var title = $(cell).text();
                            // $(cell).html(
                            //     '<select class="form-select form-select-sm" aria-label=".form-select-sm example"><option value="">pilih semua</option><option value="Aktif">Aktif</option><option value="Tidak Aktif">Tidak aktif</option></select>'
                            // );

                            // On every keypress in this input
                            $(
                                    'select',
                                    $('.filters td').eq($(api.column(colIdx).header()).index())
                                )
                                .on('change', function() {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    api.column(colIdx)
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                                });
                        });
                },

            });
        });
    </script>

    <script type="text/javascript">
        $(document).on("click", "#btn-show", function() {

            var itemid = $(this).attr('data-id');
            var itemname = $(this).attr('data-name');
            var itemuser = JSON.parse($(this).attr('data-user'))
            $('#edit-id').val(itemid);
            $('#edit-name').val(itemname);
            document.getElementById("itemname").innerHTML = itemuser.name;
            document.getElementById("itemaddress").innerHTML = itemuser.address;
            document.getElementById("link").href = "{{ route('chat') }}/p/" + itemuser.id + "";
            // console.log(itemid, itemname);
        });
        // $(document).on("click", "#btn-delete", function() {
        //     var itemid = $(this).attr('data-id');
        //     var itemname = $(this).attr('data-name');
        //     $('#delete-id').val(itemid);
        //     $('#delete-name').val(itemname);
        //     console.log(itemid, itemname);
        // });
    </script>
@endsection
@endsection

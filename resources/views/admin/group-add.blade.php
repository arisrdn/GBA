@extends('layouts.admin-main')

@section('title')
    Grup
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Grup</h1>
            @include('layouts.partials.breadcums')
        </div>
        @include('layouts.partials.flash-message')

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <form action="{{ route('group') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('post')
                        <div class="card">
                            <div class="card-header">
                                <h4>Buat Grup</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label for="inputEmail3" class="col-sm-3 col-form-label">Tipe </label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="type" required id="type">
                                                    <option value="">pilih..</option>
                                                    @foreach ($type as $data)
                                                        <option value="{{ $data->id }}">{{ $data->name }}
                                                        </option>
                                                    @endforeach
                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputEmail3" class="col-sm-3 col-form-label">Rencana Baca</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="plan_id" required id="plan"
                                                    disabled>
                                                    {{-- <option value="">pilih..</option> --}}
                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Nama Grup</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="name" disabled
                                                    name="name" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputEmail3" class="col-sm-3 col-form-label">PIC 1</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="pic" required>
                                                    <option value="">pilih..</option>
                                                    @foreach ($user as $data)
                                                        <option value="{{ $data->id }}">{{ $data->name }}
                                                        </option>
                                                    @endforeach
                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputEmail3" class="col-sm-3 col-form-label">PIC 2</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="copic" required>
                                                    <option value="">pilih..</option>
                                                    @foreach ($user as $data)
                                                        <option value="{{ $data->id }}">{{ $data->name }}
                                                        </option>
                                                    @endforeach
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 border-left border-dark">
                                        <div class="section-title mt-0">Priode</div>

                                        <div class="form-group row">
                                            <label for="inputPassword3" class="col-sm-2 col-form-label">Start</label>
                                            <div class="col">
                                                <input type="date" class="form-control" name="start" required disabled
                                                    id="start">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputPassword3" class="col-sm-2 col-form-label">End</label>
                                            <div class="col">
                                                <input type="date" class="form-control" name="end" required disabled
                                                    id="end">
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="card-footer text-right">
                                <a class="btn btn-secondary mr-1" href="{{ route('group') }}">Kembali</a>
                                <button class="btn btn-primary mr-1" type="submit">Buat</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </section>



@section('plugin_css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">

    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection
@section('plugin_js')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/eonasdan-bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js">
    </script>


    <script type="text/javascript">
        var addDays = 0

        var now = new Date();
        // now.setDate(d.getDate() + 50);
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today = now.getFullYear() + "-" + (month) + "-" + (day);


        function convertToRoman(num) {
            var roman = {
                M: 1000,
                CM: 900,
                D: 500,
                CD: 400,
                C: 100,
                XC: 90,
                L: 50,
                XL: 40,
                X: 10,
                IX: 9,
                V: 5,
                IV: 4,
                I: 1
            };
            var str = '';

            for (var i of Object.keys(roman)) {
                var q = Math.floor(num / roman[i]);
                num -= q * roman[i];
                str += i.repeat(q);
            }

            return str;
        }

        function onChangeSelect(url, id, name) {
            $.ajax({
                url: url + "/" + id,
                type: 'GET',

                success: function(data) {
                    $('#' + name).empty();
                    $('#' + name).removeAttr('disabled');
                    if (name == "name") {
                        console.log(''.replace(/[^0-9]/g, ''));
                        $('#' + name).val('GBA-' + data.data.type + data.data.description.replace(
                                /[^0-9]/g, '') + "-" +
                            data.data.countgroup);
                        // convertToRoman(data.data.countgroup));
                        addDays = data.data.add_days;
                        console.log(addDays);
                        $('#start').removeAttr('disabled');
                        $('#start').val(moment().format('YYYY-MM-DD'));
                        $('#end').removeAttr('disabled');
                        $('#end').val(moment().add(addDays - 1, 'd').format('YYYY-MM-DD'));
                    } else {
                        $('#' + name).append('<option>Pilih...</option>');
                        $.each(data.data, function(key, value) {
                            $('#' + name).append('<option value="' + value.id + '">' + value
                                .description +
                                '</option>');
                        });
                        $('#name').attr('disabled', 'disabled');
                        $('#start').attr('disabled', 'disabled');
                        $('#end').attr('disabled', 'disabled');
                        $('#name').val("");
                        $('#start').val("");
                        $('#end').val("");
                    }

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    // alert(xhr.status);
                    // alert(thrownError);
                    if (name == "name") {
                        $('#' + name).val("");
                        $('#' + name).attr('disabled', 'disabled');
                        $('#start').attr('disabled', 'disabled');
                        $('#end').attr('disabled', 'disabled');
                    } else {
                        $('#' + name).empty();
                        $('#' + name).attr('disabled', 'disabled');
                        $('#name').val("");
                        $('#start').val("");
                        $('#end').val("");
                        $('#name').attr('disabled', 'disabled');
                        $('#start').attr('disabled', 'disabled');
                        $('#end').attr('disabled', 'disabled');

                    }
                }
            });
        }
        $(function() {
            $('#type').on('change', function() {
                onChangeSelect('{{ url('/api/v1/type') }}', $(this).val(), 'plan');
            });
            $('#plan').on('change', function() {
                onChangeSelect('{{ url('/api/v1/naming') }}', $(this).val(), 'name');
            });
            $('#start').on('change', function() {

                $('#end').val(moment($('#start').val()).add(addDays, 'd').format('YYYY-MM-DD'));

            })
        });
    </script>
@endsection
@endsection


{{-- @section('plugin_js')
    <script src="https://netdna.bootstrapcdn.com/bootstrap/2.3.2/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script>

    <script>
        function onChangeSelect(url, id, name) {
            $.ajax({
                url: url + "/" + id,
                type: 'GET',
                // data: {
                //     id: id
                // },
                success: function(data) {
                    $('#' + name).empty();
                    $('#' + name).append('<option>Pilih...</option>');

                    $.each(data.data, function(key, value) {
                        $('#' + name).append('<option value="' + value.id + '">' + value.description +
                            '</option>');
                    });
                }
            });
        }
        $(function() {
            $('#provinsi').on('change', function() {
                onChangeSelect('{{ url('/api/v1/type') }}', $(this).val(), 'kota');
            });
            // $('#kota').on('change', function() {
            //     onChangeSelect('{{ url('districts') }}', $(this).val(), 'kecamatan');
            // })
            // $('#kecamatan').on('change', function() {
            //     onChangeSelect('{{ url('villages') }}', $(this).val(), 'desa');
            // })
        });

        const date = new Date();
        date.setFullYear(date.getFullYear() - 7);
        // $("#year").datetimepicker({
        //     format: 'YYYY',
        //     viewMode: "years",
        //     defaultDate: date,
        //     icons: {
        //         left: "far fa-chevron-up",
        //         right: "far fa-chevron-down",
        //     },

        // });

        $(document).ready(function() {
            $("#year").datepicker({
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years",
                autoclose: true,
                defaultViewDate: date

            });
        })
    </script> --}}

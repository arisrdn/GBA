@extends('layouts.admin-main')

@section('title')
    Persetujuan
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Group</h1>
            @include('layouts.partials.breadcums')
        </div>

        @include('layouts.partials.flash-message')
        <div class="section-body">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h4>Setujui masuk Grup</h4>
                            <div class="card-header-action">

                                {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addChurch">
                                    Tambah Gereja
                                </button> --}}

                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table-striped table" id="table-church">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Opsi Baca</th>
                                            <th>Tipe</th>
                                            <th>Rencana Baca</th>
                                            <th>Group</th>
                                            <th>Request</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($member as $k => $dat)
                                            @php
                                                $filtered_arr = [];
                                                foreach ($group as $name => $employee) {
                                                    if ($employee->reading_plan_id == $dat->reading_plan_id) {
                                                        array_push($filtered_arr, $employee);
                                                    }
                                                }
                                                
                                                $opt = json_decode($dat->data);
                                                $readopt = 'group';
                                                if ($opt->read_option == 'group' || $opt->read_option == 'g') {
                                                    # code...
                                                    $readopt = 'Group';
                                                } else {
                                                    # code...
                                                    $readopt = 'Individu';
                                                }
                                                $gp2 = '';
                                                $gp2id = '';
                                                $search_val = $dat->reading_plan_id;
                                                foreach ($group2 as $elem) {
                                                    if ($elem['reading_plan_id'] == $search_val) {
                                                        $gp2 = $elem['name'];
                                                        $gp2id = $elem['id'];
                                                    }
                                                }
                                                
                                            @endphp

                                            @php
                                                $plan = '';
                                                if (!isset($dat->plan)) {
                                                    $plan = 'Rencana baca di hapus';
                                                    $type = '-';
                                                } else {
                                                    $plan = $dat->plan->description;
                                                    $type = $dat->plan->type->name;
                                                }
                                            @endphp
                                            <tr>
                                                <td>
                                                    {{ $k + 1 }}

                                                </td>
                                                <td>{{ $dat->user->name }}</td>
                                                <td>{{ $readopt }}</td>
                                                <td>{{ $type }}</td>
                                                <td>

                                                    {{ $plan }}
                                                </td>
                                                <td>
                                                    @if ($opt->read_option == 'group' or $opt->read_option == 'g')
                                                        @if ($filtered_arr)
                                                            <select class="form-control-sm" id="gp{{ $dat->id }}">
                                                                @foreach ($filtered_arr as $gm)
                                                                    <option value="{{ $gm->id }}">{{ $gm->name }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        @else
                                                            <a href="#" class="badge badge-warning">
                                                                Group Tidak tersedia
                                                            </a>
                                                        @endif
                                                    @else
                                                        @if ($gp2)
                                                            {{ $gp2 }}
                                                        @else
                                                            Rencana Baca Belum Di Buat
                                                        @endif
                                                    @endif


                                                </td>
                                                <td>{{ $dat->created_at }}</td>
                                                <td>

                                                    @if ($opt->read_option == 'group' or $opt->read_option == 'g')
                                                        @if ($filtered_arr)
                                                            <a href="#" id="btn-approve" data-toggle="modal"
                                                                data-target="#modalapprove" data-wl="{{ $dat }}"
                                                                data-name="{{ $dat->user->name }}"
                                                                data-plan="{{ $plan }}" class="btn btn-primary">
                                                                Approve</a>
                                                            <a href="#" id="btn-reject" data-toggle="modal"
                                                                data-target="#Modaldelete" data-wl="{{ $dat }}"
                                                                data-name="{{ $dat->user->name }}" class="btn btn-warning">
                                                                Reject</a>
                                                        @else
                                                            <a href="#" class="btn disabled btn-primary">Approve</a>
                                                            <a href="#" id="btn-reject" data-toggle="modal"
                                                                data-target="#Modaldelete" data-wl="{{ $dat }}"
                                                                data-name="{{ $dat->user->name }}" class="btn btn-warning">
                                                                Reject</a>
                                                        @endif
                                                    @else
                                                        @if ($gp2)
                                                            <a href="#" id="btn-approve2" data-toggle="modal"
                                                                data-target="#modalapprove" data-wl="{{ $dat }}"
                                                                data-name="{{ $dat->user->name }}"
                                                                data-plan="{{ $plan }}"
                                                                data-gp="{{ $gp2 }}"
                                                                data-gpid="{{ $gp2id }}" class="btn btn-primary">
                                                                Approve</a>
                                                            <a href="#" id="btn-reject" data-toggle="modal"
                                                                data-target="#Modaldelete" data-wl="{{ $dat }}"
                                                                data-name="{{ $dat->user->name }}" class="btn btn-warning">
                                                                Reject</a>
                                                        @else
                                                            <a href="#" class="btn disabled btn-primary">Approve</a>
                                                            <a href="#" id="btn-reject" data-toggle="modal"
                                                                data-target="#Modaldelete" data-wl="{{ $dat }}"
                                                                data-name="{{ $dat->user->name }}" class="btn btn-warning">
                                                                Reject</a>
                                                        @endif
                                                    @endif

                                                </td>
                                            </tr>
                                        @endforeach

                                        <input type="hidden" id="custId" name="custId" value="3487">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </section>

    <div class="modal fade" id="modalapprove" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <form action="{{ route('join') }}" method="POST">
            @csrf
            @method('post')
            <div class="modal-dialog" role="document">

                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="edit">Approve User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row mb-3">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Nama</label>
                            <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext" id="name" value="">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Rencana Baca</label>
                            <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext" id="plan"
                                    value="">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Group</label>
                            <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext" id="group"
                                    value="">
                            </div>
                        </div>
                        <input type="hidden" readonly class="form-control-plaintext" id="group_id" value=""
                            name="group_id">
                        <input type="hidden" readonly class="form-control-plaintext" id="wl_id" value=""
                            name="wl_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Approve </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal fade" id="Modaldelete" tabindex="-1" role="dialog" aria-labelledby="delete"
        aria-hidden="true">
        <form action="{{ route('join') }}" method="POST">
            @csrf
            @method('DELETE')
            <div class="modal-dialog" role="document">

                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="delete">Reject data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            Yakin akan di Reject ?
                            <input type="hidden" name="reject_id" class="form-control" required id="reject-id">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger btn-shadow">Ya</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@section('plugin_css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">
@endsection
@section('plugin_js')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function() {
            setTimeout(function() {
                $(".alert").alert('close');
            }, 2000);
        });

        $("#table-church").dataTable({
            "columnDefs": [{
                // "sortable": false,
                // "targets": [2, 3]
            }]
        });
    </script>

    <script type="text/javascript">
        $(document).on("click", "#btn-approve", function() {
            var itemname = $(this).attr('data-name');
            var itempalan = $(this).attr('data-plan');
            var item = JSON.parse($(this).attr('data-wl'))
            var e = document.getElementById("gp" + item.id);
            var value = e.options[e.selectedIndex].value;
            var text = e.options[e.selectedIndex].text;
            $('#name').val(itemname);
            $('#plan').val(itempalan);
            $('#group').val(text);
            $('#group_id').val(value);
            $('#wl_id').val(item.id);
        });
        $(document).on("click", "#btn-approve2", function() {
            var itemname = $(this).attr('data-name');
            var itempalan = $(this).attr('data-plan');
            var item = JSON.parse($(this).attr('data-wl'))
            var value = $(this).attr('data-gpid')
            var text = $(this).attr('data-gp')
            console.log(value);
            $('#name').val(itemname);
            $('#plan').val(itempalan);
            $('#group').val(text);
            $('#group_id').val(value);
            $('#wl_id').val(item.id);
        });

        $(document).on("click", "#btn-reject", function() {
            var itemid = $(this).attr('data-id');
            var item = JSON.parse($(this).attr('data-wl'))
            $('#reject-id').val(item.id);
            $('#delete-name').val(itemname);
            console.log(itemid, itemname);
        });
    </script>
@endsection
@endsection

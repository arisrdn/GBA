@extends('layouts.admin-main')

@section('title')
    Broadcast
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Chat</h1>
            @include('layouts.partials.breadcums')
        </div>
        @include('layouts.partials.flash-message')

        <div class="section-body">
            <div class="row">
                <div class="col-md-8 col-sm-12">
                    <form action="{{ route('broadcast') }}" method="post">
                        @csrf
                        @method('post')
                        <div class="card">
                            <div class="card-header">
                                <h4> Broadcast Message</h4>
                            </div>
                            <div class="card-body">

                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-md-2 col-lg-2">To</label>
                                    <div class="col-sm-12 col-md-9">
                                        <select class="form-control selectric" name="to">
                                            <option value="member">Member Group</option>
                                            {{-- <option value="pic">PIC/admin</option> --}}
                                            {{-- <option value="all">All</option> --}}
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-md-2 col-lg-2">Title</label>
                                    <div class="col-sm-12 col-md-9">
                                        <input type="text" class="form-control" id="name" name="title"
                                            required="">
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-md-2 col-lg-2">Message</label>
                                    <div class="col-sm-12 col-md-9">
                                        <textarea class="summernote" required name="message"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row mb-1">
                                    <label class="col-form-label text-md-right col-12 col-md-2 col-lg-2"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button class="btn btn-primary">Publish</button>
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer text-right">

                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-4 col-sm-12">


                    <div class="card">
                        <div class="card-header">
                            <h4><i class="fa fa-history" aria-hidden="true"></i> History</h4>
                        </div>
                        <div class="card-body">
                            <ul class="list-unstyled list-unstyled-border">

                                @foreach ($data as $k => $bc)
                                    <li class="media col-12">
                                        <div class="media-body col-12">
                                            <div class="text-primary float-right">{{ $bc->created_at->format('d/m/Y') }}
                                            </div>
                                            <div class="media-title">{{ $bc->title }}</div>
                                            <span class="text-small text-muted">
                                                {{ substr(strip_tags(str_replace('&nbsp;', ' ', $bc->message)), 0, 45) }}
                                                {{-- {!! strip_tags($bc->message) !!} --}}

                                            </span>
                                        </div>
                                        <a href="#" id="btn-approve" data-toggle="modal" data-target="#modalapprove"
                                            data-bc="{{ $bc }}" class="btn btn-icon btn-outline-secondary">
                                            <i class="far fa-eye"></i>
                                        </a></a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                        <div class="card-footer text-right">

                        </div>
                    </div>
                </div>

    </section>

    <div class="modal fade" id="modalapprove" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <form action="{{ route('join') }}" method="POST">
            @csrf
            @method('post')
            <div class="modal-dialog" role="document">

                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="bctitle"> title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="message">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </form>
    </div>

@section('plugin_css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">

    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection
@section('plugin_js')
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>

    <script type="text/javascript">
        $(document).on("click", "#btn-approve", function() {

            var item = JSON.parse($(this).attr('data-bc'))
            console.log(item);
            $('#bctitle').html(item.title);
            $('#message').html(item.message);


        });
    </script>
@endsection
@endsection

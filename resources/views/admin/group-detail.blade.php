@extends('layouts.admin-main')

@section('title')
    Grup
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Grup </h1>
            @include('layouts.partials.breadcums')
        </div>
        @php
            $plan = '';
            $type = '';
            if (!isset($data->plan)) {
                $plan = '(di hapus)' . $data->plan_with_trashed->description;
                $type = '(di hapus)' . $data->plan_with_trashed->type->name;
            } else {
                $plan = $data->plan->description;
                $type = $data->plan->type->name;
            }
        @endphp
        @include('layouts.partials.flash-message')
        <div class="section-body">
            <h2 class="section-title">{{ $data->name }}</h2>

            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-primary">
                            <i class="fas fa-users"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Member</h4>
                            </div>
                            <div class="card-body">
                                {{ count($data->user) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-danger">
                            <i class="fas fa-book"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Tipe</h4>
                            </div>
                            <div class="card-body">
                                {{ $type }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-warning">
                            <i class="fas fa-bible"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Rencana Baca
                                </h4>
                            </div>
                            <div class="card-body">
                                <h6>

                                    {{ $plan }}
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-success">
                            <i class="fas fa-clock"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Masa Aktif</h4>
                            </div>
                            <div class="card-body">
                                <h6>

                                    {{ \Carbon\Carbon::parse($data->start_date)->format('j M Y') . ' - ' . date('j M Y', strtotime($data->end_date)) }}
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                @if ($data->end_date <= today())
                    @can('transfer-anggota-group')
                        <div class="col-12">
                            <div class="card card-danger">
                                <div class="card-header">
                                    <h4>Grup Telah berakhir</h4>
                                    <div class="card-header-action">
                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#transfer">
                                            Transfer Anggota
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endcan
                @endif

                <div class="col">
                    <div class="card card-success">
                        <div class="card-header">
                            <h4>Anggota Grup</h4>
                            <div class="card-header-action">
                                <a class="btn btn-primary" href="{{ route('r03', $data->id) }}">
                                    Download
                                </a>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table-striped table" id="table-user">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th>Nama </th>
                                            <th>Progress </th>
                                            <th>Terakhir Baca </th>
                                            <th>Status</th>
                                            <th>Streak</th>
                                        </tr>
                                        <tr class="filters">
                                            <td></td>
                                            <td><input type="text" placeholder="Nama" style="width: 100%;"></td>
                                            <td><input type="text" placeholder="Progress" style="width: 100%;"></td>
                                            <td><input type="text" placeholder="Terakhir Baca" style="width: 100%;"></td>
                                            <td>
                                                <select name="" id="">
                                                    <option value="">Pilih Semua</option>
                                                    <option value="AKTIF">Aktif</option>
                                                    <option value="TIDAK AKTIF">Tidak Aktif</option>
                                                </select>
                                            </td>
                                            <td><input type="text" placeholder="Steak" style="width: 100%;"></td>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($data->member as $dat => $da)
                                            @php
                                                $count_todo = $da->todo->count();
                                                $count_read = $da->todo->where('read_at', '!=', null)->count();
                                            @endphp
                                            <tr>
                                                <td>
                                                    {{ $dat + 1 }}
                                                </td>
                                                <td>{{ $da->user->name }}</td>
                                                <td>
                                                    @isset($da->todo[0])
                                                        <div class="progress text-dark mb-3">
                                                            <div class="progress-bar progress-bar-striped" role="progressbar"
                                                                aria-valuenow=" {{ $count_read }}" aria-valuemin="0"
                                                                aria-valuemax=" {{ $count_todo }}"
                                                                data-width="{{ ($count_read / $count_todo) * 100 }}%">
                                                                {{ $count_read }}</div>
                                                        </div>
                                                    @endisset



                                                </td>
                                                <td>
                                                    @if ($last = $da->todo->where('read_at', '!=', null)->sortByDesc('id')->first())
                                                        {{ $last->todolist->chapter_verse }} <br />
                                                        {{ date('j M Y', strtotime($last->read_at)) }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>

                                                <td>
                                                    @if ($da->todo->sortByDesc('id')->first())
                                                        @php
                                                            $readdate = \Carbon\Carbon::parse($da->todo->sortByDesc('id')->first()->created_at)->addday(30);
                                                            $status = 'AKTIF';
                                                            // $retVal = ? ($status = 'INACTIVE') : ($status = 'ACTIVE');
                                                        @endphp
                                                        @if ($readdate <= today())
                                                            <span class="badge badge-warning">{{ 'TIDAK AKTIF' }}</span>
                                                        @else
                                                            <span class="badge badge-success">{{ 'AKTIF' }}</span>
                                                        @endif
                                                    @else
                                                        @if ($da->updated_at->addday(30) <= today())
                                                            <span class="badge badge-warning">{{ 'TIDAK AKTIF' }}</span>
                                                        @else
                                                            <span class="badge badge-success">{{ 'AKTIF' }}</span>
                                                        @endif
                                                    @endif

                                                </td>
                                                <td>
                                                    @if (isset($last))
                                                        @if ($last->read_at == today())
                                                            {{ $da->strike }}
                                                        @else
                                                            0
                                                        @endif
                                                    @else
                                                        0
                                                    @endif



                                                </td>

                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-12 col-md-4">

                    <div class="card card-warning">
                        <div class="card-header">
                            <h4>Data grup</h4>
                            <div class="card-header-action">
                                @can('edit-group')
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addTodo">
                                        Ubah Data
                                    </button>
                                @endcan


                            </div>
                        </div>
                        <div class="card-body">
                            <div class="profile-widget-description pb-0">
                                <div class="form-group row align-items-center">
                                    <label class="col-md-4 text-md-right text-left">Nama Grup</label>
                                    <div class="col">
                                        <a href="{{ route('chat'), request('id') }}">
                                            {{ $data->name }}
                                            <i class="fas fa-comment"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="form-group row align-items-center">
                                    <label class="col-md-4 text-md-right text-left">Masa Aktif</label>
                                    <div class="col">
                                        {{ \Carbon\Carbon::parse($data->start_date)->format('j M Y') . ' - ' . date('j M Y', strtotime($data->end_date)) }}
                                    </div>
                                </div>
                                <div class="form-group row align-items-center">
                                    <label class="col-md-4 text-md-right text-left"> Tipe</label>
                                    <div class="col">
                                        {{ $type }}
                                    </div>
                                </div>
                                <div class="form-group row align-items-center">
                                    <label class="col-md-4 text-md-right text-left">Rencana Baca</label>
                                    <div class="col">
                                        {{ $plan }}
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="card card-primary mt-4">
                        <div class="card-header">
                            <h4>PIC Grup</h4>

                            @can('edit-group')
                                <div class="card-header-action">
                                    {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editpic"
                                        data-id1="{{ $dat }}" data-id2="{{ $da->user }}" id="btn-edit-pic">
                                        Edit PIC
                                    </button> --}}
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#editpic" id="btn-edit-pic">
                                        Edit PIC
                                    </button>
                                </div>
                            @endcan
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table-striped table" id="table-admin">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th>Nama </th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($data->admin as $dat => $da)
                                            <tr>
                                                <td>
                                                    {{ $dat + 1 }}
                                                </td>
                                                <td>{{ $da->user->name }}</td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>
    {{-- modal --}}
    <div class="modal fade" id="editpic" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <form action="{{ route('group') . '/admin' }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('patch')
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit PIC</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">PIC 1</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="pic" required id="pic1">
                                    <option value="">pilih..</option>
                                    @foreach ($user as $usr)
                                        <option value="{{ $usr->id }}">{{ $usr->name }}
                                        </option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">PIC 2</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="copic" required id="pic2">
                                    <option value="">pilih..</option>
                                    @foreach ($user as $usr)
                                        <option value="{{ $usr->id }}">{{ $usr->name }}
                                        </option>
                                    @endforeach
                                </select>

                            </div>
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal fade" id="transfer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <form action="{{ route('transfer.group') }}" method="POST">
            @csrf
            @method('post')
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tranfer Anggota</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            {{-- <label>Nama Admin</label> --}}
                            <select class="form-control" name="group_to" required>
                                <option value="">pilih group..</option>
                                @foreach ($group as $adm)
                                    <option value="{{ $adm->id }}">
                                        {{ $adm->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="group_id" value="{{ request('id') }}">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Transfer </button>
                    </div>
                </div>
            </div>

        </form>
    </div>
    <div class="modal fade" id="addTodo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <form action="{{ route('group') . '/todo' }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('patch')
            <input type="hidden" name="group_id" value="{{ request('id') }}">

            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel">Ubah Data</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Rencana Baca</label>
                            <div class="col-sm-9">

                                <input type="text" class="form-control" name="" readonly
                                    placeholder="Nama Grup" value="{{ $plan }}">
                                <input type="hidden" name="plan_id" placeholder="Nama Grup"
                                    value="{{ $data->reading_plan_id }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-3 col-form-label">Nama Grup</label>
                            <div class="col">
                                <input type="text" class="form-control" name="name" required
                                    placeholder="Nama Grup" value="{{ $data->name }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-3 col-form-label">Start</label>
                            <div class="col">
                                <input type="date" class="form-control" name="start" required
                                    value="{{ $data->start_date->format('Y-m-d') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-3 col-form-label">End</label>
                            <div class="col">
                                <input type="date" class="form-control" name="end" required
                                    value="{{ $data->end_date->format('Y-m-d') }}">
                            </div>
                        </div>



                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save </button>
                        </div>
                    </div>
                </div>

        </form>
    </div>



@section('plugin_css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">
@endsection
@section('plugin_js')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function() {
            setTimeout(function() {
                $(".alert").alert('close');
            }, 2000);
        });



        // $("#table-user").dataTable({
        //     "columnDefs": [{

        //     }]
        // });


        $(document).ready(function() {
            $('input[type="file"]').on("change", function() {
                let filenames = [];
                let files = this.files;
                if (files.length > 1) {
                    filenames.push("Total Files (" + files.length + ")");
                } else {
                    for (let i in files) {
                        if (files.hasOwnProperty(i)) {
                            filenames.push(files[i].name);
                        }
                    }
                }
                $(this)
                    .next(".custom-file-label")
                    .html(filenames.join(","));
            });
        });

        $(document).ready(function() {
            // Setup - add a text input to each footer cell
            // $('#table-user thead tr')
            //     .clone(true)
            //     .addClass('filt')
            //     .appendTo('#table-user thead');

            var table = $('#table-user').DataTable({
                orderCellsTop: true,
                fixedHeader: true,
                initComplete: function() {
                    var api = this.api();

                    // For each column
                    api.columns()
                        .eq(0)
                        .each(function(colIdx) {
                            // Set the header cell to contain the input element
                            // var cell = $('.filters td').eq(
                            //     $(api.column(colIdx).header()).index()
                            // );
                            // var title = $(cell).text();
                            // $(cell).html('<input type="text" placeholder="' + title + '" />');

                            // On every keypress in this input
                            $(
                                    'input',
                                    $('.filters td').eq($(api.column(colIdx).header()).index())
                                )
                                .off('keyup change')
                                .on('change', function(e) {
                                    // Get the search value
                                    $(this).attr('title', $(this).val());
                                    var regexr =
                                        '({search})'; //$(this).parents('th').find('select').val();

                                    var cursorPosition = this.selectionStart;
                                    // Search the column for that value
                                    api
                                        .column(colIdx)
                                        .search(
                                            this.value != '' ?
                                            regexr.replace('{search}', '(((' + this.value +
                                                ')))') :
                                            '',
                                            this.value != '',
                                            this.value == ''
                                        )
                                        .draw();
                                })
                                .on('keyup', function(e) {
                                    e.stopPropagation();

                                    $(this).trigger('change');
                                    $(this)
                                        .focus()[0]
                                        .setSelectionRange(cursorPosition, cursorPosition);
                                });
                        });
                    api.columns()
                        .eq(0)
                        .each(function(colIdx) {
                            // Set the header cell to contain the input element
                            // var cell = $('.filters td').eq(
                            //     $(api.column(colIdx).header()).index()
                            // );
                            // var title = $(cell).text();
                            // $(cell).html(
                            //     '<select class="form-select form-select-sm" aria-label=".form-select-sm example"><option value="">pilih semua</option><option value="Aktif">Aktif</option><option value="Tidak Aktif">Tidak aktif</option></select>'
                            // );

                            // On every keypress in this input
                            $(
                                    'select',
                                    $('.filters td').eq($(api.column(colIdx).header()).index())
                                )
                                .on('change', function() {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    api.column(colIdx)
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                                });
                        });
                },

            });
        });
    </script>

    <script type="text/javascript">
        $(document).on("click", "#btn-edit-pic", function() {

            var itemid1 = "{{ $data->admin[0]->user->id }}";
            var itemid2 = "{{ $data->admin[1]->user->id }}";
            document.querySelector('#pic1').value = itemid1;
            document.querySelector('#pic2').value = itemid2;

        });
    </script>
@endsection
@endsection

@extends('layouts.admin-main')

@section('title')
    User
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>User</h1>
            @include('layouts.partials.breadcums')
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    @include('layouts.partials.flash-message')
                    <div class="card">
                        <div class="card-header">
                            <h4>Daftar User</h4>
                            <div class="card-header-action">
                                <a href="{{ route('user') . '/add' }}" class="btn btn-primary"> Tambah User</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table-striped table" id="table-church">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th>Nama </th>
                                            <th>Email </th>
                                            <th>Role</th>
                                            <th>Aksi</th>

                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($data as $dat => $data)
                                            <tr>
                                                <td class="text-center">
                                                    {{ $dat + 1 }}
                                                </td>
                                                <td>{{ $data->name }}</td>
                                                <td>{{ $data->email }}</td>

                                                <td>
                                                    {{-- {{ $data->role->name }} --}}
                                                    @if (!empty($data->getRoleNames()))
                                                        @foreach ($data->getRoleNames() as $v)
                                                            <label class="badge badge-success">{{ $v }}</label>
                                                        @endforeach
                                                    @endif
                                                </td>

                                                <td>

                                                    <a href="#" id="btn-edit" data-toggle="modal"
                                                        data-target="#Modaledit" data-id="{{ $data->id }}"
                                                        data-name="{{ $data->name }}" data-role="{{ $data->role_id }}"
                                                        class="btn btn-outline-primary">
                                                        <i class="fas fa-edit"></i>

                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>

    <div class="modal fade" id="Modaledit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <form action="{{ route('user') }}" method="POST">
            @csrf
            @method('patch')
            <div class="modal-dialog" role="document">

                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="edit">Edit Group</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="hidden" name="id" class="form-control" required id="edit-id">
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Nama</label>
                                <div class="col">
                                    <input type="text" name="name" class="form-control" required id="edit-name"
                                        readonly required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Role</label>
                                <div class="col">
                                    <select class="form-control" name="role" id="edit-role" required>
                                        <option value="">Pilih...</option>
                                        @foreach ($role as $rol)
                                            <option value="{{ $rol->id }}">
                                                {{ $rol->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save </button>
                        </div>
                    </div>
                </div>
        </form>
    </div>

@section('plugin_css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">
@endsection
@section('plugin_js')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function() {
            setTimeout(function() {
                $(".alert").alert('close');
            }, 2000);
        });

        $("#table-church").dataTable({
            "columnDefs": [{
                // "sortable": false,
                // "targets": [2, 3]
            }]
        });
    </script>

    <script type="text/javascript">
        $(document).on("click", "#btn-edit", function() {
            var itemid = $(this).attr('data-id');
            var itemname = $(this).attr('data-name');
            var itemrole = $(this).attr('data-role');
            $('#edit-id').val(itemid);
            $('#edit-name').val(itemname);
            document.getElementById("edit-role").value = itemrole

        });
    </script>
@endsection
@endsection

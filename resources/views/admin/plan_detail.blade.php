@extends('layouts.admin-main')

@section('title')
    Rencana Baca
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Rencana Baca</h1>
            @include('layouts.partials.breadcums')
        </div>

        @include('layouts.partials.flash-message')
        <div class="section-body">


            <div class="row">
                <div class="col-12 col-md-7">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4>{{ $data->description }}</h4>
                            <div class="card-header-action">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addTodo">
                                    Ubah Data
                                </button>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="profile-widget-description pb-0">
                                <div class="form-group row align-items-center">
                                    <label class="col-md-4 text-md-right text-left">Rencana Baca</label>
                                    <div class="col">
                                        {{ $data->description }}
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table-striped table" id="table-todo">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                Hari
                                            </th>
                                            <th>Chapter </th>

                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($data->list as $todo)
                                            <tr>
                                                <td class="text-center">
                                                    {{ $todo->day }}
                                                </td>
                                                <td>{{ $todo->chapter_verse }}</td>

                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-12 col-md-5">
                    <div class="card card-warning">
                        <div class="card-header">
                            <h4>Group</h4>
                        </div>
                        <div class="card-body">

                            <div class="table-responsive">
                                <table class="table-striped table" id="table-user">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                No
                                            </th>
                                            <th>Nama Group </th>

                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($data->group->where('type', 'group') as $k => $group)
                                            <tr>
                                                <td class="text-center">
                                                    {{ $k + 1 }}
                                                </td>
                                                <td>{{ $group->name }}</td>

                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="addTodo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <form action="{{ route('rencana.baca') }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('put')
            <input type="hidden" name="rp_id" value="{{ request('id') }}">

            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel">Ubah Data</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="form-group row">
                            <label for="description" class="col-sm-3 col-form-label">Deskripsi</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="description" name="description" required
                                    placeholder="" value="{{ $data->description }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-sm-3 col-form-label">Upload File</label>
                            <div class="col-sm-9">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="todo"
                                        accept=" application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                                    <label class="custom-file-label" for="customFile">{{ $data->file_name }}</label>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save </button>
                    </div>
                </div>
            </div>

        </form>
    </div>






@section('plugin_css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">
@endsection
@section('plugin_js')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function() {
            setTimeout(function() {
                $(".alert").alert('close');
            }, 2000);
        });

        $("#table-admin").dataTable({
            scrollY: '150px',
            paging: false,
            "lengthChange": false
        });
        $("#table-user").dataTable({
            "columnDefs": [{

            }]
        });
        $("#table-todo").dataTable({
            // "info": false,
            searching: false,
            scrollY: '400px',
            paging: false,
            "lengthChange": false
        });

        $(document).ready(function() {
            $('input[type="file"]').on("change", function() {
                let filenames = [];
                let files = this.files;
                if (files.length > 1) {
                    filenames.push("Total Files (" + files.length + ")");
                } else {
                    for (let i in files) {
                        if (files.hasOwnProperty(i)) {
                            filenames.push(files[i].name);
                        }
                    }
                }
                $(this)
                    .next(".custom-file-label")
                    .html(filenames.join(","));
            });
        });
    </script>

    <script type="text/javascript">
        $(document).on("click", "#btn-show", function() {

            var itemid = $(this).attr('data-id');
            var itemname = $(this).attr('data-name');
            var itemuser = JSON.parse($(this).attr('data-user'))
            $('#edit-id').val(itemid);
            $('#edit-name').val(itemname);
            document.getElementById("itemname").innerHTML = itemuser.name;
            document.getElementById("itemaddress").innerHTML = itemuser.address;
            document.getElementById("link").href = "{{ route('chat') }}/p/" + itemuser.id + "";
            // console.log(itemid, itemname);
        });
        // $(document).on("click", "#btn-delete", function() {
        //     var itemid = $(this).attr('data-id');
        //     var itemname = $(this).attr('data-name');
        //     $('#delete-id').val(itemid);
        //     $('#delete-name').val(itemname);
        //     console.log(itemid, itemname);
        // });
    </script>
@endsection
@endsection

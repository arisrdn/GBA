<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>@yield('title') | {{ env('APP_NAME') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('logo-GBA.png') }}">

    <!-- General CSS Files -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous" />
    <!-- CSS Libraries -->
    @yield('plugin_css')
    <!-- Template CSS -->
    {{-- <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/components.css') }}"> --}}
    @vite(['resources/css/style.css', 'resources/css/components.css'])



</head>

<body>
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>
            <nav class="navbar navbar-expand-lg main-navbar">
                @include('layouts.partials.topnav')

            </nav>
            <div class="main-sidebar sidebar-style-2">
                @include('layouts.partials.sidebar')
            </div>


            <!-- Main Content -->
            <div class="main-content">
                @yield('content')
            </div>
            <footer class="main-footer footer-fixed">
                @include('layouts.partials.footer')
            </footer>
        </div>
    </div>

    {{-- <script src="{{ asset('js/app.js') }}?{{ uniqid() }}"></script> --}}
    <!-- General JS Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>

    <script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-messaging.js"></script>

    {{-- axios --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.27.2/axios.min.js"
        integrity="sha512-odNmoc1XJy5x1TMVMdC7EMs3IVdItLPlCeL5vSUPN2llYKMJ2eByTTAIiiuqLg+GdNr9hF6z81p27DArRFKT7A=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>


    <script src="{{ asset('assets/js/stisla.js') }}"></script>
    <script src="{{ asset('assets/js/scripts.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    {{-- @vite(['resources/react/notify.jsx']) --}}

    <script>
        $(document).ready(function() {
            $("#popup").modal('show');
        });
    </script>
    @if (Session::has('popup'))
        @php
            $all = PendingTask::getall();
            $leave = PendingTask::getleave();
            $join = PendingTask::getjoin();
            $tf = PendingTask::gettf();
        @endphp
        <div id="popup" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="dropdown-menu dropdown-list dropdown-menu-right show" style="width:100%">
                        <div class="modal-header">
                            <h5 class="modal-title">Persetujuan</h5>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="dropdown-header" style="border-bottom:1px dashed black"></span>

                        </div>
                        <div class="dropdown-list-content dropdown-list-icons" tabindex="2"
                            style="overflow: hidden; outline: none;">


                            <a href="{{ route('join') }}" class="dropdown-item dropdown-item-unread">
                                <div class="dropdown-item-icon bg-success text-white">
                                    <i class="fas fa-users"></i>
                                </div>
                                <div class="dropdown-item-desc">
                                    Permintaan Masuk Grup
                                    <div class="time text-primary">{{ $join->count() }} Permintaan</div>
                                </div>
                            </a>

                            <a href="{{ route('leave') }}" class="dropdown-item dropdown-item-unread">
                                <div class="dropdown-item-icon bg-warning text-white">
                                    <i class="fas fa-users"></i>
                                </div>
                                <div class="dropdown-item-desc">
                                    Permintaan Keluar Grup
                                    <div class="time text-primary">{{ $leave->count() }} Permintaan</div>
                                </div>
                            </a>

                            <a href="{{ route('transfer') }}" class="dropdown-item dropdown-item-unread">
                                <div class="dropdown-item-icon bg-primary text-white">
                                    <i class="fas fa-users"></i>
                                </div>
                                <div class="dropdown-item-desc">
                                    Permintaan Transfer Grup
                                    <div class="time text-primary">{{ $tf->count() }} Permintaan</div>
                                </div>
                            </a>

                        </div>
                        <div id="ascrail2001" class="nicescroll-rails nicescroll-rails-vr"
                            style="width: 9px; z-index: 1000; cursor: default; position: absolute; top: 58px; left: 341px; height: 350px; display: none; opacity: 0.3;">
                            <div class="nicescroll-cursors"
                                style="position: relative; top: 0px; float: right; width: 7px; height: 0px; background-color: rgb(66, 66, 66); border: 1px solid rgb(255, 255, 255); background-clip: padding-box; border-radius: 5px;">
                            </div>
                        </div>
                        <div id="ascrail2001-hr" class="nicescroll-rails nicescroll-rails-hr"
                            style="height: 9px; z-index: 1000; top: 399px; left: 0px; position: absolute; cursor: default; display: none; opacity: 0.3;">
                            <div class="nicescroll-cursors"
                                style="position: absolute; top: 0px; height: 7px; width: 0px; background-color: rgb(66, 66, 66); border: 1px solid rgb(255, 255, 255); background-clip: padding-box; border-radius: 5px; left: 0px;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @php
            Session::forget('popup');
        @endphp
    @endif

    @yield('plugin_js')

</body>

</html>

<ul class="sidebar-menu">
    <li class="menu-header"></li>
    <li class="menu-header">Dashboard</li>
    <li class="nav-item {{ Request::route()->getName() == 'dashboard' ? ' active' : '' }}">
        <a href="{{ route('dashboard') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
    </li>
    <li class="menu-header">Anggota</li>
    {{-- <li class="nav-item dropdown {{ request()->is('anggota*') ? 'active' : '' }}">
        <a href="#" class="nav-link has-dropdown" data-toggle="dropdown">
            <i class="fas fa-users"></i>
            <span>Anggota</span></a>
        <ul class="dropdown-menu">
            <li class="nav-item {{ request()->is('anggota/group*') ? ' active' : '' }}">
                <a href="{{ route('group') }}" class="nav-link">
                    <span>Group</span></a>
            </li>
            <li class="nav-item {{ request()->is('anggota/mandiri*') ? ' active' : '' }}">
                <a href="{{ route('group2') }}" class="nav-link">
                    <span>Mandiri</span></a>
            </li>
        </ul>
    </li> --}}
    <li class="nav-item {{ request()->is('group*') ? ' active' : '' }}">
        <a href="{{ route('group') }}" class="nav-link"><i class="fas fa-users"></i><span>Grup</span> </a>
    </li>
    <li class="nav-item {{ request()->is('individu*') ? ' active' : '' }}">
        <a href="{{ route('group2') }}" class="nav-link"><i class="fas fa-user"></i><span>Individu</span></a>
    </li>
    <li class="nav-item dropdown {{ request()->is('chat*') ? 'active' : '' }}">
        <a href="#" class="nav-link has-dropdown" data-toggle="dropdown">
            <i class="fas fa-comment-alt"></i>
            <span>Chat</span></a>
        <ul class="dropdown-menu">
            <li class="{{ request()->is('chat/group') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('chat') }}">Grup</a>
            </li>
            <li class="{{ request()->is('chat/broadcast') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('broadcast') }}">Broadcast</a>
            </li>

        </ul>
    </li>
    @php
        $all = PendingTask::getall();
        $leave = PendingTask::getleave();
        $join = PendingTask::getjoin();
        $tf = PendingTask::gettf();
        
    @endphp
    <li class="nav-item dropdown {{ request()->is('persetujuan*') ? 'active' : '' }}">
        <a href="#" class="nav-link has-dropdown {{ !$all->isEmpty() ? 'beep-custom' : '' }}"
            data-toggle="dropdown">
            <i class="fas fa-check-double"></i>
            <span>Persetujuan</span></a>
        <ul class="dropdown-menu">
            <li class="{{ request()->is('persetujuan/join') ? 'active' : '' }}">
                <a class="nav-link {{ !$join->isEmpty() ? 'beep beep-sidebar' : '' }}"
                    href="{{ route('join') }}">Join</a>
            </li>
            <li class="{{ request()->is('persetujuan/leave') ? 'active' : '' }}">
                <a class="nav-link {{ !$leave->isEmpty() ? 'beep beep-sidebar' : '' }}"
                    href="{{ route('leave') }}">Leave</a>
            </li>
            <li class="{{ request()->is('persetujuan/transfer') ? 'active' : '' }}">
                <a class="nav-link {{ !$tf->isEmpty() ? 'beep beep-sidebar' : '' }}"
                    href="{{ route('transfer') }}">Transfer</a>
            </li>
        </ul>
    </li>
    @if (Auth::user()->can('menu-admin/pic') || Auth::user()->can('menu-gereja') || Auth::user()->can('menu-rencana-baca'))
        <li class="menu-header">Data Master</li>
    @endif

    @can('menu-admin/pic')
        <li class="nav-item {{ request()->is('user*') ? ' active' : '' }}">
            <a href="{{ route('user') }}" class="nav-link"><i class="fas fa-id-card"></i><span>PIC/Admin</span></a>
        </li>
    @endcan
    @can('menu-gereja')
        {{-- <li class="nav-item dropdown {{ request()->is('gereja*') ? 'active' : '' }}">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-church"></i>
                <span>Gereja</span></a>
            <ul class="dropdown-menu">
                <li class="{{ request()->is('gereja/pusat') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('gereja') }}">Pusat</a>
                </li>
                <li class="{{ request()->is('gereja/cabang') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('gereja.cabang') }}">Cabang</a>
                </li>
            </ul>
        </li> --}}
        <li class="nav-item {{ request()->is('gereja*') ? ' active' : '' }}">
            <a href="{{ route('gereja') }}" class="nav-link"><i class="fas fa-church"></i><span>Gereja</span></a>
        </li>
    @endcan
    @can('menu-rencana-baca')
        <li class="nav-item {{ request()->is('rencana*') ? ' active' : '' }}">
            <a href="{{ route('rencana.baca') }}" class="nav-link"><i class="fas fa-list-ul"></i><span>Rencana
                    Baca</span></a>
        </li>
    @endcan

    {{-- <li class="menu-header">report</li> --}}

    <li class="menu-header">Pengaturan</li>
    <li class="nav-item {{ request()->is('profile*') ? ' active' : '' }}">
        <a href="{{ route('profile') }}" class="nav-link">
            <i class="fas fa-user-check"></i>
            <span>profile</span></a>
    </li>
    <li class="nav-item {{ request()->is('pengaturan*') ? ' active' : '' }}">
        <a href="{{ route('setting') }}" class="nav-link">
            <i class="fas fa-cogs"></i>
            <span>Pengaturan</span></a>
    </li>
</ul>

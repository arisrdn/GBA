<form class="form-inline mr-auto" action="{{ url('/') }}">
    <ul class="navbar-nav mr-3">
        <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
        <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a>
        </li>
    </ul>
    <div class="search-element">

        {{-- @include('admin.partials.searchhistory') --}}
    </div>
</form>
<ul class="navbar-nav navbar-right">
    <li class="dropdown dropdown-list-toggle">
        {{-- <div id="notify"></div> --}}
        @php
            $all = PendingTask::getall();
            $leave = PendingTask::getleave();
            $join = PendingTask::getjoin();
            $tf = PendingTask::gettf();
        @endphp
        <a href="#" data-toggle="dropdown"
            class="nav-link notification-toggle nav-link-lg {{ !$all->isEmpty() ? 'beep' : '' }}"><i
                class="far fa-bell"></i></a>
        <div class="dropdown-menu dropdown-list dropdown-menu-right">
            <div class="dropdown-header">Permintaan <span class="badge badge-info">{{ $all->count() }}</span>

            </div>
            <div class="dropdown-list-content dropdown-list-icons">
                @if (!$all->isEmpty())
                    {{-- @for ($i = 1; $i < 1; $i++)
                        <a href="#" class="dropdown-item dropdown-item-unread">
                            <div class="dropdown-item-icon bg-primary text-white">
                                <i class="fas fa-code"></i>
                            </div>
                            <div class="dropdown-item-desc">
                                Permintaan Masuk
                                <div class="time text-primary">2 Min Ago</div>
                            </div>
                        </a>
                    @endfor --}}
                    @if (!$join->isEmpty())
                        <a href="{{ route('join') }}" class="dropdown-item dropdown-item-unread">
                            <div class="dropdown-item-icon bg-success text-white">
                                <i class="fas fa-users"></i>
                            </div>
                            <div class="dropdown-item-desc">
                                Permintaan Masuk Group
                                <div class="time text-primary">{{ $join->count() }} Permintaan</div>
                            </div>
                        </a>
                    @endif
                    @if (!$leave->isEmpty())
                        <a href="{{ route('leave') }}" class="dropdown-item dropdown-item-unread">
                            <div class="dropdown-item-icon bg-warning text-white">
                                <i class="fas fa-users"></i>
                            </div>
                            <div class="dropdown-item-desc">
                                Permintaan Keluar Group
                                <div class="time text-primary">{{ $leave->count() }} Permintaan</div>
                            </div>
                        </a>
                    @endif
                    @if (!$tf->isEmpty())
                        <a href="{{ route('transfer') }}" class="dropdown-item dropdown-item-unread">
                            <div class="dropdown-item-icon bg-primary text-white">
                                <i class="fas fa-users"></i>
                            </div>
                            <div class="dropdown-item-desc">
                                Permintaan Transfer Group
                                <div class="time text-primary">{{ $tf->count() }} Permintaan</div>
                            </div>
                        </a>
                    @endif
                @else
                    <p class="text-muted p-2 text-center">Tidak ada permintaan!</p>
                @endif
            </div>
    </li>
    <li class="dropdown"><a href="#" data-toggle="dropdown"
            class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="image" src="https://ui-avatars.com/api/?name=
                {{ Auth::user()->name }}"
                class="rounded-circle">
            {{-- <figure class="avatar bg-danger mr-2 text-white" data-initial="UM"></figure> --}}
            <div class="d-sm-none d-lg-inline-block">Hi,
                {{ Auth::user()->name }}
            </div>
        </a>
        <div class="dropdown-menu dropdown-menu-right">
            <div class="dropdown-title">Welcome,
                {{ Auth::user()->name }}

            </div>
            <a href="
            {{ route('profile') }}
            " class="dropdown-item has-icon">
                <i class="far fa-user"></i> Profile Settings
            </a>
            <div class="dropdown-divider"></div>

            <form method="POST" action="{{ route('logout') }}">
                @csrf
                <a onclick="event.preventDefault();
                this.closest('form').submit();"
                    class="dropdown-item has-icon text-danger" style="cursor:pointer">
                    <i class="fas fa-sign-out-alt"></i> Logout
                </a>

            </form>
        </div>
    </li>
</ul>

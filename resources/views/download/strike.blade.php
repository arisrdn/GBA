<!DOCTYPE html>
<html>

<head>
    <style>
        table,
        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
</head>

<body>



    <table style="width:100%">
        <tr>
            <th colspan="7">
                <h2>TOP 20 STREAK </h2>
            </th>
        </tr>

        <tr>
            <th colspan="7">
                {{ today()->format('d-M-Y') }}
            </th>
        </tr>
        <tr>
            <th class="text-center">
                NO
            </th>
            <th>Nama</th>
            <th>Nama Group</th>
            <th>Gereja</th>
            <th>Cabang</th>
            <th>Streak</th>
            <th>#PERFECT WEEK</th>

        </tr>

        @php
            $no = 1;
        @endphp
        @foreach ($member->sortByDesc('strike')->where('strike', '>', 0)->take(20) as $dat)
            @if ($dat->overdue->d <= 1)
                <tr>
                    <td>
                        {{ $no++ }}</td>
                    <td>{{ $dat->user->name }}</td>
                    <td>{{ $dat->group->name }}</td>
                    <td>{{ $dat->user->church_branch->church->name }}</td>
                    <td>{{ $dat->user->church_branch->name }}</td>
                    <td>{{ $dat->strike }}</td>
                    <td>{{ floor($dat->strike / 7) }}
                    </td>

                </tr>
            @endif
        @endforeach
    </table>
</body>

</html>

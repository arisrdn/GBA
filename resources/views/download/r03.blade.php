<!DOCTYPE html>
<html>

<head>
    <style>
        table,
        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
</head>

<body>
    @php
        $plan = '';
        if (!isset($group->plan)) {
            $plan = '(di hapus)' . $group->plan_with_trashed->description;
        } else {
            $plan = $group->plan->description;
        }
    @endphp

    <table style="width:100%">
        <tr>
            <th colspan="5">
                <h2>Rekap Anngota Grup</h2>
            </th>
        </tr>
        <tr>
            <th colspan="5">Nama Group: {{ $group->name }}</th>
        </tr>
        <tr>
            <th colspan="5">Rencana Baca: {{ $plan }}</th>
        </tr>
        <tr>
            <th colspan="5"> {{ today()->format('d-M-Y') }}</th>
        </tr>
        <tr>
            <th>NO</th>
            <th>NAMA</th>
            <th>STATUS</th>
            <th>TTERAKHIR BACA</th>
            <th>STREAK</th>

        </tr>


        @foreach ($group->member as $dat => $da)
            <tr>
                <td>
                    {{ $dat + 1 }}
                </td>
                <td>{{ $da->user->name }}</td>

                <td>
                    @if ($da->todo->sortByDesc('id')->first())
                        @php
                            $readdate = \Carbon\Carbon::parse($da->todo->sortByDesc('id')->first()->created_at)->addday(30);
                            $status = 'ACTIVE';
                            // $retVal = ? ($status = 'INACTIVE') : ($status = 'ACTIVE');
                        @endphp
                        @if ($readdate <= today())
                            <span class="badge badge-warning">{{ 'Aktif' }}</span>
                        @else
                            <span class="badge badge-success">{{ 'Tidak Aktif' }}</span>
                        @endif
                    @else
                        @if ($da->updated_at->addday(30) <= today())
                            <span class="badge badge-warning">{{ 'Aktif' }}</span>
                        @else
                            <span class="badge badge-success">{{ 'Tidak Aktif' }}</span>
                        @endif
                    @endif

                </td>
                <td>
                    @if ($last = $da->todo->where('read_at', '!=', null)->sortByDesc('id')->first())
                        {{ $last->todolist->chapter_verse }} <br />
                        {{ date('j M Y', strtotime($last->read_at)) }}
                    @else
                        -
                    @endif

                </td>
                <td>
                    @if (isset($last))
                        @if ($last->read_at == today() || $last->read_at == today()->subday())
                            {{ $da->strike }}
                        @else
                            0
                        @endif
                    @endif


                </td>
            </tr>
        @endforeach
    </table>
</body>

</html>

<!DOCTYPE html>
<html>

<head>
    <style>
        table,
        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
</head>

<body>

    <table style="width:100%">
        <tr>
            <th colspan="5">
                <h2>MEMBER TERLAMABAT > 3 HARI</h2>
            </th>
        </tr>
        <tr>
            <th colspan="5"> {{ today()->format('d-M-Y') }}</th>
        </tr>
        <tr>
            <th class="text-center">
                NO
            </th>
            <th>Nama</th>
            <th>Nama Group</th>
            <th>Terlamabat (Hari)</th>
            <th>WA No</th>
        </tr>


        @foreach ($member as $key => $dat)
            @if ($dat->overdue->d >= 3)
                <tr>
                    <td>
                        {{ $key + 1 }}</td>
                    <td>{{ $dat->user->name }}</td>
                    <td>{{ $dat->group->name }}</td>
                    <td>{{ $dat->overdue->days }}</td>
                    <td>
                        <a href="https://wa.me/+{{ $dat->user->country->phone_code . $dat->user->whatsapp_no }}"
                            target="_blank" rel="noopener noreferrer">
                            {{ '(+' . $dat->user->country->phone_code . ')' . $dat->user->whatsapp_no }}
                        </a>
                    </td>

                </tr>
            @endif
        @endforeach
    </table>
</body>

</html>

<!DOCTYPE html>
<html>

<head>
    <style>
        table,
        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
</head>

<body>


    <table style="width:100%">
        <tr>
            <th colspan="8">
                <h2>REKAP MEMBER LIST</h2>
            </th>
        </tr>
        <tr>
            <th colspan="8">
                ( {{ today()->format('d-M-Y') }})
            </th>
        </tr>
        <tr>
            <th>NO</th>
            <th>NAMA</th>
            <th>JENIS KELAMIN</th>
            <th>NO WHATSAPP</th>
            <th>PROVINSI</th>
            <th>KOTA</th>
            <th>STATUS</th>
            <th>GROUP</th>
            {{-- <th>RENCANA BACA</th> --}}
        </tr>


        @foreach ($member as $dat => $da)
            <tr>
                <td>
                    {{ $dat + 1 }}
                </td>
                <td>{{ $da->name }}</td>
                <td>
                    @if ($da->gender == 'male')
                        Pria
                    @else
                        Wanita
                    @endif
                </td>
                <td>
                    <a href="https://wa.me/+{{ $da->country->phone_code . $da->whatsapp_no }}" target="_blank"
                        rel="noopener noreferrer">
                        {{ '(+' . $da->country->phone_code . ')' . $da->whatsapp_no }}
                    </a>
                </td>
                <td>
                    {{ $da->province }}
                </td>
                <td>
                    {{ $da->city }}
                </td>
                <td>
                    @php
                        $status = '';
                        $group = '-';
                        $plan = '-';
                        if (isset($da->groupmember)) {
                            $last = $da->groupmember->todo
                                ->where('read_at', '!=', null)
                                ->sortByDesc('id')
                                ->first();
                            $group = $da->groupmember->group->name;
                            $read = $last->read_at->addday(30);
                            if ($read <= today()) {
                                $status = 'Tidak Aktif';
                            } else {
                                $status = 'Aktif';
                            }
                        } else {
                            $status = 'Belum memiliki Bacaan Aktif';
                        }
                        
                    @endphp
                    {{ $status }}
                </td>
                <td>
                    {{ $group }}
                </td>
                {{-- <td>
                    {{ $plan }}
                </td> --}}
            </tr>
        @endforeach
    </table>
</body>

</html>

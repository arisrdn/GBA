<?php

namespace App\Helpers;

class Setting
{
    // Group
    const STARTTIME = "00:15";
    const ENDTIME = "23:30";
    const MAXUSER = 100;
    const MAXGROUPADDUSER = 14;
}

function allUpper($str)
{
    return strtoupper($str);
}

<?php

namespace App\Helpers;

use App\Models\WaitingList;
use Illuminate\Support\Facades\DB;

class Pending
{
    public static function getall()
    {
        $all = WaitingList::all();
        return $all;
    }
    public static function getjoin()
    {
        $all =
            WaitingList::where('type', "join")->get();
        return $all;
    }
    public static function getleave()
    {
        $all =
            WaitingList::where('type', "leave")->get();
        return $all;
    }
    public static function gettf()
    {
        $all =
            WaitingList::where('type', "transfer")->get();
        return $all;
    }
}

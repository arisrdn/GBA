<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MemberTodolist extends Model
{
    use HasFactory;
    protected $fillable = [
        'group_member_id',
        'reading_plan_list_id',
        'chapter_verse',
        'read_at',
        'schedule',
        'id'
    ];
    protected $hidden = ['created_at', 'updated_at'];
    protected $dates = ['created_at', 'updated_at', 'read_at', "schedule"];


    public function todolist()
    {
        return $this->belongsTo(ReadingPlanList::class, "reading_plan_list_id", 'id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'group_id',
        'group_name',
        'priode_start',
        'priode_end',
        'reading_plan_description',
        "reading_plan_id",
        'reading_plan_count',
        'readed',
        'strike',
        'status',
    ];
    protected $hidden = ['created_at', 'updated_at', 'user_id', 'group_id'];
}

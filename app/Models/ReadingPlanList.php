<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReadingPlanList extends Model
{
    use HasFactory;

    protected $fillable = [
        'day',
        'reading_plan_id',
        'chapter_verse',
    ];
    protected $hidden = ['created_at', 'updated_at'];
    // public function readed()
    // {
    //     return $this->hasOne(GroupTodolist::class, "group_todolist_id", 'id');
    // }
}

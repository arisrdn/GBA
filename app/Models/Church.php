<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Church extends Model
{
    use HasFactory, SoftDeletes;


    protected $fillable = [
        'name',
    ];

    public function branch()
    {
        return $this->hasMany(ChurchBranch::class, 'church_id');
    }
    public function user()
    {
        return $this->hasOneThrough(User::class, ChurchBranch::class);
    }
    public function userMale()
    {
        return $this->hasOneThrough(User::class, ChurchBranch::class)->where("users.gender", "male");
    }
    public function userFemale()
    {
        return $this->hasOneThrough(User::class, ChurchBranch::class)->where("users.gender", "female");
    }
}

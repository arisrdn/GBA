<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReadingPlanType extends Model
{
    use HasFactory;
    public function plan()
    {
        return $this->hasMany(ReadingPlan::class, "reading_plan_type_id");
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChurchBranch extends Model
{
    use HasFactory, SoftDeletes;


    protected $fillable = [
        'name',
        'address',
        'church_id'
    ];

    public function church()
    {
        return $this->belongsTo(Church::class, 'church_id');
    }
    public function user()
    {
        return $this->hasMany(User::class, "church_branch_id", "id");
    }
    public function useractive()
    {
        return $this->hasManyThrough(GroupMember::class, User::class);
    }
    public function withChurch()
    {
        return $this->belongsTo(Church::class)->with('church');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupMember extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'group_id',
        'approved_at',
        'leave_at',
        'complete_at',
    ];

    protected $hidden = [
        'leave_at',
        'complete_at',
    ];
    public function group()
    {
        return $this->belongsTo(Group::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class, "user_id", 'id');
    }
    public function todo()
    {
        return $this->hasMany(MemberTodolist::class);
    }
    public function latestRead()
    {
        return $this->hasOne(MemberTodolist::class)->where('read_at', '!=', null)->orderByDesc("read_at");
    }
    // public function latestOrder()
    // {
    //     return $this->latestRead()->latestOfMany();
    // }
}

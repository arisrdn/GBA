<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReadingPlan extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'description',
        'file_name',
        'reading_plan_type_id'
    ];
    protected $hidden = ['updated_at'];

    public function list()
    {
        return $this->hasMany(ReadingPlanList::class);
    }
    public function group()
    {
        return $this->hasMany(Group::class);
    }
    public function grouponly()
    {
        return $this->hasMany(Group::class)->where("groups.type", "group")->where("groups.end_date", ">", today());
    }
    public function member()
    {
        return $this->hasOneThrough(GroupMember::class, Group::class,);
    }
    public function type()
    {
        return $this->belongsTo(ReadingPlanType::class, "reading_plan_type_id");
    }
}

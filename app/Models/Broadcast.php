<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Broadcast extends Model
{
    use HasFactory;
    protected $fillable = [
        'from_id',
        'to_id',
        'message',
        "title"
    ];

    protected $hidden = [
        'created_at',
        'Updated_at',
        // 'role_id',
        " updated_at",
        'to_id',
    ];
    public function sender()
    {
        return $this->belongsTo(User::class, 'from_id', 'id')->select(['id', 'name']);
    }
}

<?php

namespace App\Console\Commands;

use App\Helpers\Notify;
use App\Models\Group;
use App\Models\GroupChat;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SendGroupAct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'group:act';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'broadcast act group';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $sender = 1;
        $group = Group::where("type", "group")->where('end_date', ">", today())->where("end_date", ">", today())->get();
        foreach ($group as $key => $g) {
            $output[$key] = 'Rekap Bacaan ' . today()->format('d/m/Y') . "<br>";
            foreach ($g->member as $k => $m) {
                $last = $m->todo->where('read_at', '!=', null)->sortByDesc('id')->first();
                if ($last) {
                    $ls = $last->todolist->chapter_verse;
                } else {
                    $ls = "belum memulai bacaan";
                }
                $output[$key] .= $k + 1 . ". " . $m->user->name . " - " . $ls . "<br> ";
            }


            $data = GroupChat::create([
                'message' => $output[$key],
                'group_id' => $g->id,
                'from_id' => $sender
            ]);
            Notify::messageGroup("system", "Rekap Bacaan " . today()->format('d/m/Y'), $g->id);
            Log::info('Cronjob group:act dijalankan');
        }
    }
}

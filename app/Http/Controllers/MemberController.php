<?php

namespace App\Http\Controllers;

use App\Helpers\Notify;
use App\Helpers\Setting;
use App\Imports\GroupActivityImport;
use App\Models\Group;
use App\Models\GroupMember;
use App\Models\GroupTodolist;
use App\Models\History;
use App\Models\MemberTodolist;
use App\Models\ReadingPlanList;
use App\Models\User;
use App\Models\WaitingList;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;

class MemberController extends Controller
{
    public function index()
    {
        //
        return view('admin.profile');
    }
    public function changepassword(Request $request)
    {
        // dd($request);
        // $id = auth('sanctum')->user()->id;
        $request->validate([
            'old_password' => 'required',
            'password' => 'required|string|min:8|confirmed'
        ]);

        $id = auth()->user();
        $data = User::find($id);

        if (Hash::check($request->old_password, $data->password)) {
            $data->fill([
                'password' => Hash::make($request->password)
            ])->save();
            return back()->with('success', 'Berhasil Di ubah');
        } else {
            return back()->with('error', 'password lama tidak sesuai');
        }
    }
    public function update(Request $request)
    {
        // dd($request);
        $request->validate([
            'name' => 'required|string|max:255',
            'whatsapp_no' => 'required',
            'gender' => 'required',

            'birth_date' => 'required',
            'province' => 'required',
            'city' => 'required',

        ]);

        $id = auth()->user()->id;
        $data = User::where("id", $id)->first();
        // dd($data);
        $data->name = $request->name;
        $data->whatsapp_no = $request->whatsapp_no;
        $data->gender = $request->gender;
        $data->birth_date = $request->birth_date;
        $data->province = $request->province;
        $data->city = $request->city;
        $data->save();
        if ($data) {


            return back()->with('success', 'Berhasil Di Update');
        } else {
            return back()->with('error', 'Terjadi Kesalahan');
        }
    }


    public function indexleave()
    {
        //
        // Excel::import(new GroupActivityImport(2), public_path('/files/upload.xlsx'));
        $data = GroupMember::where('approved_at', null)->get();
        $data2 = GroupMember::where('approved_at', "!=", null)
            ->where('reason_leave_id', '!=', null)
            ->where('leave_at', null)
            ->get();
        $member = WaitingList::where('type', "leave")->get();
        return view('admin.leave', compact("member"));
    }
    public function indexjoin()
    {
        if (auth()->user()->role_id == 1) {
            $group = Group::where('end_date', ">", today())->where("end_date", ">", today())->withCount('user')->having('user_count', "<", Setting::MAXUSER)
                ->get();
        } else {
            $group = Group::where('end_date', ">", today())->where("start_date", ">", today()->subDay(Setting::MAXGROUPADDUSER))->where("end_date", ">", today())->withCount('user')->having('user_count', "<", Setting::MAXUSER)
                ->whereHas('admin', function ($q1) {
                    $q1->where('group_admins.user_id', auth()->user()->id);
                })->get();
        }
        // $group = Group::where('end_date', ">", today())->withCount('user')->having('user_count', "<", Setting::MAXUSER)

        $member = WaitingList::where('type', "join")->get();
        // $group2 = Group::where('type', "=", "p")->get();
        $group2 = Group::where('type', "=", "individu")->get();
        // dd($member);
        return view('admin.approve', compact("member", "group", "group2"));
    }
    public function indextf()
    {
        $group = Group::where('end_date', ">", today())->withCount('user')->having('user_count', "<", Setting::MAXUSER)
            ->get();
        $member = WaitingList::where('type', "transfer")->get();

        return view('admin.transfer', compact("member", "group"));
    }

    public function storejoin(Request $request)
    {

        try {
            //code...
            $wl = WaitingList::find($request->wl_id);
            $group = Group::find($request->group_id);
            $todo = ReadingPlanList::where("reading_plan_id", $wl->reading_plan_id)->get();
            $strdate = today();
            // dd($strdate->addDays(40 - 1));
            // dd(date("dmyHi", strtotime(now())));
            $data = GroupMember::create([
                'user_id' => $wl->user_id,
                'group_id' => $request->group_id,
                'approved_at' => Carbon::now()
            ]);

            if ($group->type == "group") {
                $strdate = $data->group->start_date;
            }
            // dd($strdate);

            if ($data) {
                foreach ($todo as $n => $val) {
                    $scl = Carbon::parse($strdate);
                    // echo  $scl . "->" . $scl->addDays($val->day) . "<br>";
                    MemberTodolist::create([
                        // 'id' => $data->id . date("dmY", strtotime(today())) . $n + 1,
                        'id' => $data->id . date("dmyHi", strtotime(now())) . $n + 1,
                        'group_member_id' => $data->id,
                        'reading_plan_list_id' => $val->id,
                        'chapter_verse' => $val->chapter_verse,
                        'schedule' => $scl->addDays($val->day - 1)
                    ]);
                }
                Notify::approvejoingroup($wl->user_id);

                $wl->delete();
                return back()->with('success', 'Berhasil Di Approve');
            } else {
                return back()->with('error', 'Terjadi Kesalahan');
            }


            // dd($todo);
            return back()->with('status', 'Berhasil');
        } catch (\Throwable $th) {
            // throw $th;
            return back()->with('status', 'galat');
        }
    }
    public function storeleave(Request $request)
    {
        try {
            $wl = WaitingList::find($request->wl_id);
            $data  = GroupMember::where("user_id", $wl->user_id)->first();
            // dd($data->group->plan->description);
            $planid = null;
            $plandes = null;
            if (!isset($data->group->plan)) {
                # code...
                $planid = $data->group->plan_with_trashed->id;
                $plandes = $data->group->plan_with_trashed->description;
            } else {
                $planid = $data->group->plan->id;
                $plandes = $data->group->plan->description;
            }

            $hs = History::create([
                'user_id' => $wl->user_id,
                'group_id' => $data->group_id,
                'group_name' => $data->group->name,
                'priode_start' => $data->group->start_date,
                'priode_end' => $data->group->end_date,
                'reading_plan_id' => $planid,
                'reading_plan_description' => $plandes,
                'readed' => $data->todo->where("read_at", "!=", null)->count(),
                'strike' => $data->strike,
                'status' => "leave"
            ]);
            if ($hs) {
                $data->delete();
                $wl->delete();
                return back()->with('success', 'Berhasil Di Approve');
            } else {
                return back()->with('error', 'Terjadi Kesalahan');
            }
        } catch (\Throwable $th) {
            throw $th;

            return back()->with('error', 'Terjadi Kesalahan');
        }
    }
    public function storetf(Request $request)
    {
        try {
            $wl = WaitingList::find($request->wl_id);
            // dd($wl->data);

            // $gl = GroupMember::find($request->gm_id);
            // dd($gl);
            $data = GroupMember::where("user_id", $wl->user_id)->first();
            $hs = History::create([
                'user_id' => $wl->user_id,
                'group_id' => $data->group_id,
                'group_name' => $data->group->name,
                'priode_start' => $data->group->start_date,
                'priode_end' => $data->group->end_date,
                'reading_plan_description' => $data->group->plan->description,
                'reading_plan_count' => $data->todo->count(),
                'readed' => $data->todo->where("read_at", "!=", null)->count(),
                'strike' => $data->strike,
                'status' => "tranfer"

            ]);
            if ($wl->data) {
                $gm = GroupMember::create([
                    'user_id' => $wl->user_id,
                    'group_id' => $request->group_id,
                    'approved_at' => Carbon::now()
                ]);
            }

            if ($data) {
                $data->delete();
                $wl->delete();
                return back()->with('success', 'Berhasil Di Approve');
            } else {
                return back()->with('error', 'Terjadi Kesalahan');
            }
        } catch (\Throwable $th) {
            throw $th;
            return back()->with('status', 'galat');
        }
    }

    public function rejectjoin(Request $request)
    {
        // dd($request);
        try {
            $wl = WaitingList::find($request->reject_id);

            if ($wl) {

                $wl->delete();
                return back()->with('success', 'Berhasil Di Reject');
            } else {
                return back()->with('error', 'Terjadi Kesalahan');
            }
        } catch (\Throwable $th) {
            throw $th;
            return back()->with('status', 'galat');
        }
    }
    public function rejectleave(Request $request)
    {
        try {
            $wl = WaitingList::find($request->reject_id);

            if ($wl) {

                $wl->delete();
                return back()->with('success', 'Berhasil Di Reject');
            } else {
                return back()->with('error', 'Terjadi Kesalahan');
            }
        } catch (\Throwable $th) {
            throw $th;
            return back()->with('status', 'galat');
        }
    }
    public function rejecttf(Request $request)
    {
        try {
            $wl = WaitingList::find($request->reject_id);

            if ($wl) {

                $wl->delete();
                return back()->with('success', 'Berhasil Di Reject');
            } else {
                return back()->with('error', 'Terjadi Kesalahan');
            }
        } catch (\Throwable $th) {
            throw $th;
            return back()->with('status', 'galat');
        }
    }
}

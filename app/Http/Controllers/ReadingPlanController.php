<?php

namespace App\Http\Controllers;

use App\Imports\GroupTodolistImport;
use App\Models\Group;
use App\Models\MemberTodolist;
use App\Models\ReadingPlan;
use App\Models\ReadingPlanList;
use App\Models\ReadingPlanType;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class ReadingPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ReadingPlan::all();
        // $data = ReadingPlan::whereHas("grouponly")->get();
        $type = ReadingPlanType::all();

        // dd($data[1]->group());
        return view("admin.plan", compact("data", "type"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'description' => 'required',
                'type' => 'required',
                'todo' => 'required',

            ]);
            if ($file = $request->file('todo')) {
                $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $name = 'gba-' . time() . substr(str_shuffle($permitted_chars), 0, 16) . '.' . $file->extension();
                $path = $file->move(public_path('files/'), $name);
            }

            $rp = ReadingPlan::create([
                'description' => $request->description,
                'file_name' => $name,
                'reading_plan_type_id' => $request->type,

            ]);

            // dd($rp);

            $data = $rp;
            if ($data) {
                Group::create([
                    'type' => 'individu',
                    'name' => "individu",
                    'reading_plan_id' => $rp->id,
                ]);

                Excel::import(new GroupTodolistImport($rp->id), public_path('files/' . $name));
                return back()->with('success', 'Data Berhasil di tambah');
            } else {
                return back()->with('error', 'Data Gagal Di Tambah');
            }
        } catch (\Throwable $th) {
            // throw $th;
            $file_path = public_path('files/') . $name;
            if (File::exists($file_path)) {
                unlink($file_path);
            }
            $data->delete();

            return back()->with('error', 'Terjaid kesalahan');
        }
        // catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
        //     //And if ExceptionTypeTwo happen
        //     return back()->with('error', 'Format tidak sesuai');
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ReadingPlan::find($id);
        return view("admin.plan_detail", compact("data"));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateall(Request $request)
    {
        try {
            if ($request->file()) {
                $request->validate([
                    'todo' => 'required',
                    'description' => 'required',

                ]);
                $file = $request->file('todo');

                $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $name = 'gba-' . time() . substr(str_shuffle($permitted_chars), 0, 16) . '.' . $file->extension();
                $path = $file->move(public_path('files/'), $name);

                $group = ReadingPlan::findOrFail($request->rp_id);

                $file_path = public_path('files/') . $group->file_name;
                if (File::exists($file_path)) {
                    unlink($file_path);
                }

                $group->update([
                    'file_name' => $name,
                    'description' => $request->description,
                ]);

                ReadingPlanList::where("reading_plan_id", $group->id)->delete();
                Excel::import(new GroupTodolistImport($request->rp_id), public_path('files/' . $name));
                $data = $group;
                if ($data) {
                    # code...
                    return back()->with('success', 'Data Berhasil di update');
                } else {
                    # code...
                    return back()->with('error', 'Data Gagal di update');
                }
            } else {
                $request->validate([
                    'description' => 'required',
                ]);
                $group = ReadingPlan::findOrFail($request->rp_id);
                $group->update([
                    'description' => $request->description,
                ]);
                $data = $group;
                if ($data) {
                    return back()->with('success', 'Data Berhasil di update');
                } else {
                    # code...
                    return back()->with('error', 'Data Gagal di update');
                }
            }
        } catch (Exception $err) {
            // $file_path = public_path('files/') . $name;
            // if (File::exists($file_path)) {
            //     unlink($file_path);
            // }
            // throw $err;
            return back()->with('error', "terjadi kesalahan");
        }
    }

    public function update(Request $request)
    {
        try {
            if ($request->file()) {
                $request->validate([
                    'todo' => 'required',
                    'description' => 'required',

                ]);
                $file = $request->file('todo');

                $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $name = 'gba-' . time() . substr(str_shuffle($permitted_chars), 0, 16) . '.' . $file->extension();
                $path = $file->move(public_path('files/'), $name);

                $group = ReadingPlan::findOrFail($request->rp_id);

                $file_path = public_path('files/') . $group->file_name;
                // dd($file_path);
                if (File::exists($file_path)) {
                    unlink($file_path);
                }

                $group->update([
                    'file_name' => $name,
                    'description' => $request->description,
                ]);




                ReadingPlanList::where("reading_plan_id", $group->id)->delete();
                $imp = Excel::import(new GroupTodolistImport($request->rp_id), public_path('files/' . $name));
                if ($imp) {
                    $rp = ReadingPlan::findOrFail($request->rp_id);
                    $rpcount = $rp->list->count();
                    // dd($rp->list[0])
                    $g = 1;
                    $m = 1;
                    foreach ($rp->group as $group) {
                        foreach ($group->member as $member) {
                            // dd($member->todo->count());
                            $tdcount = $member->todo->count();
                            // print_r($g . "-> " . $m . " todo" . $rpcount . "-" . $tdcount . "<br>");
                            if ($rpcount > $tdcount) {
                                // print("create rp >td <br>");
                                for ($i = 0; $i < $tdcount; $i++) {

                                    $member->todo[$i]->update([
                                        'chapter_verse' => $rp->list[$i]->chapter_verse,
                                        'reading_plan_list_id' => $rp->list[$i]->id,
                                    ]);
                                }
                                $strdate = $member->todo->min('schedule');
                                for ($p = 0; $p < $rpcount - $tdcount; $p++) {
                                    $scl = Carbon::parse($strdate);
                                    // echo  $scl . "->" . $scl->addDays($val->day) . "<br>";
                                    MemberTodolist::create([
                                        'id' => $member->id . date("dmyHi", strtotime(now())) . $p + 1,
                                        'group_member_id' => $member->id,
                                        'chapter_verse' => $rp->list[$i]->chapter_verse,
                                        'reading_plan_list_id' => $rp->list[$i]->id,
                                        'schedule' => $scl->addDays($rp->list[$i]->day - 1)
                                    ]);
                                    $i++;
                                }
                            } elseif ($rpcount < $tdcount) {
                                # code...
                                // print("delete rp < td  <br>");
                                for ($i = 0; $i < $rpcount; $i++) {
                                    // print("i=" . $i . " " . $member->todo[$i]->id . "--" . $rp->list[$i]->chapter_verse . "<br>");
                                    $member->todo[$i]->update([
                                        'chapter_verse' => $rp->list[$i]->chapter_verse,
                                        'reading_plan_list_id' => $rp->list[$i]->id,
                                    ]);
                                }
                                // print($i . "-----" . $tdcount - $rpcount);
                                for ($p = 0; $p < $tdcount - $rpcount; $p++) {
                                    $member->todo[$i]->delete();
                                    // print("p=" . $p . " " . $member->todo[$i]->id . "<br>");
                                    $i++;
                                }
                            } else {
                                // print("update done");
                                for ($i = 0; $i < $rpcount; $i++) {
                                    $member->todo[$i]->update([
                                        'chapter_verse' => $rp->list[$i]->chapter_verse,
                                        'reading_plan_list_id' => $rp->list[$i]->id,
                                    ]);
                                }
                            }

                            $m++;
                        }
                        $g++;
                    }
                }
                $data = $group;
                if ($data) {
                    # code...
                    return back()->with('success', 'Data Berhasil di update');
                } else {
                    # code...
                    return back()->with('error', 'Data Gagal di update');
                }
            } else {
                $request->validate([
                    'description' => 'required',
                ]);
                $group = ReadingPlan::findOrFail($request->rp_id);
                $group->update([
                    'description' => $request->description,
                ]);
                $data = $group;
                if ($data) {
                    return back()->with('success', 'Data Berhasil di update');
                } else {
                    # code...
                    return back()->with('error', 'Data Gagal di update');
                }
            }
        } catch (Exception $err) {
            // $file_path = public_path('files/') . $name;
            // if (File::exists($file_path)) {
            //     unlink($file_path);
            // }
            throw $err;
            return back()->with('error', "terjadi kesalahan");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;
        $data = ReadingPlan::find($id);
        $data->delete();
        if ($data) {
            return back()->with('success', 'Data Berhasil di Hapus');
        } else {
            # code...
            return back()->with('error', 'Data Gagal di Hapus');
        }
    }
}

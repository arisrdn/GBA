<?php

namespace App\Http\Controllers\Api;

use App\Helpers\APIFormatter;
use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\GroupChat;
use App\Models\GroupMember;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use PHPUnit\TextUI\XmlConfiguration\Groups;

class GroupChatController extends Controller
{
    //
    public function index2()
    {
        $id = auth('sanctum')->user()->id;
        // $data = Group::whereHas('user', function ($q1) use ($id) {
        //     $q1->where('user_id', $id);
        // })->where("end_date", ">", today())
        //     ->get(['id', 'name']);
        // $check = GroupMember::where('user_id', '=', $id)
        //     ->orderBy('created_at', 'DESC')->first();

        // foreach ($data as $i => $value) {
        //     $data[$i]->last_chat = GroupChat::where("group_id", $value->id)
        //         // ->where('from_id', auth()->user()->id)
        //         // ->orWhere(function ($query) use ($value) {
        //         //     $query->where('to_id', '=', auth()->user()->id);
        //         //     $query->where('from_id', '=', $value->id);
        //         // })
        //         ->orderBy('created_at', 'DESC')->first();
        //     $i++;
        // }

        $data = User::find($id);
        // $chats = Group::find($data->memberactive->group_id);
        // dd($chats->chats);

        if ($data) {
            // dd($data);
            // if ($data[0]->type == "group") {
            $group = Group::find($data->memberactive->group_id);
            $group->chats = $group->chats;
            $chat = GroupChat::where("group_id", $data->memberactive->group_id)
                ->get();
            return APIFormatter::responseAPI(200, 'Request Success', $group);
            // }
            // else {
            //     return APIFormatter::responseAPI(403, 'read option "individu" has no chat group',);
            // }
        } else {
            return APIFormatter::responseAPI(400, 'failed');
        }

        return APIFormatter::responseAPI(200, 'The request has succeeded', $data);
    }

    public function index()
    {
        $id = auth('sanctum')->user()->id;
        $data = Group::whereHas('user', function ($q1) use ($id) {
            $q1->where('user_id', $id);
        })
            ->get(['id', 'name']);
        // $check = GroupMember::where('user_id', '=', $id)
        //     ->orderBy('created_at', 'DESC')->first();

        // dd($data);
        foreach ($data as $i => $value) {
            $data[$i]->last_chat = GroupChat::where("group_id", $value->id)
                // ->where('from_id', auth()->user()->id)
                // ->orWhere(function ($query) use ($value) {
                //     $query->where('to_id', '=', auth()->user()->id);
                //     $query->where('from_id', '=', $value->id);
                // })
                ->orderBy('created_at', 'DESC')->first();
            $i++;
        }

        // $data = User::find($id);
        // $chats = Group::find($data->memberactive->group_id);
        // dd($data[0]);

        if ($data) {
            // dd($data);
            // if ($data[0]->type == "group") {
            //     $group = Group::find($data->memberactive->group_id);
            //     $group->chats = $group->chats;
            //     $chat = GroupChat::where("group_id", $data->memberactive->group_id)
            //         ->get();
            //     return APIFormatter::responseAPI(200, 'Request Success', $group);
            // } else {
            //     return APIFormatter::responseAPI(403, 'read option "individu" has no chat group',);
            // }sas

            return APIFormatter::responseAPI(200, 'Request Success', $data);
        } else {
            return APIFormatter::responseAPI(400, 'failed');
        }

        return APIFormatter::responseAPI(200, 'The request has succeeded', $data);
    }
    // show
    public function show($id)
    {
        $userid = auth('sanctum')->user()->id;
        // $g = Group::whereHas('user', function ($q1) use ($id) {
        //     $q1->where('user_id', $id);
        // })
        //     ->first();


        $data = GroupChat::where("group_id", $id)
            ->get();


        return APIFormatter::responseAPI(200, 'The request has succeeded', $data);
    }
    //store
    public function store(Request $request)
    {
        try {
            $id = auth('sanctum')->user()->id;
            $g = Group::whereHas('user', function ($q1) use ($id) {
                $q1->where('user_id', $id);
            })
                ->first();

            if ($g->type != "group") {
                return APIFormatter::responseAPI(403, 'read option "individu" has no chat group',);
            }
            $validator = Validator::make($request->all(), [
                'message' => 'required',
            ]);

            if ($validator->fails()) {
                return APIFormatter::responseAPI(422, 'failed', null, $validator->errors());
            }

            $check = GroupMember::where('user_id', '=', $id)->where('approved_at', '!=', null)->orderBy('created_at', 'DESC')->first();

            if (!$check) {
                return APIFormatter::responseAPI(403, 'Access to that resource is forbidden', null,);
            }

            $data = GroupChat::create([
                'message' => $request->message,
                'group_id' => $g->id,
                'from_id' => $id
            ]);
            // $check = GroupMember::where('user_id', '=', $id)->where('approved_at', '!=', null)->orderBy('created_at', 'DESC')->first();
            // $data = GroupChat::create([
            //     'message' => $request->message,
            //     'group_id' => $request->group_id,
            //     'from_id' => $id
            // ]);

            if ($data) {
                # code...
                return APIFormatter::responseAPI(201, 'Success Created', $data);
            } else {
                # code...
                return APIFormatter::responseAPI(400, 'failed');
            }
        } catch (Exception $err) {
            // throw $err;
            return APIFormatter::responseAPI(400, 'failed', null, $err->getMessage());
        }
    }
}

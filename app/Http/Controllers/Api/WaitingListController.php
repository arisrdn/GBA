<?php

namespace App\Http\Controllers\Api;

use App\Helpers\APIFormatter;
use App\Helpers\Notify;
use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\GroupMember;
use App\Models\GroupPlan;
use App\Models\History;
use App\Models\MemberTodolist;
use App\Models\ReadingPlan;
use App\Models\ReadingPlanList;
use App\Models\ReasonLeave;
use App\Models\WaitingList;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class WaitingListController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 
     */
    public function join(Request $request)
    {

        try {
            $user_id = auth('sanctum')->user()->id;
            $validator = Validator::make($request->all(), [
                'plan' => 'required',
                'read_option' => 'required',
            ]);
            if ($validator->fails()) {
                return APIFormatter::responseAPI(422, 'failed', null, $validator->errors());
            }
            if (!ReadingPlan::find($request->plan)) {
                # code...
                return APIFormatter::responseAPI(422, 'failed', null, "Plan Not Exist");
            }

            $dt = [
                'read_option' => $request->read_option,
            ];
            if ($request->read_option == "individu" || $request->read_option == "i") {

                $group = Group::where("type", "individu")->where("reading_plan_id", $request->plan)->first();
                $todo = ReadingPlanList::where("reading_plan_id", $request->plan)->get();
                $strdate = today();

                $data = GroupMember::create([
                    'user_id' => $user_id,
                    'group_id' => $group->id,
                    'approved_at' => Carbon::now()
                ]);


                foreach ($todo as $n => $val) {
                    $scl = Carbon::parse($strdate);
                    MemberTodolist::create([
                        // 'id' => $data->id . date("dmY", strtotime(today())) . $n + 1,
                        'id' => $data->id . date("dmyHis", strtotime(today())) . $n + 1,
                        'group_member_id' => $data->id,
                        'reading_plan_list_id' => $val->id,
                        'chapter_verse' => $val->chapter_verse,
                        'schedule' => $scl->addDays($val->day - 1)
                    ]);
                }

                Notify::approvejoingroup($user_id);
            } else {
                $data = WaitingList::create([
                    'user_id' => $user_id,
                    'type' => "join",
                    'data' => json_encode($dt),
                    'reading_plan_id' => $request->plan,
                ]);
                Notify::joingroup($user_id);
            }
            if ($data) {
                return APIFormatter::responseAPI(200, 'Request success', $dt);
            } else {
                return APIFormatter::responseAPI(400, 'failed');
            }
        } catch (Exception $err) {
            throw $err;
            return APIFormatter::responseAPI(400, 'failed', null, $err->getMessage());
        }
    }
    public function leave(Request $request)
    {
        try {
            $user_id = auth('sanctum')->user()->id;
            $validator = Validator::make($request->all(), [
                // 'user_id' => 'required',
                'reason_leave' => 'required',
                'note' => 'required',
            ]);
            if ($validator->fails()) {
                return APIFormatter::responseAPI(422, 'failed', null, $validator->errors());
            }

            $data  = GroupMember::where("user_id", $user_id)->first();
            $type = $data->group->type;

            if ($type == "individu" || $type == "i") {
                // dd($type);
                $planid = null;
                $plandes = null;
                if (!isset($data->group->plan)) {
                    # code...
                    $planid = $data->group->plan_with_trashed->id;
                    $plandes = $data->group->plan_with_trashed->description;
                } else {
                    $planid = $data->group->plan->id;
                    $plandes = $data->group->plan->description;
                }
                $hs = History::create([
                    'user_id' => $user_id,
                    'group_id' => $data->group_id,
                    'group_name' => $data->group->name,
                    'priode_start' => $data->group->start_date,
                    'priode_end' => $data->group->end_date,
                    'reading_plan_id' => $planid,
                    'reading_plan_description' => $plandes,
                    'reading_plan_count' => $data->todo->count(),
                    'readed' => $data->todo->where("read_at", "!=", null)->count(),
                    'strike' => $data->strike,
                    'status' => "leave"
                ]);
                // dd("ss");

                if ($hs) {
                    $data->delete();
                    return APIFormatter::responseAPI(200, 'Request success');
                } else {
                    return APIFormatter::responseAPI(400, 'failed');
                }
            } else {
                // dd("abc");
                $dt = array('note' => $request->note);

                $data = WaitingList::create([
                    'user_id' => $user_id,
                    'type' => "leave",
                    'data' => json_encode($dt),
                    'reason_leave_id' => $request->reason_leave,
                ]);
                if ($data) {

                    return APIFormatter::responseAPI(200, 'Request success', $dt);
                } else {
                    return APIFormatter::responseAPI(400, 'failed');
                }
            }
        } catch (Exception $err) {
            // throw $err;
            return APIFormatter::responseAPI(400, 'failed', null, $err->getMessage());
        }
    }
    public function transfer(Request $request)
    {
        try {
            $user_id = auth('sanctum')->user()->id;
            $validator = Validator::make($request->all(), [
                // 'user_id' => 'required',
                'transfer' => 'required',

            ]);
            if ($validator->fails()) {
                return APIFormatter::responseAPI(422, 'failed', null, $validator->errors());
            }

            $dt = [
                'transfer' => $request->transfer,

            ];
            $data = WaitingList::create([
                'user_id' => $user_id,
                'type' => "transfer",
                'data' => json_encode($dt),
            ]);
            if ($data) {
                return APIFormatter::responseAPI(200, 'Request success', $dt);
            } else {
                return APIFormatter::responseAPI(400, 'failed');
            }
        } catch (Exception $err) {
            // throw $err;
            return APIFormatter::responseAPI(400, 'failed', null, $err->getMessage());
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reason()
    {
        $data = ReasonLeave::all();


        return APIFormatter::responseAPI(200, 'The request has succeeded', $data);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Helpers\APIFormatter;
use App\Helpers\Setting;
use App\Http\Controllers\Controller;
use App\Models\GroupMember;
use App\Models\GroupTodolist;
use App\Models\MemberTodolist;
use App\Models\ReadingPlanList;
use App\Models\WaitingList;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Laravel\Sanctum\PersonalAccessToken;

class TodoListController extends Controller
{
    //
    public function index()
    {
        $id = auth('sanctum')->user()->id;

        $check = GroupMember::where('user_id', '=', $id)->orderBy('id', 'DESC')->first();
        $data = MemberTodolist::with('todolist')->where('group_member_id', $check->id)->get();
        // $todo = GroupTodolist::where("group_id", $check->group->id)->get();
        // for ($i = 0; $i < count($todo); $i++) {
        //     $todo[$i]->readed;
        //     if (count($check->todo) > $i) {
        //         $todo[$i]->read_at = $check->todo[$i]->read_at;
        //     } else {
        //         $todo[$i]->read_at = null;
        //     }
        // }

        // dd($check->group->member)?

        if ($data) {
            return APIFormatter::responseAPI(200, 'The request has succeeded', $data);
        } else {
            return APIFormatter::responseAPI(400, 'failed');
        }
    }


    public function summary()
    {
        $id = auth('sanctum')->user()->id;

        $check = GroupMember::where('user_id', '=', $id)->orderBy('created_at', 'DESC')->first();
        $todos = $check->todo;
        // $todo = MemberTodolist::where('group_member_id', $check->id)->get();
        $readed = MemberTodolist::where('group_member_id', $check->id)->where("read_at", "!=", null)->get();

        $unread = count($todos) - count($readed);
        $percentage = round(count($readed) / count($todos) * 100);

        $strike = 0;
        $lastread = MemberTodolist::where('group_member_id', $check->id)->where("read_at", "!=", null)->orderBy('id', 'DESC')->first();

        if (isset($lastread)) {
            # code...
            for ($i = 0; $i < $readed->count(); $i++) {
                // for ($i = 0; $i < 5; $i++) {
                if ($i == 0) {
                    $strike = 1;
                } else {
                    if (date('d-m-Y', strtotime($readed[$i]->read_at)) == date('d-m-Y', strtotime($readed[$i - 1]->read_at->addday(1))) || date('d-m-Y', strtotime($readed[$i]->read_at)) == date('d-m-Y', strtotime($readed[$i - 1]->read_at))) {
                        // echo $da->todo[$i]->id . 'tambah^';
                        $strike = $strike + 1;
                    } else {
                        $strike = 1;
                    }
                }
            }
        }

        // dd($lastread);
        $data = array("todolist" => count($todos), "readed" => count($readed), "unread" => $unread, "percentage" => $percentage, "strike" => $strike, "strike_week" => ceil($strike / 7));
        if ($data) {
            return APIFormatter::responseAPI(200, 'The request has succeeded', $data);
        } else {
            return APIFormatter::responseAPI(400, 'failed');
        }
    }
    public function today()
    {
        $id = auth('sanctum')->user()->id;
        // dd(auth('sanctum')->user()->group);
        $gm = auth('sanctum')->user()->groupmember;
        // $gt = GroupTodolist::where("group_id", $gm->group_id)->get();
        $gt = $gm->todo;
        $coun_gt = count($gt);
        $data = MemberTodolist::where('group_member_id', $gm->id)->where("schedule", today())->with('todolist')->first();
        $exp = MemberTodolist::where('group_member_id', $gm->id)->orderBy('id', 'DESC')->first();
        // dd($exp);
        // dd($data);
        if ($data) {
            // $count = count($data);
            // $count = 129;
            $res = $data;
            $res["lasread"] = false;
            $res["group_expire"] = false;
            if ($data->id == $exp->id) {
                $res["lasread"] = true;
            }
            if (today() >= $gm->group->end_date) {
                $res["group_expire"] = true;
            }


            return APIFormatter::responseAPI(200, 'The request has succeeded', $res);
        } else {

            $res = $data;

            if (today() < $gm->group->start_date) {
                $res["message"] = "The group hasn't started yet";
                return APIFormatter::responseAPI(200, 'The request has succeeded', $res);
            } else {
                $res["message"] = 'todo complete';
                return APIFormatter::responseAPI(200, 'The request has succeeded', $res);
            }
        }


        // return APIFormatter::responseAPI(200, 'The request has succeeded', $gt);

        // if ($data) {
        //     return APIFormatter::responseAPI(200, 'The request has succeeded', $data);
        // } else {
        //     return APIFormatter::responseAPI(400, 'failed');
        // }
    }
    public function history()
    {
        $id = auth('sanctum')->user()->id;
        // dd(today()->subDay(1));

        $check = GroupMember::where('user_id', '=', $id)->orderBy('created_at', 'DESC')->first();
        $data = MemberTodolist::with('todolist')->where('group_member_id', $check->id)->where('schedule', '<', today())->limit(10)->orderBy('id', 'DESC')->get();
        // $data->now = now();
        // dd($data);

        if ($data) {
            return APIFormatter::responseAPI(200, 'The request has succeeded ' . now(), $data);
        } else {
            return APIFormatter::responseAPI(400, 'failed');
        }
    }
    public function store(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'todo_id' => 'required',

            ]);

            if ($validator->fails()) {

                return APIFormatter::responseAPI(422, 'failed', null, $validator->errors());
            }
            $todo_id = $request->todo_id;
            $id = auth('sanctum')->user()->id;
            $gm = GroupMember::where('user_id', '=', $id)->orderBy('id', 'DESC')->first();
            $read = now();
            $endtime = Carbon::parse(date("Y-m-d") . " " . Setting::ENDTIME);
            $lastread = $gm->todo->where('read_at', '!=', null)->sortByDesc('id')->first();
            // dd($lastread);
            // dd($read)
            if ($read < $endtime) {
                # code...
                $read = Carbon::parse(date("Y-m-d") . " " . Setting::STARTTIME)->addDay(1);
            }
            $check = MemberTodolist::where("id", $todo_id)->where("group_member_id", $gm->id)->first();

            if ($check == null) {
                return APIFormatter::responseAPI(410, 'access denied.');
            }
            if ($check->read_at != null) {
                return APIFormatter::responseAPI(200, 'already submitted');
            }


            $strike =  $gm->strike;
            if (isset($lastread->read_at)) {
                if ($lastread->read_at == today()) {
                    # code...
                } elseif ($lastread->read_at->addDay(1) == today()) {
                    $strike =  $strike + 1;
                } else {
                    $strike = 1;
                }
            } else {
                $strike = 1;
            }
            $check->read_at = $read;
            $check->save();

            $gm->strike = $strike;
            $gm->save();

            if ($request->transfer) {
                // dd()
                $data = WaitingList::create([
                    'user_id' => $id,
                    'type' => "transfer",
                    'data' => $request->transfer
                ]);
            }
            $mt = MemberTodolist::where('group_member_id', $gm->id)->get();
            $gt = MemberTodolist::where('group_member_id', $gm->id)->where("read_at", "!=", null)->get();

            if (count($mt) == count($gt)) {
                # code...
                $gm->complete_at = now();
                $gm->save();
            }
            $data = $check;
            if ($data) {
                # code...
                return APIFormatter::responseAPI(200, 'post read success', $data);
            } else {
                # code...
                return APIFormatter::responseAPI(400, 'failed');
            }
        } catch (Exception $err) {
            throw $err;
            // dd($err);
            return APIFormatter::responseAPI(400, 'failed', null, $err->getMessage());
        }
    }
    public function stores(Request $request)
    {
        try {
            // dd($request->todos);

            $validator = Validator::make($request->all(), [
                'todos' => 'required',

            ]);

            if ($validator->fails()) {

                return APIFormatter::responseAPI(422, 'failed', null, $validator->errors());
            }
            $id = auth('sanctum')->user()->id;
            $gm = GroupMember::where('user_id', '=', $id)->orderBy('id', 'DESC')->first();
            $read = now();
            $endtime = Carbon::parse(date("Y-m-d") . " " . Setting::ENDTIME);
            $lastread = $gm->todo->where('read_at', '!=', null)->sortByDesc('id')->first();
            // dd($lastread);
            // dd($read)
            if ($read < $endtime) {
                # code...
                $read = Carbon::parse(date("Y-m-d") . " " . Setting::STARTTIME)->addDay(1);
            }

            $todos = $request->todos;


            foreach ($todos as $value) {
                # code...
                $todo_id = $value["todo_id"];
                $readed = $value["readed"];
                // $check = MemberTodolist::where("id", $todo_id)->where("group_member_id", $gm->id)->first();
                $check = MemberTodolist::where("id", $todo_id)->first();
                // dd($check);
                if ($readed) {
                    $check->read_at = $read;
                } else {
                    $check->read_at = null;
                }

                $check->save();
            }


            $strike =  $gm->strike;
            if (isset($lastread->read_at)) {
                if ($lastread->read_at == today()) {
                    # code...
                } elseif ($lastread->read_at->addDay(1) == today()) {
                    $strike =  $strike + 1;
                } else {
                    $strike = 1;
                }
            } else {
                $strike = 1;
            }

            $gm->strike = $strike;
            $gm->save();

            if ($request->transfer) {
                // dd()
                $data = WaitingList::create([
                    'user_id' => $id,
                    'type' => "transfer",
                    'data' => $request->transfer
                ]);
            }
            $mt = MemberTodolist::where('group_member_id', $gm->id)->get();
            $gt = MemberTodolist::where('group_member_id', $gm->id)->where("read_at", "!=", null)->get();

            if (count($mt) == count($gt)) {
                # code...
                $gm->complete_at = now();
                $gm->save();
            }
            $data = $check;
            if ($data) {
                # code...
                return APIFormatter::responseAPI(200, 'post read success', $data);
            } else {
                # code...
                return APIFormatter::responseAPI(400, 'failed');
            }
        } catch (Exception $err) {
            throw $err;
            // dd($err);
            return APIFormatter::responseAPI(400, 'failed', null, $err->getMessage());
        }
    }
}

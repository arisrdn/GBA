<?php

namespace App\Http\Controllers\Api;

use App\Helpers\APIFormatter;
use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\GroupPlan;
use App\Models\ReadingPlan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReadingPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function group()
    {
        //
        // $data = ReadingPlan::whereHas("list")->get();
        $data = ReadingPlan::whereHas("grouponly")->get();
        // dd($data);
        foreach ($data as $key => $value) {
            $data[$key]->description = $value->type->name . " - " . $value->description;

            # code...
        }
        // dd($data);
        if ($data) {
            return APIFormatter::responseAPI(200, 'The request has succeeded', $data);
        } else {
            return APIFormatter::responseAPI(400, 'failed');
        }
    }
    public function individu()
    {
        //
        $data = ReadingPlan::whereHas("list")->get();
        // $data = ReadingPla   n::whereHas("grouponly")->get();
        // dd($data);
        foreach ($data as $key => $value) {
            $data[$key]->description = $value->type->name . " - " . $value->description;

            # code...
        }
        // dd($data);
        if ($data) {
            return APIFormatter::responseAPI(200, 'The request has succeeded', $data);
        } else {
            return APIFormatter::responseAPI(400, 'failed');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'read_option' => 'required',
        ]);
        if ($validator->fails()) {
            return APIFormatter::responseAPI(422, 'Validation Failed ', null, $validator->errors());
        }

        if ($request->read_option == "group") {
            $data = ReadingPlan::whereHas("grouponly")->get();
        } else {
            $data = ReadingPlan::whereHas("list")->get();
        }


        foreach ($data as $key => $value) {
            $data[$key]->description = $value->type->name . " - " . $value->description;
        }

        if ($data) {
            return APIFormatter::responseAPI(200, 'The request has succeeded', $data);
        } else {
            return APIFormatter::responseAPI(400, 'failed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = Group::where("group_plan_id", $id)->get();
        if ($data) {
            return APIFormatter::responseAPI(200, 'The request has succeeded', $data);
        } else {
            return APIFormatter::responseAPI(400, 'failed');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

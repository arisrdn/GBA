<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{

    public function index()
    {
        return view("admin.setting.index");
    }


    public function group()
    {
        $setting = Setting::first();
        return view("admin.setting.group", compact('setting'));
    }
}

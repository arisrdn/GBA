<?php

namespace App\Http\Controllers;

use App\Helpers\APIFormatter;
use App\Models\CountryCode;
use App\Models\Group;
use App\Models\Province;
use App\Models\Role;
use App\Models\User;
use App\Notifications\UserCreated;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
        // $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index', 'store']]);
        // $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:menu-admin/pic', ['only' => ['create', 'index']]);
    }


    public function index()
    {

        $data = User::where("role_id", "!=", "2")->get();
        $role = Role::where("name", "!=", "user")->get();

        return view("admin.user", compact("data", "role"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $country = CountryCode::all();
        $province = Province::all();
        $role = Role::where("name", "!=", "user")->get();
        return view("admin.user-add", compact("province", "country", "role"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            // 'password' => 'required|string|min:8|confirmed',
            'whatsapp_no' => 'required',
            'gender' => 'required',
            // 'address' => 'required',
            'birth_date' => 'required',
            // 'regency' => 'required',
            'role' => 'required',
            'province' => 'required',
            'city' => 'required',


        ]);
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        ////upload to public 
        if ($file = $request->file('photo_profile')) {
            $name = 'photo-' . time() . substr(str_shuffle($permitted_chars), 0, 16) . '.' . $file->extension();
            $path = $file->move(public_path('images/users/'), $name);
            $path = "/images/users/" . $name;
        }
        // dd($path);
        $password =  substr(str_shuffle($permitted_chars), 0, 8);
        $number = $request->whatsapp_no;

        if ($number[0] == '0') {
            $number = substr($number, 1);
        }
        $data = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($password),
            'whatsapp_no' =>  $number,
            'gender' => $request->gender,
            // 'address' => $request->address,
            'province' => $request->address,
            'city' => $request->province,
            'birth_date' => $request->city,
            'country_id' => $request->phone_code,
            'church_branch_id' => $request->church_branch_id,
            // 'regency_id' => $request->regency,
            'role_id' => $request->role,
            'email_verified_at' => today(),

        ]);
        $data->notify(new UserCreated($data->email, $password));

        if ($data) {
            return back()->with('success', 'Data Berhasil Di Tambah');
        } else {
            return back()->with('error', 'Data Gagal Di Tambah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = User::with('church_branch', 'country')->find($id);
        $data['photo_path'] =  asset('images/users/') . $data->photo_profile;
        return APIFormatter::responseAPI(200, 'The request has succeeded', $data);
    }

    public function showg($id)
    {
        $data = Group::find($id);
        $data->photo_profile =  null;
        return APIFormatter::responseAPI(200, 'The request has succeeded', $data);
    }


    /**

     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request);
        $request->validate([
            'role' => 'required',
        ]);
        $user = User::find($request->id);
        $user->role_id = $request->role;
        $user->save();
        DB::table('model_has_roles')->where('model_id', $request->id)->delete();
        $user->assignRole($request->role);

        return back()->with('success', 'User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function test($id)
    {
        //
        $data = User::whereHas('group', function ($q1) use ($id) {
            $q1->where('group_id', $id);
        })
            ->pluck('device_token')->toArray();
        $data2 = User::whereHas('adminGroup', function ($q1) use ($id) {
            $q1->where('group_id', $id);
        })
            ->pluck('device_token')->toArray();
        $tokens = array_merge($data, $data2);

        return APIFormatter::responseAPI(200, 'The request has succeeded', $tokens);
    }

    public function auth()
    {

        $data = auth()->user();
        // $data->photo_profile =  null;
        return APIFormatter::responseAPI(200, 'The request has succeeded', $data);
    }
}

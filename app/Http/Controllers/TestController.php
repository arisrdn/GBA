<?php

namespace App\Http\Controllers;

use App\Helpers\APIFormatter;
use App\Models\Group;
use App\Models\ReadingPlan;
use App\Models\ReadingPlanType;
use Exception;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index()
    {
        $data = ReadingPlanType::all();
        if ($data) {
            return APIFormatter::responseAPI(200, 'The request has succeeded', $data);
        } else {
            return APIFormatter::responseAPI(400, 'failed');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {

            $data = ReadingPlanType::findOrFail($id)->plan;
            // dd($data);
            if ($data) {
                # code...
                return APIFormatter::responseAPI(200, 'success', $data);
            } else {
                # code...
                return APIFormatter::responseAPI(400, 'failed');
            }
        } catch (Exception $err) {
            //throw $th;
            // dd($err);
            return APIFormatter::responseAPI(400, 'failed', null, $err->getMessage());
        }
    }
    public function naming($id)
    {
        try {

            $data = ReadingPlan::findOrFail($id);
            // dd($data);


            if ($data) {
                $rp = ReadingPlan::where("id", $data->id)->with("type")->withCount("list")->first();
                $countgroup = Group::where("type", "group")->whereHas('plan', function ($q1) use ($data) {
                    $q1->where('reading_plans.reading_plan_type_id', $data->reading_plan_type_id);
                })->count();
                // dd($countgroup);
                $data->type = $rp->type->short;
                $data->add_days = $rp->list_count;
                $data->countgroup = $countgroup + 1;
                # code...
                return APIFormatter::responseAPI(200, 'success', $data);
            } else {
                # code...
                return APIFormatter::responseAPI(400, 'failed');
            }
        } catch (Exception $err) {
            //throw $th;
            // dd($err);
            return APIFormatter::responseAPI(400, 'failed', null, $err->getMessage());
        }
    }
}

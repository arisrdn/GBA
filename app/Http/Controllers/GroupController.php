<?php

namespace App\Http\Controllers;

use App\Helpers\APIFormatter;
use App\Imports\GroupTodolistImport;
use App\Models\Group;
use App\Models\GroupAdmin;
use App\Models\GroupEod;
use App\Models\GroupMember;
use App\Models\GroupPlan;
use App\Models\GroupTodolist;
use App\Models\History;
use App\Models\MemberTodolist;
use App\Models\ReadingPlan;
use App\Models\ReadingPlanList;
use App\Models\ReadingPlanType;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $a = auth()->user()->adminGroup;
        // // dd($a[0]->group->plan);
        $data = Group::withCount('user')->where("type", "group")->with('plan')
            ->orderBy('end_date', 'DESC')
            ->orderBy('name', 'ASC')
            // ->orderBy('names', 'ASC')
            ->get();
        return view("admin.group", compact("data"));
    }

    public function index2()
    {
        $data = Group::withCount('user')->where("type", "individu")->get();

        // dd($data[4]->plan_with_trashed);
        return view("admin.group2", compact("data"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $plan = ReadingPlan::all();
        $user = User::where("role_id", "3")->get();
        $type = ReadingPlanType::all();
        return view("admin.group-add", compact("plan", "user", "type"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());
        try {

            $request->validate([
                'name' => 'required',
                'plan_id' => 'required',
                'pic' => 'required',
                'copic' => 'required',

            ]);

            $group = Group::create([
                'name' => $request->name,
                'reading_plan_id' => $request->plan_id,
                'start_date' => $request->start,
                'end_date' => $request->end
            ]);
            // dd($group->id);
            $datagp = Group::find($group->id);
            if ($group) {

                $Groupchat = GroupAdmin::create([
                    'group_id' => $datagp->id,
                    'user_id' =>  $request->pic,
                    'type' => "0"

                ]);
                $Groupchat = GroupAdmin::create([
                    'group_id' => $datagp->id,
                    'user_id' =>  $request->copic,
                    'type' => "1"

                ]);
            }


            $data = $group;
            if ($data) {
                # code...
                return redirect(route("group"))->with('success', 'Data Berhasil Di Tambah');
            } else {
                # code...
                return back()->with('error', 'Data Gagal Di Tambah');
            }
        } catch (Exception $err) {

            throw $err;
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Group::findOrfail($id);
        $plan = ReadingPlan::all();
        if (auth()->user()->role_id == 1) {
            $group = Group::where("type", "group")->where("reading_plan_id", $data->reading_plan_id)->doesntHave("member")->get();

            # code...
        } else {
            $group = Group::where("type", "group")->where("reading_plan_id", $data->reading_plan_id)->doesntHave("member")
                ->whereHas('admin', function ($q1) {
                    $q1->where('group_admins.user_id', auth()->user()->id);
                    // $ql->where("user_iid", 2);
                })->get();
            // dd($group);
        }

        // $group = Group::where("type", "group")->where("start_date", "<", now())->get();
        // dd($group);
        $user = User::where("role_id", "3")->get();

        return view("admin.group-detail", compact("data", "plan", "group", "user"));
    }
    public function show2($id)
    {
        $data = Group::findOrfail($id);
        $plan = ReadingPlan::all();
        $admin = User::where("role_id", 1)->get();

        return view("admin.group-detail2", compact("data", "plan", "admin"));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        // dd($req);
        $id = $req->id;
        $data = Group::find($id);
        $data->delete();
        if ($data) {
            return back()->with('success', 'Data Berhasil di Hapus');
        } else {
            # code...
            return back()->with('error', 'Data Gagal di Hapus');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatetodo(Request $request)
    {
        // dd($request);

        try {
            // if (isset($request->type)) {
            //     # code...
            //     $request->validate([
            //         'todo' => 'required',

            //     ]);
            //     $file = $request->file('todo');

            //     $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            //     $name = 'gba-' . time() . substr(str_shuffle($permitted_chars), 0, 16) . '.' . $file->extension();
            //     $path = $file->move(public_path('files/'), $name);

            //     $group = Group::findOrFail($request->group_id);
            //     $group->update([

            //         'todo_file' => $name
            //     ]);


            //     Excel::import(new GroupTodolistImport($request->group_id), public_path('files/' . $name));





            //     $data = $group;
            //     if ($data) {
            //         # code...
            //         return back()->with('success', 'Data Berhasil di tambah');
            //     } else {
            //         # code...
            //         return back()->with('error', 'Data Gagal di tambah');
            //     }
            // } else {
            $request->validate([
                'name' => 'required',
                'plan_id' => 'required',
                'start' => 'required',
                'end' => 'required',

            ]);

            $group = Group::findOrFail($request->group_id);
            $group->update([
                'name' => $request->name,
                'reading_plan_id' => $request->plan_id,
                'start_date' => $request->start,
                'end_date' => $request->end
            ]);




            $data = $group;
            if ($data) {
                # code...
                return back()->with('success', 'Data Berhasil Ubah');
            } else {
                # code...
                return back()->with('error', 'Data Gagal di Ubah');
            }
            // }


            // dd($request);

        } catch (Exception $err) {
            // $file_path = public_path('files/') . $name;
            // if (File::exists($file_path)) {
            //     unlink($file_path);
            // }
            throw $err;
            return back()->with('error', "terjadi kesalahan");
        }
    }


    public function updateadmin(Request $request)
    {

        // dd($request);
        $request->validate([
            'id' => 'required',
            'pic' => 'required',
            'copic' => 'required',

        ]);
        $delete = GroupAdmin::where("group_id", $request->id)->delete();

        $Groupchat = GroupAdmin::create([
            'group_id' => $request->id,
            'user_id' =>  $request->pic,
            'type' => "0"

        ]);
        $Groupchat = GroupAdmin::create([
            'group_id' => $request->id,
            'user_id' =>  $request->copic,
            'type' => "1"

        ]);
        if ($delete) {
            # code...
            return back()->with('success', 'Data Berhasil Ubah');
        } else {
            # code...
            return back()->with('error', 'Data Gagal di Ubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function transfer(Request $request)
    {
        date_default_timezone_set("Asia/Jakarta");
        $group = Group::find($request->group_id);
        $todo = ReadingPlanList::where("reading_plan_id", $group->reading_plan_id)->get();
        // $togroup = $member->map(function (GroupMember $group) use ($request) {
        //     return [
        //         'user_id' => $group->user_id,
        //         // 'user_id2' => $group->group_id,
        //         'group_id' => $request->group_to,
        //         'approved_at' => now()

        //     ];
        // });

        foreach ($group->member as $wl) {
            # code...
            $data = GroupMember::create([
                'user_id' => $wl->user_id,
                'group_id' => $request->group_to,
                'approved_at' => now()
            ]);
            foreach ($todo as $n => $val) {
                MemberTodolist::create([
                    'id' => $data->id . date("dmyHis", strtotime(today())) . $n + 1,
                    'group_member_id' => $data->id,
                    'reading_plan_list_id' => $val->id,
                    'chapter_verse' => $val->chapter_verse,
                    // 'schedule' => today()->addDays($val->day)
                    'schedule' => $data->group->start_date->addDays($val->day - 1)

                ]);
            }
            $hs = History::create([
                'user_id' => $wl->user_id,
                'group_id' => $wl->group_id,
                'group_name' => $wl->group->name,
                'priode_start' => $wl->group->start_date,
                'priode_end' => $wl->group->end_date,
                'reading_plan_id' => $wl->group->plan->id,
                'reading_plan_description' => $wl->group->plan->description,
                'reading_plan_count' => $wl->todo->count(),
                'readed' => $wl->todo->where("read_at", "!=", null)->count(),
                'strike' => $wl->strike,
                'status' => "transfer"
            ]);

            $delete = GroupMember::destroy($wl->id);
        }
        // $delete = GroupMember::where("group_id", $request->group_to)->delete();

        dd(date("dmyHis", strtotime(now())));
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Chat;
use App\Models\Group;
use App\Models\GroupMember;
use App\Models\GroupTodolist;
use App\Models\MemberTodolist;
use App\Models\User;
use App\Notifications\ApiEmailVerified;
use App\Notifications\UserNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Helpers\Message;
use App\Models\Church;
use App\Models\ChurchBranch;
use App\Models\ReadingPlan;
use App\Models\Regency;
use App\Models\Role;
use App\Models\WaitingList;
use App\Notifications\User\GlobalNotification;
use DateTime;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{

    public function index()
    {

        if (auth()->user()->role_id == 1) {
            $member = GroupMember::whereHas("group", function ($query) {
                $query->where('type', 'group');
                $query->where('end_date', ">=", today());
            })->with("latestRead")->get()->sortBy("latestRead.read_at");


            foreach ($member as $i => $value) {
                $member[$i]->lastRead =  MemberTodolist::where('group_member_id', $value->id)
                    // ->where("read_at", "!=", null)
                    ->orderBy('read_at', 'DESC')
                    // ->get();
                    ->first();
                $tgl1 = new DateTime($member[$i]->group->star_date);
                if (isset($member[$i]->lastRead->read_at)) {
                    $tgl1 = new DateTime($member[$i]->lastRead->read_at);
                }

                // dd($member[$i]->lastRead);
                $tgl2 =  today();
                $member[$i]->tgl1 = $tgl1;
                $member[$i]->tgl2 = $tgl2;
                $member[$i]->overdue = $tgl2->diff($tgl1);
                $i++;
            }
        } else {
            $member = GroupMember::whereHas("group", function ($query) {
                $query->where('type', 'group');
                $query->where('end_date', ">=", today());
                $query->whereIn('id', auth()->user()->adminGroup->pluck("group_id")->toArray());
            })->with("latestRead")->get()->sortBy("latestRead.read_at");
            $i = 0;
            foreach ($member as $value) {
                $member[$i]->lastRead =  MemberTodolist::where('group_member_id', $value->id)
                    ->where("read_at", "!=", null)
                    ->orderBy('id', 'DESC')
                    ->first();
                $tgl1 = new DateTime($member[$i]->group->star_date);
                if (isset($member[$i]->lastRead->read_at)) {
                    $tgl1 = new DateTime($member[$i]->lastRead->read_at);
                }
                $tgl2 = today();
                $member[$i]->overdue = $tgl2->diff($tgl1);
                $i++;
            }
        }
        // dd($member);
        // $arrayName = [1, 2, 3];
        $test = MemberTodolist::where('group_member_id', 6)
            ->where("read_at", "!=", null)
            ->orderBy('read_at', 'DESC')
            // ->get();
            ->first();
        // print_r($member[0]->tgl1);
        // dd($test->read_at);

        $strike = GroupMember::whereHas("group", function ($query) {
            $query->where('type', 'group');
            $query->where('end_date', ">=", today());
        })->with("latestRead")->get();
        // // dd(now()->subDays(rand(1, 5)));
        // $datagroup = Group::withCount("member")->orderBy('member_count', 'desc')
        //     ->get();

        $user = Role::withCount("user")->get();
        // user bygender
        $gender = [];
        $gender['male'] = User::where([['gender', 'male'], ['role_id', 2]])->count();
        $gender['female'] = User::where([['gender', 'female'], ['role_id', 2]])->count();



        $group["inactive"] = Group::where('end_date', '<=', today())->where("type", "group")
            ->count();
        $group["active"] = Group::where('end_date', '>=', today())->where("type", "group")
            ->count();

        $gereja = Church::withCount('user')->withCount('branch')->get();

        $plan = ReadingPlan::withCount('member')->get();

        // composition
        $church_ck7 = Church::where('name', 'like', '%CK7%')->withCount('userFemale')->withCount('userMale')->withCount('user')->get();
        $church_other = Church::where('name', 'not like', '%CK7%')->withCount('userFemale')->withCount('userMale')->withCount('user')->get();
        $composition = [];
        $composition[0] = $church_ck7;
        $composition[1] = $church_other;

        // Branch
        $branch = ChurchBranch::whereHas('church', function ($q1) {
            $q1->where('churches.name', 'like', '%CK7%');
        })
            ->withCount('user')
            ->get();
        $i = 0;
        foreach ($branch as $value) {
            // $sum = 1;
            $sum1 = 0;
            $sum2 = 0;
            foreach ($value->useractive as $val) {
                if ($val->updated_at->addday(30) <= today()) {
                    $sum1++;
                    // $sum++;
                } else {
                    // $sum++;
                    $sum2++;
                }
            }
            // $branch[$i]->user = $sum;
            $branch[$i]->userinactive = $sum1;
            $branch[$i]->useractive = $sum2;

            $i++;
        }

        $wl = WaitingList::all();
        // $data = User::select(DB::raw("year(created_at) as year,DAY(created_at) as 
        //   day,MONTHNAME(created_at) as monthname"))
        //     ->whereYear('created_at', date('Y'))
        //     ->get();
        $data = User::groupBy('role_id')
            ->selectRaw('count(*) as total, role_id')
            ->get();

        // dd($data);
        return view('admin.dashboard', compact("gender", "member", "user", "group", "gereja", "plan", "composition", "branch", "wl"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function approveleave(Request $request)
    {
        try {
            //code...
            $data = GroupMember::find($request->id);
            $data->leave_at = Carbon::now();
            $data->save();
            // dd($data->group_id);


            // dd($todo);
            return back()->with('status', 'Berhasil');
        } catch (\Throwable $th) {
            throw $th;
            // dd($th);

            return back()->with('status', 'galat');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cratemessage(Request $request)
    {
        //
        $chat = Chat::create([
            'from_id' => auth()->user()->id,
            'message' => $request->message
        ]);

        return redirect()->back();
    }

    public function update()
    {
        $data = User::all();
        $data2 = Chat::all();
        $data3 = Group::all();
        // dd($data2);
        // foreach ($data as $key) {
        //     # code...
        //     // dd($key->member2());
        //     dd($key->group->name);
        // }
        // return view('admin.test')->with('data1', $data,)->with('data2', $data2,)->with('data3', $data3)->with('notifications', auth()->user()->unreadNotifications);
        return view('admin.test')->with('data1', $data,)->with('data2', $data2,)->with('data3', $data3)->with('notifications', auth()->user()->unreadNotifications);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function test()
    {
        //
        return View::make("layouts.partials.chat")
            ->with("status", "something")
            ->render();
    }


    public function abc()
    {
        $user = User::find(5);
        $data2 = Chat::all();
        $data3 = Group::all();
        // dd($data2);
        // foreach ($data as $key) {
        //     # code...
        //     // dd($key->member2());
        //     dd($key->group->name);
        // }
        $message = Message::REGISTER;
        $user->notify(new GlobalNotification($message));
        // auth()->user()->notify(new UserNotification($user, $message));
        // $user->notifications
        // dd(auth()->user()->unreadNotifications);

        return auth()->user()->unreadNotifications;
    }
}

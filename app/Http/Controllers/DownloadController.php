<?php

namespace App\Http\Controllers;

use App\Exports\OverdueExport;
use App\Exports\Report03;
use App\Exports\StrikeExport;
use App\Exports\UsersExport;
use App\Models\Group;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class DownloadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function r03($id)
    {
        $data = Group::find($id);

        return Excel::download(new Report03($id), today() . " MEMBER GROUP-" . $data->name . '.xlsx');
    }

    public function user()
    {

        return Excel::download(new UsersExport(), today() . " MEMBER LIST" . '.xlsx');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function overdue()
    {
        return Excel::download(new OverdueExport(), today() . " OVERDUE " . '.xlsx');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function strike()
    {
        return Excel::download(new StrikeExport(), today() . " Strike " . '.xlsx');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

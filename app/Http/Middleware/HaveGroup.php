<?php

namespace App\Http\Middleware;

use App\Helpers\APIFormatter;
use Closure;
use Illuminate\Http\Request;
use Laravel\Sanctum\PersonalAccessToken;

class HaveGroup
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next,)
    {
        $msg = 'do not have an active group';
        $hashedTooken = $request->bearerToken();
        $token = PersonalAccessToken::findToken($hashedTooken);
        $data = $token->tokenable;
        if (isset($data->memberactive)) {
            return $next($request);
            // $check = $data->memberactive;
            // if ($check->leave_at == null && $check->complete_at == null && $check->transfer == null) {
            //     if ($check->group->type != "group") {
            //         return $next($request);
            //     } else {
            //         if ($check->group->end_date > today()) {
            //             return $next($request);
            //         }
            //     }


            //     // return APIFormatter::responseAPI(403, $msg, null, 'Forbidden');
            // }
            // dd($data->memberactive);
            // return APIFormatter::responseAPI(403, $msg, null, 'Forbidden');
        }
        return APIFormatter::responseAPI(403, $msg, null, 'Forbidden');
    }
}

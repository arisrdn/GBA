<?php

namespace App\Http\Middleware;

use App\Helpers\APIFormatter;
use App\Models\GroupMember;
use Closure;
use Illuminate\Http\Request;
use Laravel\Sanctum\PersonalAccessToken;

class activeuser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $type)
    {
        $msg = 'not allowed';
        $hashedTooken = $request->bearerToken();
        $token = PersonalAccessToken::findToken($hashedTooken);
        $data = $token->tokenable;
        // $check = GroupMember::where('user_id', '=', $data->id):
        // dd($data->memberactive);
        if (isset($data->memberactive)) {
            if ($type == "join") {
                return APIFormatter::responseAPI(403, $msg, null, 'Forbidden');
            } else {
                return $next($request);
            }
            return APIFormatter::responseAPI(403, $msg, null, 'Forbidden');
        }
        if ($type == "join") {
            return $next($request);
        } else {
            return APIFormatter::responseAPI(403, $msg, null, 'Forbidden');
        }
    }
}

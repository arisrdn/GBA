<?php

namespace App\Imports;

use App\Models\GroupTodolist;
use App\Models\ReadingPlanList;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

// class GroupActivityImport implements ToModel
class GroupTodolistImport implements ToModel, WithHeadingRow

{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */


    public function  __construct($group_id)
    {
        $this->group_id = $group_id;
    }
    public function model(array $row)
    {
        // dd($row->days);
        // if (array_key_exists("day", $row) && array_key_exists("chapterverse", $row)) {
        //     # code...
        //     return new ReadingPlanList([
        //         //
        //         'reading_plan_id' => $this->group_id,
        //         'day' => $row['day'],
        //         'chapter_verse' => $row['chapterverse'],
        //     ]);
        // } else {
        //     # code...
        //     // return false;
        //     // return null;
        // }

        return new ReadingPlanList([
            //
            'reading_plan_id' => $this->group_id,
            'day' => $row['day'],
            'chapter_verse' => $row['chapterverse'],
        ]);
    }
}

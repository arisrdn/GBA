<?php

namespace App\Exports;


use App\Models\Group;
use App\Models\GroupMember;
use App\Models\MemberTodolist;
use App\Models\User;
use DateTime;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use phpDocumentor\Reflection\PseudoTypes\True_;


class StrikeExport implements FromView, ShouldAutoSize, WithEvents
{
    // protected $id;

    // function __construct()
    // {
    //     $this->id = $id;
    // }
    public function view(): View
    {
        $member = GroupMember::whereHas("group", function ($query) {
            $query->where('type', 'group');
            $query->where('end_date', ">=", today());
        })->with("latestRead")->get()->sortBy("latestRead.read_at");
        $i = 0;
        foreach ($member as $value) {
            $member[$i]->lastRead =  MemberTodolist::where('group_member_id', $value->id)
                ->where("read_at", "!=", null)
                ->orderBy('id', 'DESC')
                ->first();
            $tgl1 = new DateTime($member[$i]->group->star_date);
            if (isset($member[$i]->lastRead->read_at)) {
                $tgl1 = new DateTime($member[$i]->lastRead->read_at);
            }
            $tgl2 = today();
            $member[$i]->overdue = $tgl2->diff($tgl1);
            $i++;
        }
        return view('download.strike', [
            'member' =>  $member
        ]);
    }

    public function registerEvents(): array
    {

        return [
            AfterSheet::class    => function (AfterSheet $event) {
                // $cellRange = 'A5:W5'; // All headers
                // $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);
                // $event->sheet->getDelegate()->getStyle('A1:E3')
                //     ->getAlignment()
                //     ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $header = [
                    'font' => [
                        'bold' => true,
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],

                ];
                $header2 = [
                    'font' => [
                        'bold' => true,
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'rotation' => 90,
                        'startColor' => [
                            'argb' => 'FFA0A0A0',
                        ],
                        'endColor' => [
                            'argb' => 'FFA0A0A0',
                        ],
                    ],
                ];


                $event->sheet->setAutoFilter('A3:G3');
                $event->sheet->getDelegate()->getStyle('A1:G3')->applyFromArray($header);
                $event->sheet->getDelegate()->getStyle('A3:G3')->applyFromArray($header2);
            },
        ];
    }
}

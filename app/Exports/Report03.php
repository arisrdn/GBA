<?php

namespace App\Exports;

use App\Models\Group;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use phpDocumentor\Reflection\PseudoTypes\True_;

// class Report03 implements FromCollection, WithHeadings
// {

//     private $id;

//     public function __construct($id)
//     {
//         $this->request = $id;
//     }
//     /**
//      * @return \Illuminate\Support\Collection
//      */
//     public function collection()
//     {
//         //
//         return Group::all();
//     }

//     public function headings(): array
//     {
//         return [
//             ['#', 'date'],
//             ['Amount', 'Status'],
//         ];
//     }
//     public function styles(Worksheet $sheet)
//     {
//         return [
//             // Style the first row as bold text.
//             1    => ['font' => ['bold' => true]],
//         ];
//     }
// }

// use App\Models\Group;
// use Maatwebsite\Excel\Concerns\FromView;


class Report03 implements FromView, ShouldAutoSize, WithEvents
{
    protected $id;

    function __construct($id)
    {
        $this->id = $id;
    }
    public function view(): View
    {
        // dd($this->id);
        return view('download.r03', [
            'group' =>  Group::find($this->id)
        ]);
    }

    public function registerEvents(): array
    {

        return [
            AfterSheet::class    => function (AfterSheet $event) {
                // $cellRange = 'A5:W5'; // All headers
                // $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);
                // $event->sheet->getDelegate()->getStyle('A1:E3')
                //     ->getAlignment()
                //     ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $header = [
                    'font' => [
                        'bold' => true,
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],

                ];
                $header2 = [
                    'font' => [
                        'bold' => true,
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'rotation' => 90,
                        'startColor' => [
                            'argb' => 'FFA0A0A0',
                        ],
                        'endColor' => [
                            'argb' => 'FFA0A0A0',
                        ],
                    ],
                ];


                $event->sheet->getDelegate()->getStyle('A1:E5')->applyFromArray($header);
                $event->sheet->getDelegate()->getStyle('A5:E5')->applyFromArray($header2);
            },
        ];
    }
}

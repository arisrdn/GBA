<?php

namespace App\Exports;

use App\Models\Group;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use phpDocumentor\Reflection\PseudoTypes\True_;


class UsersExport implements FromView, ShouldAutoSize, WithEvents
{
    // protected $id;

    // function __construct()
    // {
    //     $this->id = $id;
    // }
    public function view(): View
    {
        // dd($this->id);
        return view('download.ruser', [
            'member' =>   User::where('role_id', 2)->get()
        ]);
    }

    public function registerEvents(): array
    {

        return [
            AfterSheet::class    => function (AfterSheet $event) {
                // $cellRange = 'A5:W5'; // All headers
                // $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);
                // $event->sheet->getDelegate()->getStyle('A1:E3')
                //     ->getAlignment()
                //     ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $header = [
                    'font' => [
                        'bold' => true,
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],

                ];
                $header2 = [
                    'font' => [
                        'bold' => true,
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'rotation' => 90,
                        'startColor' => [
                            'argb' => 'FFA0A0A0',
                        ],
                        'endColor' => [
                            'argb' => 'FFA0A0A0',
                        ],
                    ],
                ];


                $event->sheet->setAutoFilter('A3:H3');
                $event->sheet->getDelegate()->getStyle('A1:H3')->applyFromArray($header);
                $event->sheet->getDelegate()->getStyle('A3:H3')->applyFromArray($header2);
            },
        ];
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class PendingTask extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        require_once app_path() . "/Helpers/Pending.php";
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
